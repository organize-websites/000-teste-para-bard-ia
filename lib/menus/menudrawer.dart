// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/api/notificacoes_api.dart';
import 'package:simplite/constants.dart';
import 'package:simplite/cotacoes/cotacoes.dart';
import 'package:simplite/funcoes/funcoes_gerais.dart';
import 'package:simplite/notificacoes/notificacoes.dart';
import 'package:simplite/splash/splash.dart';
import 'package:url_launcher/url_launcher.dart';

class MenuDrawer extends StatefulWidget {
  const MenuDrawer({super.key});
  @override
  State<MenuDrawer> createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(defaultPaddingMini),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.network(
                'https://app.simpsimuladoresonline.com.br/img/logo1.png',
                width: 150),
            const Text(
              'Porque SIMP é Simples!',
              style:
                  TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            const Divider(
              color: Colors.white,
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            FutureBuilder(
                future: dadosDoUsuario(),
                builder: (BuildContext context, AsyncSnapshot snapshot) =>
                    snapshot.hasData
                        ? Marquee(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                  backgroundImage: NetworkImage(snapshot.data[
                                      2]), // Substitua pelo caminho da sua imagem
                                  radius: 30.0,
                                ),
                                const SizedBox(
                                  width: defaultPaddingMini,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Marquee(
                                      child: Text(
                                        snapshot.data[0],
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Marquee(
                                      child: Text(
                                        snapshot.data[1],
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        : const Center()),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            const Divider(
              color: Colors.white,
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            ListTile(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return const NotificacoesModal();
                  },
                );
              },
              leading: const Icon(Icons.notifications, color: Colors.white),
              title: Row(
                children: [
                  const Text('Notificações   ',
                      style: TextStyle(color: Colors.white, fontSize: 13)),
                  CircleAvatar(
                    radius: 10,
                    backgroundColor: Colors.red,
                    child: FutureBuilder(
                        future: qtdNotificacoesNaoLidas(),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) =>
                                snapshot.hasData
                                    ? Text(
                                        snapshot.data,
                                        style: const TextStyle(
                                            color: Colors.white, fontSize: 10),
                                      )
                                    : const Center()),
                  ),
                ],
              ),
              trailing: const Icon(Icons.chevron_right, color: Colors.white),
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            const Divider(
              color: Colors.white,
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            ListTile(
              onTap: () async {
                var prefs = await SharedPreferences.getInstance();
                String token = prefs.getString('usr_tkn') ?? '';
                if (!await launchUrl(
                    Uri.parse(
                        'https://barra.agencialink.com.br/autologin/$token/simp_dashboard'),
                    mode: LaunchMode.externalApplication)) {
                  throw Exception('Could not launch');
                }
              },
              leading: const Icon(Icons.open_in_new, color: Colors.white),
              title: const Text('Versão completa',
                  style: TextStyle(color: Colors.white, fontSize: 13)),
              trailing: const Icon(Icons.chevron_right, color: Colors.white),
            ),
            ListTile(
              onTap: () async {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return const CotacoesEnviadas();
                  },
                );
                /*if (!await launchUrl(
                    Uri.parse(
                        'https://dashboard-simp.agencialink.com.br/cotacoes'),
                    mode: LaunchMode.externalApplication)) {
                  throw Exception('Could not launch');
                }*/
              },
              leading: const Icon(Icons.send, color: Colors.white),
              title: const Text('Cotações Enviadas',
                  style: TextStyle(color: Colors.white, fontSize: 13)),
              trailing: const Icon(Icons.chevron_right, color: Colors.white),
            ),
            ListTile(
              onTap: () async {
                if (!await launchUrl(
                    Uri.parse('https://suporte.agencialink.com.br/'),
                    mode: LaunchMode.externalApplication)) {
                  throw Exception('Could not launch');
                }
              },
              leading: const Icon(Icons.support_agent, color: Colors.white),
              title: const Text('Suporte',
                  style: TextStyle(color: Colors.white, fontSize: 13)),
              trailing: const Icon(Icons.chevron_right, color: Colors.white),
            ),
            ListTile(
              onTap: () async {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        title: Center(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      icon: const Icon(Icons.close))
                                ],
                              ),
                              const FaIcon(
                                FontAwesomeIcons.triangleExclamation,
                                size: 30,
                                color: Colors.deepOrange,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text(
                                'Atenção!!!',
                                style: TextStyle(color: Colors.deepOrange),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text(
                                'Deseja realmente sair?',
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                        actions: [
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text(
                                'Cancelar',
                                style: TextStyle(color: Colors.grey),
                              )),
                          ElevatedButton(
                              onPressed: () async {
                                var prefs =
                                    await SharedPreferences.getInstance();
                                await prefs.clear();
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const Splash()));
                              },
                              child: const Text(
                                'Sair',
                                style: TextStyle(color: Colors.red),
                              )),
                        ],
                      );
                    });
              },
              leading: const Icon(Icons.exit_to_app, color: Colors.white),
              title: const Text('Sair',
                  style: TextStyle(color: Colors.white, fontSize: 13)),
              trailing: const Icon(Icons.chevron_right, color: Colors.white),
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            const Divider(
              color: Colors.white,
            ),
            const SizedBox(
              height: defaultPaddingMini,
            ),
            Center(
              child: IconButton(
                  onPressed: () {
                    var themeState =
                        Provider.of<ThemeState>(context, listen: false);
                    themeState.toggleTheme();
                  },
                  icon: FaIcon(
                    Provider.of<ThemeState>(context, listen: true).isDarkMode
                        ? FontAwesomeIcons.sun
                        : FontAwesomeIcons.moon,
                    color: Colors.white,
                  )),
            ),
            Center(
              child: RichText(
                text: const TextSpan(
                  children: [
                    TextSpan(
                      text: 'agencia',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                    TextSpan(
                      text: 'link',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 13.0,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                    TextSpan(
                      text: '.com',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                        fontWeight: FontWeight.w200,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const Center(
                child: Text(
              'v 3.3.22',
              style: TextStyle(color: Colors.white, fontSize: 11),
            ))
          ],
        ),
      ),
    );
  }
}
