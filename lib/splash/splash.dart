// ignore_for_file: use_build_context_synchronously

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/home/home.dart';
import 'package:simplite/login/login.dart';
import 'package:simplite/operadora/operadora.dart';
import 'package:video_player/video_player.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});
  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  late VideoPlayerController _controller;
  @override
  void initState() {
    super.initState();
    setState(() {
      EscolhaAOperadora.isEdicaoDeCotacao = false;
    });
    _controller = VideoPlayerController.asset('assets/s.mp4')
      ..initialize().then((_) {
        _controller.play();
        setState(() {});
      });
    /*Timer(const Duration(seconds: 5), () async {
      var prefs = await SharedPreferences.getInstance();
      bool isLoggedIn = prefs.getBool('isLoggedIn') ?? false;
      String idDaCidadeNoCache = prefs.getString('idDaCidadenoCache') ?? '';
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => isLoggedIn
                ? MyHomePage(
                    nomeDoCliente: '',
                    cidadeEstado: idDaCidadeNoCache,
                    custoDoPlanoAtualValorInicial: '0',
                    value0a18: 0,
                    value19a23: 0,
                    value24a28: 0,
                    value29a33: 0,
                    value34a38: 0,
                    value39a43: 0,
                    value44a48: 0,
                    value49a53: 0,
                    value54a58: 0,
                    value59: 0,
                  )
                : const LoginScreen()),
      );
    });*/
    irAgora();
  }

  Future irAgora() async {
    var prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool('isLoggedIn') ?? false;
    await Future.delayed(Duration(seconds: isLoggedIn ? 4 : 11));
    String idDaCidadeNoCache = prefs.getString('idDaCidadenoCache') ?? '';
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => isLoggedIn
              ? MyHomePage(
                  nomeDoCliente: '',
                  cidadeEstado: idDaCidadeNoCache,
                  custoDoPlanoAtualValorInicial: '0',
                  value0a18: 0,
                  value19a23: 0,
                  value24a28: 0,
                  value29a33: 0,
                  value34a38: 0,
                  value39a43: 0,
                  value44a48: 0,
                  value49a53: 0,
                  value54a58: 0,
                  value59: 0,
                  idDoCliente: '',
                  emailDoCliente: '',
                  celularDoCliente: '',
                )
              : const LoginScreen()),
    );
  }

  Future<bool> videoOuNormal() async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.getBool('isLoggedIn') ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: videoOuNormal(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return snapshot.data!
                  ? Center(
                      child: Container(
                        color: Colors.white,
                        child: Center(
                          child: Padding(
                              padding: const EdgeInsets.all(25.0),
                              child: Image.asset(
                                'assets/loader.gif',
                                width: 150,
                              )
                              /*Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.network(
                                    'https://app.simpsimuladoresonline.com.br/img/logo1.png',
                                    width: 160),
                                const Text(
                                  'Porque SIMP é Simples!',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontStyle: FontStyle.italic),
                                )
                              ],
                            ),*/
                              ),
                        ),
                      ),
                    )
                  : Container(
                      color: Colors.white,
                      child: Center(
                          child: _controller.value.isInitialized
                              ? AspectRatio(
                                  aspectRatio: _controller.value.aspectRatio,
                                  child: VideoPlayer(_controller),
                                )
                              : Center(
                                  child: Image.asset(
                                  'assets/loader.gif',
                                  width: 150,
                                ))),
                    );
            } else {
              return Image.asset(
                'assets/loader.gif',
                width: 80,
              );
            }
          }),
    );
  }
}
