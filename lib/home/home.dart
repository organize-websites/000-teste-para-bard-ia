// ignore_for_file: use_build_context_synchronously, must_be_immutable

import 'package:el_tooltip2/el_tooltip2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/api/notificacoes_api.dart';
import 'package:simplite/constants.dart';
import 'package:simplite/cotacoes/cotacoes.dart';
import 'package:simplite/funcoes/funcoes_homepage.dart';
import 'package:simplite/menus/menudrawer.dart';
import 'package:flutter_masked_text2/flutter_masked_text2.dart';
import 'package:simplite/funcoes/funcoes_gerais.dart';
import 'package:simplite/modelos/modelos.dart';
import 'package:simplite/notificacoes/notificacoes.dart';

import '../operadora/operadora.dart';

class MyHomePage extends StatefulWidget {
  final String nomeDoCliente;
  final String cidadeEstado;
  final String custoDoPlanoAtualValorInicial;
  int value0a18;
  int value19a23;
  int value24a28;
  int value29a33;
  int value34a38;
  int value39a43;
  int value44a48;
  int value49a53;
  int value54a58;
  int value59;
  String idDoCliente;
  String emailDoCliente;
  String celularDoCliente;
  MyHomePage(
      {super.key,
      required this.nomeDoCliente,
      required this.cidadeEstado,
      required this.custoDoPlanoAtualValorInicial,
      required this.value0a18,
      required this.value19a23,
      required this.value24a28,
      required this.value29a33,
      required this.value34a38,
      required this.value39a43,
      required this.value44a48,
      required this.value49a53,
      required this.value54a58,
      required this.value59,
      required this.idDoCliente,
      required this.emailDoCliente,
      required this.celularDoCliente});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _value0a18b = false;
  bool _value19a23b = false;
  bool _value24a28b = false;
  bool _value29a33b = false;
  bool _value34a38b = false;
  bool _value39a43b = false;
  bool _value44a48b = false;
  bool _value49a53b = false;
  bool _value54a58b = false;
  bool _value59b = false;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _valueController = TextEditingController();
  final _advancedDrawerController = AdvancedDrawerController();
  final TextEditingController _searchController = TextEditingController();
  String mesDeReajuste = '';
  final TextEditingController _observacoesDoPlanoAtualController =
      TextEditingController();
  final MoneyMaskedTextController _custoDoPlanoAtualController =
      MoneyMaskedTextController(
    initialValue: 0.0,
    leftSymbol: 'R\$ ',
    decimalSeparator: ',',
    thousandSeparator: '',
  );
  final MoneyMaskedTextController _indiceDeReajusteController =
      MoneyMaskedTextController(
    rightSymbol: '%',
    decimalSeparator: ',',
    thousandSeparator: '',
  );
  final MoneyMaskedTextController _valorAporReajustadoController =
      MoneyMaskedTextController(
    initialValue: 0.0,
    leftSymbol: 'R\$ ',
    decimalSeparator: ',',
    thousandSeparator: '',
  );
  String _selectedCity = '';
  String _selectedCityID = '';
  String _selectedUf = '';
  final _formKey = GlobalKey<FormState>();
  List<Lead> leads = [];
  String searchText = '';
  Lead? selectedLead;

  List<Lead> get filteredLeads {
    return leads.where((lead) {
      final nameMatches =
          lead.name.toLowerCase().contains(searchText.toLowerCase());
      final emailMatches =
          lead.email.toLowerCase().contains(searchText.toLowerCase());
      final dataMatches =
          lead.data.toLowerCase().contains(searchText.toLowerCase());
      final idMatches =
          lead.id.toLowerCase().contains(searchText.toLowerCase());
      return nameMatches || emailMatches || dataMatches || idMatches;
    }).toList();
  }

  Future<List<dynamic>> _getSuggestions(String pattern) async {
    final List<dynamic> cities = await buscarCidades('', pattern);
    return List.generate(
        cities.length,
        (index) =>
            '${cities[index]["cidade"]} - ${cities[index]["uf"]} (${cities[index]["id"]})');
  }

  Future<bool> temB2cor() async {
    var prefs = await SharedPreferences.getInstance();
    bool temB2cor = prefs.getBool('b2cor') ?? false;
    return temB2cor;
  }

  @override
  void initState() {
    super.initState();
    fetchLeads(searchText).then((leadList) {
      setState(() {
        leads = leadList;
      });
    });
    setState(() {
      _nameController.text = widget.nomeDoCliente;
      _searchController.text = widget.cidadeEstado.toString().split(" (").first;
      _selectedCity = widget.cidadeEstado.toString().split(" -").first;
      _selectedUf =
          widget.cidadeEstado.toString().split("- ").last.split(" (").first;
      _selectedCityID =
          widget.cidadeEstado.toString().split(" (").last.split(")").first;
      _custoDoPlanoAtualController.text =
          'R\$ ${widget.custoDoPlanoAtualValorInicial}';
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _cityController.dispose();
    _valueController.dispose();
    super.dispose();
  }

  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }

  @override
  Widget build(BuildContext context) {
    if (selectedLead != null) {
      _nameController.text = selectedLead!.name;
    }
    return AdvancedDrawer(
      backdrop: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.blueGrey, Colors.blueGrey.withOpacity(0.7)],
          ),
        ),
      ),
      controller: _advancedDrawerController,
      animationCurve: Curves.easeInOut,
      animationDuration: const Duration(milliseconds: 300),
      animateChildDecoration: true,
      rtlOpening: false,
      disabledGestures: false,
      childDecoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      drawer: const MenuDrawer(),
      child: SafeArea(
        child: Scaffold(
          body: Column(
            children: [
              FutureBuilder(
                  future: dadosDoUsuario(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) =>
                      snapshot.hasData
                          ? Container(
                              color: Colors.blue,
                              padding: const EdgeInsets.all(defaultPaddingMini),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  IconButton(
                                    icon: const Icon(Icons.menu,
                                        color: Colors.white),
                                    onPressed: _handleMenuButtonPressed,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        snapshot.data[0],
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        snapshot.data[1],
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                  CircleAvatar(
                                    backgroundImage:
                                        NetworkImage(snapshot.data[2]),
                                    radius: 20.0,
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              color: Colors.blue,
                              padding: const EdgeInsets.all(defaultPaddingMini),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  IconButton(
                                    icon: const Icon(Icons.menu,
                                        color: Colors.white),
                                    onPressed: _handleMenuButtonPressed,
                                  ),
                                  const Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        '',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        '',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                  const CircleAvatar(
                                    radius: 20.0,
                                    child: Center(child: Icon(Icons.person)),
                                  ),
                                ],
                              ),
                            )),
              Wrap(alignment: WrapAlignment.spaceBetween, children: [
                TextButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const NotificacoesModal();
                        },
                      );
                    },
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        const Icon(
                          Icons.notifications,
                          size: 12,
                        ),
                        const SizedBox(
                          width: 3,
                        ),
                        const Text('Notificações',
                            style: TextStyle(fontSize: 12)),
                        const SizedBox(width: 3),
                        CircleAvatar(
                          radius: 8,
                          backgroundColor: Colors.red,
                          child: Center(
                            child: FutureBuilder(
                                future: qtdNotificacoesNaoLidas(),
                                builder: (BuildContext context,
                                        AsyncSnapshot snapshot) =>
                                    snapshot.hasData
                                        ? Text(
                                            snapshot.data,
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 10),
                                          )
                                        : const Center()),
                          ),
                        ),
                      ],
                    )),
                TextButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const CotacoesEnviadas();
                        },
                      );
                    },
                    child: const Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Icon(
                          Icons.send,
                          size: 12,
                        ),
                        SizedBox(
                          width: 3,
                        ),
                        Text('Cotações Enviadas',
                            style: TextStyle(fontSize: 12)),
                      ],
                    )),
                const Row()
              ]),
              Expanded(
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 10),
                  child: SizedBox(
                    width: 360,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          const Spacer(),
                          Center(
                              child: Padding(
                            padding: const EdgeInsets.only(top: defaultPadding),
                            child: Image.network(
                                Provider.of<ThemeState>(context, listen: true)
                                        .isDarkMode
                                    ? 'https://www.simuladorpersonalizado.com.br/l.webp'
                                    : 'https://simpsimuladoresonline.com.br/img/2.webp',
                                width: 200),
                          )),
                          Center(
                            child: Text(
                              Provider.of<ThemeState>(context, listen: true)
                                      .isDarkMode
                                  ? ''
                                  : "Porque SIMP é Simples!",
                              style:
                                  Provider.of<ThemeState>(context, listen: true)
                                          .isDarkMode
                                      ? const TextStyle(
                                          fontSize: 1,
                                        )
                                      : const TextStyle(
                                          color: Color(0xff263560),
                                          fontStyle: FontStyle.italic),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          const Spacer(),
                          TextFormField(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Por favor, digite o nome do cliente';
                              }
                              return null;
                            },
                            controller: _nameController,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.person),
                              labelText: 'Nome do Cliente',
                              labelStyle: const TextStyle(fontSize: 14),
                              suffixIcon: FutureBuilder(
                                  future: temB2cor(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return snapshot.data!
                                          ? IconButton(
                                              onPressed: () {
                                                showModalBottomSheet(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8.0),
                                                    ),
                                                    enableDrag: false,
                                                    isScrollControlled: true,
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return StatefulBuilder(
                                                          builder: (BuildContext
                                                                  context,
                                                              StateSetter
                                                                  setState) {
                                                        return SizedBox(
                                                          height: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .height *
                                                              0.7,
                                                          child:
                                                              SingleChildScrollView(
                                                            child: Column(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: [
                                                                Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                          .all(
                                                                          8.0),
                                                                  child:
                                                                      TextFormField(
                                                                    onChanged:
                                                                        (value) async {
                                                                      setState(
                                                                          () {
                                                                        searchText =
                                                                            value;
                                                                      });
                                                                      await fetchLeads(
                                                                              searchText)
                                                                          .then(
                                                                              (leadList) {
                                                                        setState(
                                                                            () {
                                                                          leads =
                                                                              leadList;
                                                                        });
                                                                      });
                                                                    },
                                                                    decoration:
                                                                        const InputDecoration(
                                                                      hintText:
                                                                          'Pesquisar Lead...',
                                                                      prefixIcon:
                                                                          Icon(Icons
                                                                              .search),
                                                                    ),
                                                                  ),
                                                                ),
                                                                for (var lead
                                                                    in filteredLeads)
                                                                  ListTile(
                                                                    onTap:
                                                                        () async {
                                                                      setState(
                                                                          () {
                                                                        selectedLead =
                                                                            lead;
                                                                        _nameController.text =
                                                                            lead.name;
                                                                        widget.emailDoCliente =
                                                                            lead.email;
                                                                        widget.idDoCliente =
                                                                            lead.id;
                                                                        widget.celularDoCliente =
                                                                            lead.celular;
                                                                      });
                                                                      Navigator.pop(
                                                                          context);
                                                                    },
                                                                    leading:
                                                                        Icon(
                                                                      selectedLead ==
                                                                              lead
                                                                          ? Icons
                                                                              .check
                                                                          : Icons
                                                                              .check_box_outline_blank,
                                                                      color: selectedLead ==
                                                                              lead
                                                                          ? Colors
                                                                              .blue
                                                                          : null,
                                                                    ),
                                                                    title: Text(
                                                                        lead
                                                                            .name,
                                                                        style: const TextStyle(
                                                                            fontSize:
                                                                                14)),
                                                                    subtitle:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        lead.email ==
                                                                                ''
                                                                            ? const SizedBox
                                                                                .shrink()
                                                                            : Text(lead.email,
                                                                                style: const TextStyle(fontSize: 10)),
                                                                        Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            Text(lead.data,
                                                                                style: const TextStyle(fontSize: 10)),
                                                                            const Text('-',
                                                                                style: TextStyle(fontSize: 10)),
                                                                            Text('ID Lead: ${lead.id}',
                                                                                style: const TextStyle(fontSize: 10)),
                                                                          ],
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      });
                                                    });
                                              },
                                              icon: const Icon(Icons.search))
                                          : const SizedBox.shrink();
                                    } else {
                                      return const CircularProgressIndicator
                                          .adaptive();
                                    }
                                  }),
                            ),
                          ),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                          const Spacer(),
                          TypeAheadFormField(
                            textFieldConfiguration: TextFieldConfiguration(
                              controller: _searchController,
                              decoration: InputDecoration(
                                  labelText: 'Buscar Cidade',
                                  prefixIcon: const Icon(Icons.map),
                                  labelStyle: const TextStyle(fontSize: 14),
                                  suffixIcon: _searchController.text != ''
                                      ? IconButton(
                                          onPressed: () async {
                                            setState(() {
                                              _searchController.text = '';
                                            });
                                          },
                                          icon: const Icon(Icons.close))
                                      : const Icon(Icons.search)),
                            ),
                            suggestionsCallback: (pattern) async {
                              return _getSuggestions(pattern);
                            },
                            itemBuilder: (context, suggestion) {
                              return ListTile(
                                title: Text(
                                    suggestion.toString().split(" (").first),
                              );
                            },
                            onSuggestionSelected: (suggestion) {
                              setState(() {
                                _searchController.text =
                                    suggestion.toString().split(" (").first;
                                _selectedCityID = suggestion
                                    .toString()
                                    .split(" (")
                                    .last
                                    .split(")")
                                    .first;
                                _selectedCity =
                                    suggestion.toString().split(" -").first;
                                _selectedUf = suggestion
                                    .toString()
                                    .split("- ")
                                    .last
                                    .split(" (")
                                    .first
                                    .split(")")
                                    .first;
                              });
                            },
                            noItemsFoundBuilder: (context) {
                              return const ListTile(
                                title: Text('Nenhum resultado encontrado'),
                              );
                            },
                            transitionBuilder:
                                (context, suggestionsBox, controller) {
                              return suggestionsBox;
                            },
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Por favor, selecione uma cidade';
                              }
                              return null;
                            },
                          ),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                          const Spacer(),
                          GestureDetector(
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                      ),
                                      insetPadding: const EdgeInsets.all(8.0),
                                      title: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text('Dados do Plano Atual'),
                                          IconButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            icon: const Icon(Icons.close),
                                          ),
                                        ],
                                      ),
                                      content: Column(
                                        children: [
                                          const Spacer(),
                                          const Text(
                                            'Preencha os dados para calcular a projeção de economia, considerando o reajuste do plano atual:',
                                            textAlign: TextAlign.center,
                                          ),
                                          const Spacer(),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                                          TextFormField(
                                            keyboardType: TextInputType.number,
                                            controller:
                                                _custoDoPlanoAtualController,
                                            decoration: const InputDecoration(
                                              prefixIcon:
                                                  Icon(Icons.monetization_on),
                                              labelText: 'Custo do Plano Atual',
                                            ),
                                            onChanged: (value) {
                                              setState(() {
                                                double indiceReajuste =
                                                    double.parse(
                                                        _indiceDeReajusteController
                                                            .text
                                                            .replaceAll(
                                                                ',', '.')
                                                            .replaceAll(
                                                                '%', ''));
                                                double custoPlano =
                                                    double.parse(value
                                                        .replaceAll('R\$ ', '')
                                                        .replaceAll(',', '.'));
                                                double valorAposReajuste =
                                                    custoPlano *
                                                        (1 +
                                                            (indiceReajuste /
                                                                100));
                                                _valorAporReajustadoController
                                                        .text =
                                                    valorAposReajuste
                                                        .toStringAsFixed(2);
                                              });
                                            },
                                          ),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                                          const Spacer(),
                                          DropdownButtonFormField<String>(
                                            padding: const EdgeInsets.all(2),
                                            isExpanded: true,
                                            items: const [
                                              DropdownMenuItem(
                                                  value: 'Janeiro',
                                                  child: Text('Janeiro')),
                                              DropdownMenuItem(
                                                  value: 'Fevereiro',
                                                  child: Text('Fevereiro')),
                                              DropdownMenuItem(
                                                  value: 'Março',
                                                  child: Text('Março')),
                                              DropdownMenuItem(
                                                  value: 'Abril',
                                                  child: Text('Abril')),
                                              DropdownMenuItem(
                                                  value: 'Maio',
                                                  child: Text('Maio')),
                                              DropdownMenuItem(
                                                  value: 'Junho',
                                                  child: Text('Junho')),
                                              DropdownMenuItem(
                                                  value: 'Julho',
                                                  child: Text('Julho')),
                                              DropdownMenuItem(
                                                  value: 'Agosto',
                                                  child: Text('Agosto')),
                                              DropdownMenuItem(
                                                  value: 'Setembro',
                                                  child: Text('Setembro')),
                                              DropdownMenuItem(
                                                  value: 'Outubro',
                                                  child: Text('Outubro')),
                                              DropdownMenuItem(
                                                  value: 'Novembro',
                                                  child: Text('Novembro')),
                                              DropdownMenuItem(
                                                  value: 'Dezembro',
                                                  child: Text('Dezembro')),
                                            ],
                                            decoration: const InputDecoration(
                                              helperText: 'Mês de Reajuste',
                                              contentPadding: EdgeInsets.all(2),
                                              hintText: 'Selecione',
                                              prefixIcon:
                                                  Icon(Icons.calendar_month),
                                            ),
                                            onChanged: (value) async {
                                              setState(() {
                                                mesDeReajuste = value ==
                                                        'Janeiro'
                                                    ? '1'
                                                    : value == 'Fevereiro'
                                                        ? '2'
                                                        : value == 'Março'
                                                            ? '3'
                                                            : value == 'Abril'
                                                                ? '4'
                                                                : value ==
                                                                        'Maio'
                                                                    ? '5'
                                                                    : value ==
                                                                            'Junho'
                                                                        ? '6'
                                                                        : value ==
                                                                                'Julho'
                                                                            ? '7'
                                                                            : value == 'Agosto'
                                                                                ? '8'
                                                                                : value == 'Setembro'
                                                                                    ? '9'
                                                                                    : value == 'Outubro'
                                                                                        ? '10'
                                                                                        : value == 'Novembro'
                                                                                            ? '11'
                                                                                            : value == 'Dezembro'
                                                                                                ? '12'
                                                                                                : '';
                                              });
                                            },
                                          ),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                                          const Spacer(),
                                          TextFormField(
                                            controller:
                                                _indiceDeReajusteController,
                                            keyboardType: TextInputType.number,
                                            decoration: const InputDecoration(
                                                prefixIcon: Icon(Icons.percent),
                                                labelText:
                                                    'Índice de Reajuste (%)',
                                                suffixIcon: ElTooltip(
                                                    content: Text(
                                                        'Informe o percentual de reajuste aplicado anualmente, ou caso não saiba, informe abaixo o valor após o reajuste para o sistema calcular o percentual'),
                                                    child: Icon(Icons.info))),
                                            onChanged: (value) {
                                              setState(() {
                                                double custoPlano = double.parse(
                                                    _custoDoPlanoAtualController
                                                        .text
                                                        .replaceAll('R\$ ', '')
                                                        .replaceAll(',', '.'));
                                                double indiceReajuste =
                                                    double.parse(value
                                                        .replaceAll(',', '.')
                                                        .replaceAll('%', ''));
                                                double valorAposReajuste =
                                                    custoPlano *
                                                        (1 +
                                                            (indiceReajuste /
                                                                100));
                                                _valorAporReajustadoController
                                                        .text =
                                                    valorAposReajuste
                                                        .toStringAsFixed(2);
                                              });
                                            },
                                          ),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                                          const Spacer(),
                                          TextFormField(
                                            controller:
                                                _valorAporReajustadoController,
                                            keyboardType: TextInputType.number,
                                            decoration: const InputDecoration(
                                                prefixIcon: Icon(Icons
                                                    .monetization_on_outlined),
                                                labelText:
                                                    'Valor Após Reajustado',
                                                suffixIcon: ElTooltip(
                                                    content: Text(
                                                        'Informe o valor do plano após o reajuste anual, ou informe acima o percentual de reajuste para o sistema calcular automaticamente'),
                                                    child: Icon(Icons.info))),
                                            onChanged: (value) {
                                              setState(() {
                                                double custoPlano = double.parse(
                                                    _custoDoPlanoAtualController
                                                        .text
                                                        .replaceAll('R\$ ', '')
                                                        .replaceAll(',', '.'));
                                                double valorAposReajuste =
                                                    double.parse(value
                                                        .replaceAll('R\$ ', '')
                                                        .replaceAll(',', '.'));
                                                double indiceReajuste =
                                                    ((valorAposReajuste -
                                                                custoPlano) /
                                                            custoPlano) *
                                                        100;
                                                _indiceDeReajusteController
                                                        .text =
                                                    indiceReajuste
                                                        .toStringAsFixed(2);
                                              });
                                            },
                                          ),
                                          const SizedBox(
                                            height: defaultPadding,
                                          ),
                                          const Spacer(),
                                          TextFormField(
                                            controller:
                                                _observacoesDoPlanoAtualController,
                                            maxLines: 3,
                                            keyboardType: TextInputType.text,
                                            decoration: const InputDecoration(
                                              prefixIcon:
                                                  Icon(Icons.text_fields),
                                              labelText: 'Observações',
                                            ),
                                          ),
                                          const SizedBox(height: 20),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              ElevatedButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: const Text(
                                                  'Fechar',
                                                  style: TextStyle(
                                                      color: Colors.grey),
                                                ),
                                              ),
                                              const SizedBox(width: 10),
                                              ElevatedButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: const Text('Continuar'),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.grey[300]!),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10))),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 11),
                                  child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Padding(
                                          padding: EdgeInsets.only(
                                              top: 0,
                                              bottom: 0,
                                              left: 8,
                                              right: 8),
                                          child: Icon(
                                            Icons.monetization_on,
                                          ),
                                        ),
                                        const Text(
                                          'Custo do Plano Atual: ',
                                        ),
                                        Text(
                                          _custoDoPlanoAtualController.text,
                                        ),
                                      ]),
                                ),
                              )),
                          const Spacer(),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Faixas Etárias:'),
                                Text(
                                    'Total: ${(widget.value0a18 + widget.value19a23 + widget.value24a28 + widget.value29a33 + widget.value34a38 + widget.value39a43 + widget.value44a48 + widget.value49a53 + widget.value54a58 + widget.value59).toString()}')
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: defaultPadding,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: GridView.count(
                              crossAxisCount: 3,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              childAspectRatio: 1.7,
                              children: List.generate(12, (index) {
                                if (index == 9) {
                                  return Padding(
                                    padding: const EdgeInsets.all(
                                        defaultPaddingMini),
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        textStyle:
                                            const MaterialStatePropertyAll(
                                                TextStyle(color: Colors.white)),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.grey[400]!),
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                        ),
                                        padding: MaterialStateProperty.all<
                                            EdgeInsetsGeometry>(
                                          const EdgeInsets.all(5.0),
                                        ),
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          widget.value0a18 = 0;
                                          widget.value19a23 = 0;
                                          widget.value24a28 = 0;
                                          widget.value29a33 = 0;
                                          widget.value34a38 = 0;
                                          widget.value39a43 = 0;
                                          widget.value44a48 = 0;
                                          widget.value49a53 = 0;
                                          widget.value54a58 = 0;
                                          widget.value59 = 0;
                                        });
                                      },
                                      child: const Text('Limpar',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                  );
                                } else if (index == 11) {
                                  return Padding(
                                    padding: const EdgeInsets.all(
                                        defaultPaddingMini),
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        textStyle:
                                            const MaterialStatePropertyAll(
                                                TextStyle(color: Colors.white)),
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.blue),
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                        ),
                                        padding: MaterialStateProperty.all<
                                            EdgeInsetsGeometry>(
                                          const EdgeInsets.all(5.0),
                                        ),
                                      ),
                                      onPressed: () async {
                                        var prefs = await SharedPreferences
                                            .getInstance();
                                        if (_formKey.currentState!.validate()) {
                                          await prefs.setString(
                                              'idDaCidadenoCache',
                                              '$_selectedCity - $_selectedUf ($_selectedCityID)');
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      EscolhaAOperadora(
                                                        isAdesaoAgora: false,
                                                        nomedocliente:
                                                            _nameController
                                                                .text,
                                                        cidade: _selectedCity,
                                                        idDaCidade:
                                                            _selectedCityID,
                                                        uf: _selectedUf,
                                                        valoratual:
                                                            _custoDoPlanoAtualController
                                                                .text
                                                                .toString()
                                                                .split(' ')
                                                                .last,
                                                        i0a18: widget.value0a18
                                                            .toString(),
                                                        i19a23: widget
                                                            .value19a23
                                                            .toString(),
                                                        i24a28: widget
                                                            .value24a28
                                                            .toString(),
                                                        i29a33: widget
                                                            .value29a33
                                                            .toString(),
                                                        i34a38: widget
                                                            .value34a38
                                                            .toString(),
                                                        i39a43: widget
                                                            .value39a43
                                                            .toString(),
                                                        i44a48: widget
                                                            .value44a48
                                                            .toString(),
                                                        i49a53: widget
                                                            .value49a53
                                                            .toString(),
                                                        i54a58: widget
                                                            .value54a58
                                                            .toString(),
                                                        i59: widget.value59
                                                            .toString(),
                                                        totalDeVidas: (widget
                                                                    .value0a18 +
                                                                widget
                                                                    .value19a23 +
                                                                widget
                                                                    .value24a28 +
                                                                widget
                                                                    .value29a33 +
                                                                widget
                                                                    .value34a38 +
                                                                widget
                                                                    .value39a43 +
                                                                widget
                                                                    .value44a48 +
                                                                widget
                                                                    .value49a53 +
                                                                widget
                                                                    .value54a58 +
                                                                widget.value59)
                                                            .toString(),
                                                        selectedSaudeIndex: 0,
                                                        selectedPlanoIndex: 2,
                                                        saudeOuOdonto: 's',
                                                        pfOuPme: 'pf',
                                                        meiFiltro: 'MEI',
                                                        reembolsoFiltro:
                                                            'Reembolso',
                                                        somarTaxasFiltro:
                                                            'Somar Taxas',
                                                        abrangenciaFiltro:
                                                            'Abrangência',
                                                        atendimentoFiltro:
                                                            'Atendimento',
                                                        coparticipacaoFiltro:
                                                            'Coparticipação',
                                                        padraoDeConfortoFiltro:
                                                            'Padrão de Conforto',
                                                        regrasDeAceitacaoFiltro:
                                                            'Regras de Aceitação',
                                                        coparticipacao: '',
                                                        padraoDeConforto: '',
                                                        reembolso: '',
                                                        abrangencia: '',
                                                        atendimento: '',
                                                        mei: '',
                                                        somarTaxas: '',
                                                        regrasDeAcetacao: '',
                                                        credenciado: const [],
                                                        produtosFiltro: const [],
                                                        operadoras: const [],
                                                        entidade: '',
                                                        profissao: '',
                                                        administradora: '',
                                                        mesDeReajuste:
                                                            mesDeReajuste,
                                                        indiceDeReajuste:
                                                            _indiceDeReajusteController
                                                                .text
                                                                .split('%')
                                                                .first,
                                                        valorReajustado:
                                                            _valorAporReajustadoController
                                                                .text
                                                                .toString()
                                                                .split(' ')
                                                                .last,
                                                        observacoesDeReajuste:
                                                            _observacoesDoPlanoAtualController
                                                                .text,
                                                        emailDoCliente: widget
                                                            .emailDoCliente,
                                                        idDoCliente:
                                                            widget.idDoCliente,
                                                        celularDoCliente: widget
                                                            .celularDoCliente,
                                                        qualAdministradora: '',
                                                        qualProfissao: '',
                                                        qualEntidade: '',
                                                      )));
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                  ),
                                                  title: Center(
                                                    child: Column(
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            IconButton(
                                                                onPressed: () {
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                icon: const Icon(
                                                                    Icons
                                                                        .close))
                                                          ],
                                                        ),
                                                        const FaIcon(
                                                          FontAwesomeIcons
                                                              .triangleExclamation,
                                                          size: 30,
                                                          color:
                                                              Colors.deepOrange,
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        const Text(
                                                          'Atenção!!!',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .deepOrange),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        const Text(
                                                          'Preencha os dados obrigatórios!',
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                        const SizedBox(
                                                          height: 20,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        }
                                      },
                                      child: const Text('Próxima',
                                          textAlign: TextAlign.center,
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                  );
                                } else if (index == 0) {
                                  return _value0a18b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('0 a 18'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value0a18b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value0a18 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value0a18++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value0a18b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value0a18 =
                                                  widget.value0a18 == 0
                                                      ? 0
                                                      : widget.value0a18 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text(
                                                      '0 a 18',
                                                      style: TextStyle(
                                                          fontSize: 10),
                                                    )),
                                                Center(
                                                    child: Text(
                                                  widget.value0a18.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget.value0a18 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 1) {
                                  return _value19a23b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('19 a 23'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value19a23b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value19a23 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value19a23++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value19a23b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value19a23 =
                                                  widget.value19a23 == 0
                                                      ? 0
                                                      : widget.value19a23 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('19 a 23',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value19a23.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value19a23 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 2) {
                                  return _value24a28b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('24 a 28'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value24a28b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value24a28 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value24a28++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value24a28b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value24a28 =
                                                  widget.value24a28 == 0
                                                      ? 0
                                                      : widget.value24a28 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('24 a 28',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value24a28.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value24a28 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 3) {
                                  return _value29a33b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('29 a 33'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value29a33b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value29a33 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value29a33++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value29a33b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value29a33 =
                                                  widget.value29a33 == 0
                                                      ? 0
                                                      : widget.value29a33 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('29 a 33',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value29a33.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value29a33 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 4) {
                                  return _value34a38b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('34 a 38'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value34a38b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value34a38 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value34a38++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value34a38b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value34a38 =
                                                  widget.value34a38 == 0
                                                      ? 0
                                                      : widget.value34a38 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('34 a 38',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value34a38.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value34a38 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 5) {
                                  return _value39a43b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('39 a 43'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value39a43b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value39a43 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value39a43++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value39a43b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value39a43 =
                                                  widget.value39a43 == 0
                                                      ? 0
                                                      : widget.value39a43 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('39 a 43',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value39a43.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value39a43 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 6) {
                                  return _value44a48b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('44 a 48'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value44a48b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value44a48 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value44a48++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value44a48b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value44a48 =
                                                  widget.value44a48 == 0
                                                      ? 0
                                                      : widget.value44a48 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('44 a 48',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value44a48.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value44a48 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 7) {
                                  return _value49a53b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('49 a 53'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value49a53b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value49a53 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value49a53++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value49a53b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value49a53 =
                                                  widget.value49a53 == 0
                                                      ? 0
                                                      : widget.value49a53 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('49 a 53',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value49a53.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value49a53 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 8) {
                                  return _value54a58b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('54 a 58'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value54a58b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value54a58 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value54a58++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value54a58b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value54a58 =
                                                  widget.value54a58 == 0
                                                      ? 0
                                                      : widget.value54a58 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('54 a 58',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value54a58.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget
                                                                  .value54a58 <=
                                                              0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else if (index == 10) {
                                  return _value59b
                                      ? Container(
                                          margin: const EdgeInsets.all(5.0),
                                          padding: const EdgeInsets.only(
                                              left: 8, right: 8, bottom: 8),
                                          decoration: BoxDecoration(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.grey[600]
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            border: Border.all(
                                              color: Colors.grey[200]!,
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey[200]!,
                                                blurRadius: 2.0,
                                                spreadRadius: 1.0,
                                                offset: const Offset(0, 1),
                                              ),
                                            ],
                                          ),
                                          child: TextFormField(
                                            style: const TextStyle(
                                              fontSize: 12,
                                            ),
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                label: const Text('59+'),
                                                labelStyle: const TextStyle(
                                                    fontSize: 9),
                                                suffixIcon: GestureDetector(
                                                    onTap: () async {
                                                      setState(() {
                                                        _value59b = false;
                                                      });
                                                    },
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 16,
                                                    ))),
                                            onChanged: (value) async {
                                              setState(() {
                                                widget.value59 =
                                                    int.parse(value);
                                              });
                                            },
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () async {
                                            setState(() {
                                              widget.value59++;
                                            });
                                          },
                                          onLongPress: () async {
                                            setState(() {
                                              _value59b = true;
                                            });
                                          },
                                          onVerticalDragEnd: (details) async {
                                            setState(() {
                                              widget.value59 =
                                                  widget.value59 == 0
                                                      ? 0
                                                      : widget.value59 - 1;
                                            });
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.all(5.0),
                                            padding: const EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(
                                              color: Provider.of<ThemeState>(
                                                          context,
                                                          listen: true)
                                                      .isDarkMode
                                                  ? Colors.grey[600]
                                                  : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              border: Border.all(
                                                color: Colors.grey[200]!,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[200]!,
                                                  blurRadius: 2.0,
                                                  spreadRadius: 1.0,
                                                  offset: const Offset(0, 1),
                                                ),
                                              ],
                                            ),
                                            child: Stack(
                                              children: [
                                                const Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: Text('59+',
                                                        style: TextStyle(
                                                            fontSize: 10))),
                                                Center(
                                                    child: Text(
                                                  widget.value59.toString(),
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      color: widget.value59 <= 0
                                                          ? Colors.grey
                                                          : Provider.of<ThemeState>(
                                                                      context,
                                                                      listen:
                                                                          true)
                                                                  .isDarkMode
                                                              ? Colors.blue[100]
                                                              : Colors
                                                                  .blue[600]),
                                                )),
                                                const SizedBox(
                                                  height: 1,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                } else {
                                  return const Center();
                                }
                              }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
