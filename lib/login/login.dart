// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/constants.dart';
import 'package:simplite/home/home.dart';
import 'package:url_launcher/url_launcher.dart';

import '../api/api.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isPasswordVisible = false;
  final _tLogin = TextEditingController();
  final _tSenha = TextEditingController();
  bool isLogging = false;
  final _formKey = GlobalKey<FormState>();
  String? email;
  String? password;
  final List<String?> errors = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      isLogging = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              color: Colors.blue,
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network(
                          'https://app.simpsimuladoresonline.com.br/img/logo1.png',
                          width: 150),
                    ),
                  ],
                ),
              ),
            ),
            Lottie.asset('assets/json.json',
                width: MediaQuery.of(context).size.width),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: SizedBox(
                  width: 400,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextFormField(
                          controller: _tLogin,
                          decoration: const InputDecoration(
                            hintText: 'Digite seu e-mail',
                            prefixIcon: Icon(Icons.email),
                            labelText: 'Email',
                          ),
                        ),
                        const SizedBox(height: 10.0),
                        TextFormField(
                          controller: _tSenha,
                          obscureText: !_isPasswordVisible,
                          decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.lock),
                            labelText: 'Senha',
                            hintText: 'Digite sua senha',
                            suffixIcon: IconButton(
                              icon: Icon(
                                _isPasswordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                              onPressed: () {
                                setState(() {
                                  _isPasswordVisible = !_isPasswordVisible;
                                });
                              },
                            ),
                          ),
                        ),
                        const SizedBox(height: 10.0),
                        Align(
                          alignment: Alignment.centerRight,
                          child: TextButton(
                            onPressed: () async {
                              if (!await launchUrl(
                                  Uri.parse(
                                      'https://b2cor-crm.agencialink.com.br/login/esqueceu.php'),
                                  mode: LaunchMode.externalApplication)) {
                                throw Exception('Could not launch');
                              }
                            },
                            child: const Text(
                              'Esqueceu a senha?',
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10.0),
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton(
                            onPressed: () async {
                              setState(() {
                                isLogging = true;
                              });
                              var prefs = await SharedPreferences.getInstance();

                              final login = _tLogin.text;
                              final senha = _tSenha.text;
                              final String onesignalUserId =
                                  prefs.getString("login") ?? "";
                              //const onesignalUserId = '145623';

                              if (login.isEmpty || senha.isEmpty) {
                                alert2(context, "Preencha os Dados");
                                setState(() {
                                  isLogging = false;
                                });
                              } else {
                                var usuario = await LoginApi.login(
                                    login, senha, onesignalUserId, 1);
                                // ignore: unnecessary_null_comparison
                                if (usuario != null) {
                                  setState(() {
                                    isLogging = false;
                                  });
                                  await prefs.setString("login", login);
                                  await prefs.setString("senha", senha);
                                  await prefs.setBool("isLoggedIn", true);
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => MyHomePage(
                                                nomeDoCliente: '',
                                                cidadeEstado: '',
                                                custoDoPlanoAtualValorInicial:
                                                    '0',
                                                value0a18: 0,
                                                value19a23: 0,
                                                value24a28: 0,
                                                value29a33: 0,
                                                value34a38: 0,
                                                value39a43: 0,
                                                value44a48: 0,
                                                value49a53: 0,
                                                value54a58: 0,
                                                value59: 0,
                                                idDoCliente: '',
                                                emailDoCliente: '',
                                                celularDoCliente: '',
                                              )));
                                } else {
                                  setState(() {
                                    isLogging = false;
                                  });
                                  String msg = (prefs.getString("msg") ?? "");
                                  alert(context, msg);
                                }
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(defaultPadding),
                              child: isLogging
                                  ? const LinearProgressIndicator()
                                  : const Text('Login'),
                            ),
                          ),
                        ),
                        const SizedBox(height: 20.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(defaultPadding),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.blue,
                                ),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.all(defaultPaddingMini),
                                  child: IconButton(
                                    icon: const FaIcon(
                                        FontAwesomeIcons.facebook,
                                        color: Colors.white),
                                    onPressed: () async {
                                      if (!await launchUrl(
                                          Uri.parse(
                                              'https://www.facebook.com/PortalMultiCalculo/'),
                                          mode:
                                              LaunchMode.externalApplication)) {
                                        throw Exception('Could not launch');
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(defaultPadding),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.blue,
                                ),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.all(defaultPaddingMini),
                                  child: IconButton(
                                    icon: const FaIcon(
                                        FontAwesomeIcons.instagram,
                                        color: Colors.white),
                                    onPressed: () async {
                                      if (!await launchUrl(
                                          Uri.parse(
                                              'https://www.instagram.com/agencialink.com.br/'),
                                          mode:
                                              LaunchMode.externalApplication)) {
                                        throw Exception('Could not launch');
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(defaultPadding),
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.blue,
                                ),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.all(defaultPaddingMini),
                                  child: IconButton(
                                    icon: const FaIcon(FontAwesomeIcons.youtube,
                                        color: Colors.white),
                                    onPressed: () async {
                                      if (!await launchUrl(
                                          Uri.parse(
                                              'https://www.youtube.com/channel/UCASWLAKfTzb0Z1EJAxVGDqw'),
                                          mode:
                                              LaunchMode.externalApplication)) {
                                        throw Exception('Could not launch');
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: defaultPadding,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                              child: IconButton(
                                  onPressed: () {
                                    var themeState = Provider.of<ThemeState>(
                                        context,
                                        listen: false);
                                    themeState.toggleTheme();
                                  },
                                  icon: FaIcon(
                                      Provider.of<ThemeState>(context,
                                                  listen: true)
                                              .isDarkMode
                                          ? FontAwesomeIcons.sun
                                          : FontAwesomeIcons.moon,
                                      color: Provider.of<ThemeState>(context,
                                                  listen: true)
                                              .isDarkMode
                                          ? Colors.white
                                          : Colors.grey)),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

alert(BuildContext context, String msg) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(8.0), // Seu valor de borderRadius aqui
            ),
            title: Center(
                child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FaIcon(
                    FontAwesomeIcons.faceSadTear,
                    size: 90,
                    color: Colors.red[700],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Oops...",
                    style: TextStyle(color: Colors.red[700], fontSize: 25),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            )),
            content: Text(
              msg,
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Tentar Novamente'))
            ]);
      });
}

alert2(BuildContext context, String msg) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(8.0), // Seu valor de borderRadius aqui
            ),
            title: Center(
                child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FaIcon(
                    FontAwesomeIcons.penToSquare,
                    size: 90,
                    color: Colors.blue[900],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Oops...",
                    style: TextStyle(color: Colors.blue[900], fontSize: 25),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            )),
            content: Text(
              msg,
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Tentar Novamente'))
            ]);
      });
}
