import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/api/cotacoes_api.dart';
import 'package:http/http.dart' as http;
import 'package:simplite/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class CotacoesEnviadas extends StatefulWidget {
  const CotacoesEnviadas({super.key});

  static TextEditingController searchController = TextEditingController();
  static Widget statusDaMensagem = CotacoesTodas(
    searchController: CotacoesEnviadas.searchController,
  );
  static bool isLoadinDetalhesDaCotacao = false;

  @override
  State<CotacoesEnviadas> createState() => _CotacoesEnviadasState();
}

class _CotacoesEnviadasState extends State<CotacoesEnviadas> {
  bool isTodas = true;
  bool isLidas = false;

  Future<String> _fetchTotalDeCotacoes() async {
    var prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('usr_tkn')!;
    var header = {'Authorization': 'Bearer $token'};
    var response = await http.post(
        Uri.parse(
            "https://api-simp.agencialink.com.br/dashboard/cotacoes/total"),
        headers: header);
    if (response.statusCode == 200) {
      var mapResponse = json.decode(response.body);
      return mapResponse["dados"]["total"].toString();
    } else {
      return 'erro';
    }
  }

  @override
  void initState() {
    super.initState();
    CotacoesEnviadas.statusDaMensagem = CotacoesTodas(
      searchController: CotacoesEnviadas.searchController,
    );
    CotacoesEnviadas.isLoadinDetalhesDaCotacao = false;
    _fetchTotalDeCotacoes();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      insetPadding: const EdgeInsets.all(5.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Wrap(
              alignment: WrapAlignment.spaceBetween,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                const Text(
                  'Cotações',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.close),
                ),
                const Row()
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                  style: isTodas
                      ? const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.blue),
                          textStyle: MaterialStatePropertyAll(
                              TextStyle(color: Colors.white)),
                          iconColor: MaterialStatePropertyAll(Colors.white))
                      : const ButtonStyle(),
                  onPressed: () async {
                    setState(() {
                      CotacoesEnviadas.statusDaMensagem = CotacoesTodas(
                        searchController: CotacoesEnviadas.searchController,
                      );
                      isTodas = true;
                      isLidas = false;
                    });
                  },
                  child: Row(
                    children: [
                      const FaIcon(FontAwesomeIcons.paperPlane, size: 13),
                      const SizedBox(
                        width: 6,
                      ),
                      Text('Todas',
                          style: isTodas
                              ? const TextStyle(color: Colors.white)
                              : const TextStyle()),
                    ],
                  )),
              ElevatedButton(
                  style: isLidas
                      ? const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.blue),
                          textStyle: MaterialStatePropertyAll(
                              TextStyle(color: Colors.white)),
                          iconColor: MaterialStatePropertyAll(Colors.white))
                      : const ButtonStyle(),
                  onPressed: () async {
                    setState(() {
                      CotacoesEnviadas.statusDaMensagem = CotacoesLidasClass(
                        searchController: CotacoesEnviadas.searchController,
                      );
                      isTodas = false;
                      isLidas = true;
                    });
                  },
                  child: Row(
                    children: [
                      const FaIcon(FontAwesomeIcons.envelopeOpen, size: 13),
                      const SizedBox(
                        width: 6,
                      ),
                      Text('Visualizadas',
                          style: isLidas
                              ? const TextStyle(color: Colors.white)
                              : const TextStyle()),
                    ],
                  )),
            ],
          ),
          const SizedBox(
            height: defaultPadding,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFormField(
              controller: CotacoesEnviadas.searchController,
              decoration: InputDecoration(
                  hintText: 'Pesquisar...',
                  prefixIcon: CotacoesEnviadas.searchController.text == ''
                      ? IconButton(
                          onPressed: () {
                            setState(() {});
                          },
                          icon: const Icon(Icons.search))
                      : IconButton(
                          onPressed: () async {
                            setState(() {
                              CotacoesEnviadas.searchController.text = '';
                            });
                            setState(() {
                              CotacoesEnviadas.statusDaMensagem ==
                                      CotacoesBusca2(
                                        searchController:
                                            CotacoesEnviadas.searchController,
                                      )
                                  ? CotacoesEnviadas.statusDaMensagem =
                                      CotacoesBusca1(
                                      searchController:
                                          CotacoesEnviadas.searchController,
                                    )
                                  : CotacoesEnviadas.statusDaMensagem =
                                      CotacoesBusca2(
                                      searchController:
                                          CotacoesEnviadas.searchController,
                                    );
                            });
                          },
                          icon: const Icon(Icons.close))),
              onChanged: (value) async {
                setState(() {
                  CotacoesEnviadas.searchController.text = value;
                });
                setState(() {
                  CotacoesEnviadas.statusDaMensagem ==
                          CotacoesBusca2(
                            searchController: CotacoesEnviadas.searchController,
                          )
                      ? CotacoesEnviadas.statusDaMensagem = CotacoesBusca1(
                          searchController: CotacoesEnviadas.searchController,
                        )
                      : CotacoesEnviadas.statusDaMensagem = CotacoesBusca2(
                          searchController: CotacoesEnviadas.searchController,
                        );
                });
              },
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: CotacoesEnviadas.statusDaMensagem,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8, left: 8, bottom: 8),
            child: ElevatedButton(
                onPressed: () async {
                  var prefs = await SharedPreferences.getInstance();
                  String token = prefs.getString('usr_tkn') ?? '';
                  if (!await launchUrl(
                      Uri.parse(
                          'https://barra.agencialink.com.br/autologin/$token/simp_dashboard?destino=cotacoes'),
                      mode: LaunchMode.externalApplication)) {
                    throw Exception('Could not launch');
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FutureBuilder(
                        future: _fetchTotalDeCotacoes(),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) =>
                                snapshot.hasData
                                    ? Text(
                                        'Total de Cotações: ${snapshot.data}',
                                        style: const TextStyle(fontSize: 12),
                                      )
                                    : const Text(
                                        '',
                                        style: TextStyle(fontSize: 12),
                                      )),
                    const Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text(
                          'Ver todas',
                          style: TextStyle(fontSize: 12),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.open_in_new,
                          size: 12,
                        ),
                      ],
                    )
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
