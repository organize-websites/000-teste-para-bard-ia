import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:webviewx_plus/webviewx_plus.dart';

class VisualizarCotacao extends StatefulWidget {
  final String urlDaNotificacao;
  final String hashUrl;
  const VisualizarCotacao(
      {super.key, required this.urlDaNotificacao, required this.hashUrl});

  @override
  State<VisualizarCotacao> createState() => _VisualizarCotacaoState();
}

class _VisualizarCotacaoState extends State<VisualizarCotacao> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.chevron_left)),
        title: ListTile(
          contentPadding: const EdgeInsets.all(1),
          title: Marquee(
            child: const Text(
              'Link da Cotação',
              style: TextStyle(fontSize: 9),
            ),
          ),
          subtitle: Marquee(
            child: Text(
              widget.urlDaNotificacao,
              style: const TextStyle(fontSize: 12),
            ),
          ),
        ),
        actions: [
          IconButton(
              onPressed: () {
                FlutterClipboard.copy(widget.urlDaNotificacao);
              },
              icon: const Icon(Icons.copy)),
        ],
      ),
      body: SafeArea(
          child: Center(
        child: WebViewX(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          initialContent:
              'https://profissional-simp.agencialink.com.br/cotacao/${widget.hashUrl}?source=iframe',
        ),
      )),
    );
  }
}
