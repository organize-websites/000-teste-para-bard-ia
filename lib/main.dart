import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simplite/constants.dart';
import 'package:simplite/splash/splash.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (_) => ThemeState(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static const String _title = 'Simp Simuladores';
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeState>(builder: (context, themeState, _) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: _title,
        theme: ThemeData(
            dialogBackgroundColor: Colors.white,
            dialogTheme: const DialogTheme(
                backgroundColor: Colors.white, surfaceTintColor: Colors.white),
            checkboxTheme: const CheckboxThemeData(
                overlayColor: MaterialStatePropertyAll(Colors.grey)),
            primaryIconTheme: const IconThemeData(color: Colors.grey),
            iconTheme: const IconThemeData(color: Colors.grey),
            inputDecorationTheme: InputDecorationTheme(
              prefixIconColor: Colors.grey,
              suffixIconColor: Colors.grey,
              iconColor: Colors.grey,
              labelStyle: const TextStyle(color: Colors.grey),
              hintStyle: const TextStyle(color: Colors.grey),
              prefixStyle: const TextStyle(color: Colors.grey),
              suffixStyle: const TextStyle(color: Colors.grey),
              contentPadding: const EdgeInsets.all(2),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Colors.grey[200]!),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Colors.grey[300]!),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Colors.grey[300]!),
              ),
            ),
            colorSchemeSeed: Colors.blue,
            useMaterial3: true,
            brightness: Brightness.light),
        darkTheme: ThemeData(
            inputDecorationTheme: InputDecorationTheme(
              contentPadding: const EdgeInsets.all(2),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Colors.grey[200]!),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Colors.grey[300]!),
              ),
            ),
            colorSchemeSeed: Colors.blue,
            useMaterial3: true,
            brightness: Brightness.dark),
        themeMode: themeState.isDarkMode ? ThemeMode.dark : ThemeMode.light,
        home: const Splash(),
      );
    });
  }
}
