import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simplite/api/notificacoes_api.dart';

class NotificacoesModal extends StatefulWidget {
  const NotificacoesModal({super.key});

  static Widget statusDaMensagem = const NotificacoesNaoLidasClass();
  @override
  State<NotificacoesModal> createState() => _NotificacoesModalState();
}

class _NotificacoesModalState extends State<NotificacoesModal> {
  bool isTodas = false;
  bool isNaoLidas = true;
  bool isLidas = false;

  @override
  void initState() {
    super.initState();
    NotificacoesModal.statusDaMensagem = const NotificacoesNaoLidasClass();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius:
            BorderRadius.circular(8.0), // Seu valor de borderRadius aqui
      ),
      insetPadding: const EdgeInsets.all(5.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Flexible(
                  flex: 3,
                  child: Text(
                    'Notificações',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Flexible(
                    flex: 2,
                    child: GestureDetector(
                      onTap: () async {
                        await marcarTodasComoLidas();
                        setState(() {
                          NotificacoesModal.statusDaMensagem =
                              const NotificacoesNaoLidasClass();
                          isTodas = false;
                          isNaoLidas = true;
                          isLidas = false;
                        });
                      },
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          FaIcon(FontAwesomeIcons.envelopeOpen,
                              color: Colors.grey, size: 10),
                          SizedBox(
                            width: 5,
                          ),
                          Text('Marcar tudo lido',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 11))
                        ],
                      ),
                    )),
                Flexible(
                  flex: 1,
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.close),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                  style: isTodas
                      ? const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.blue),
                          textStyle: MaterialStatePropertyAll(
                              TextStyle(color: Colors.white)),
                          iconColor: MaterialStatePropertyAll(Colors.white))
                      : const ButtonStyle(),
                  onPressed: () async {
                    setState(() {
                      NotificacoesModal.statusDaMensagem =
                          const NotificacoesTodas();
                      isTodas = true;
                      isNaoLidas = false;
                      isLidas = false;
                    });
                  },
                  child: Text('Todas',
                      style: isTodas
                          ? const TextStyle(color: Colors.white)
                          : const TextStyle())),
              ElevatedButton(
                  style: isNaoLidas
                      ? const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.blue),
                          textStyle: MaterialStatePropertyAll(
                              TextStyle(color: Colors.white)),
                          iconColor: MaterialStatePropertyAll(Colors.white))
                      : const ButtonStyle(),
                  onPressed: () async {
                    setState(() {
                      NotificacoesModal.statusDaMensagem =
                          const NotificacoesNaoLidasClass();
                      isTodas = false;
                      isNaoLidas = true;
                      isLidas = false;
                    });
                  },
                  child: Row(
                    children: [
                      const FaIcon(FontAwesomeIcons.envelope, size: 13),
                      const SizedBox(
                        width: 6,
                      ),
                      Text('Não Lidas',
                          style: isNaoLidas
                              ? const TextStyle(color: Colors.white)
                              : const TextStyle()),
                    ],
                  )),
              ElevatedButton(
                  style: isLidas
                      ? const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.blue),
                          textStyle: MaterialStatePropertyAll(
                              TextStyle(color: Colors.white)),
                          iconColor: MaterialStatePropertyAll(Colors.white))
                      : const ButtonStyle(),
                  onPressed: () async {
                    setState(() {
                      NotificacoesModal.statusDaMensagem =
                          const NotificacoesLidasClass();
                      isTodas = false;
                      isNaoLidas = false;
                      isLidas = true;
                    });
                  },
                  child: Row(
                    children: [
                      const FaIcon(FontAwesomeIcons.envelopeOpen, size: 13),
                      const SizedBox(
                        width: 6,
                      ),
                      Text('Lidas',
                          style: isLidas
                              ? const TextStyle(color: Colors.white)
                              : const TextStyle()),
                    ],
                  )),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: NotificacoesModal.statusDaMensagem,
            ),
          ),
        ],
      ),
    );
  }
}
