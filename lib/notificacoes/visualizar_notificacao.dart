import 'package:flutter/material.dart';
import 'package:webviewx_plus/webviewx_plus.dart';

class VisualizarNotificacao extends StatefulWidget {
  final String urlDaNotificacao;
  const VisualizarNotificacao({super.key, required this.urlDaNotificacao});

  @override
  State<VisualizarNotificacao> createState() => _VisualizarNotificacaoState();
}

class _VisualizarNotificacaoState extends State<VisualizarNotificacao> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: WebViewX(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          initialContent: widget.urlDaNotificacao,
        ),
      )),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.close),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }
}
