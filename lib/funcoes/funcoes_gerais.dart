import 'package:shared_preferences/shared_preferences.dart';

Future<List<String>> dadosDoUsuario() async {
  var prefs = await SharedPreferences.getInstance();
  String nome = prefs.getString('nome') ?? '';
  String email = prefs.getString('login') ?? '';
  String foto = prefs.getString('logotipousuario') ?? '';
  return [nome, email, foto];
}
