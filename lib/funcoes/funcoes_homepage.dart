import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/modelos/modelos.dart';
import 'package:http/http.dart' as http;

//FUNÇÕES DA HOMEPAGE

Future<List<dynamic>> buscarCidades(String uf, String busca) async {
  const String baseUrl = 'https://api-simp.agencialink.com.br/cidades';
  final String url = '$baseUrl/$uf?busca=$busca';

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    return json.decode(response.body);
  } else {
    throw Exception('Falha ao carregar as cidades');
  }
}

Future<List<Lead>> fetchLeads(String search) async {
  var prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('tokenjwt2') ?? '';
  String id = prefs.getString('id') ?? '';
  final url = Uri.parse('https://b2corapi.agencialink.com.br/lead/find');

  final response = await http.post(url,
      headers: {'x-api-key': token, 'Content-Type': 'application/json'},
      body: json.encode({
        "search": search.length < 3
            ? ""
            : RegExp(r'^[0-9]').hasMatch(search)
                ? ""
                : search,
        "user": id
      }));

  if (response.statusCode == 200) {
    final List<dynamic> jsonData = json.decode(response.body);
    jsonData.sort((a, b) {
      DateTime dataA = DateTime.parse(a["data"]);
      DateTime dataB = DateTime.parse(b["data"]);
      return dataB
          .compareTo(dataA); // Ordena do mais recente para o mais antigo
    });
    return jsonData
        .map((item) => Lead(
            name: item['nome'],
            email: item['email'],
            data: DateFormat('dd/MM/yyyy - HH:mm:ss')
                .format(DateTime.parse(item['data']))
                .toString(),
            id: item['id_cliente'],
            celular: item['celular']))
        .toList();
  } else {
    throw Exception('Failed to load leads');
  }
}
