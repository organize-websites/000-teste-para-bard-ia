import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:http/http.dart' as http;
import 'package:simplite/constants.dart';
import 'package:simplite/operadora/operadora.dart';

//FUNÇÕES ADESÃO

Future<List<dynamic>> buscarAdministradoras(String uf, String busca,
    String saudeOuOdonto, String pfOuPme, String idDaCidade) async {
  const String baseUrl =
      'https://api-simp.agencialink.com.br/administradoras_autocomplete';
  final String url = '$baseUrl/$uf/?busca=$busca';

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    final List<dynamic> administradoras = json.decode(response.body);
    try {
      return List.generate(
          administradoras.length,
          (index) => administradoras[index]["nome"]
                  .toString()
                  .toLowerCase()
                  .contains(busca.toLowerCase())
              ? '${administradoras[index]["nome"]} - (${administradoras[index]["id"]})'
              : '');
    } catch (e) {
      return ['Não foi possível buscar'];
    }
  } else {
    throw Exception('Falha ao carregar as administradoras');
  }
}

Future<List<dynamic>> buscarProfissoes(
    String uf, String busca, String idDaCidade) async {
  const String baseUrl = 'https://api-simp.agencialink.com.br/profissoes';
  final String url = '$baseUrl/$uf/?busca=$busca';

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    final List<dynamic> profissoes = json.decode(response.body);
    try {
      return List.generate(
          profissoes.length,
          (index) =>
              '${profissoes[index]["nome"]} - (${profissoes[index]["id"]})');
    } catch (e) {
      return ['Não foi possível buscar'];
    }
  } else {
    throw Exception('Falha ao carregar as profissões');
  }
}

Future<List<dynamic>> buscarEntidades(
    String uf, String busca, String idDaCidade) async {
  const String baseUrl = 'https://api-simp.agencialink.com.br/entidades';
  final String url = '$baseUrl/$uf/?busca=$busca';

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    final List<dynamic> profissoes = json.decode(response.body);
    try {
      return List.generate(
          profissoes.length,
          (index) =>
              '${profissoes[index]["nome"]} - (${profissoes[index]["id"]})');
    } catch (e) {
      return ['Não foi possível buscar'];
    }
  } else {
    throw Exception('Falha ao carregar as profissões');
  }
}

modalFiltrosAdesao(
    BuildContext context,
    TextEditingController adesaoAdministradoraController,
    TextEditingController adesaoProfissoesController,
    TextEditingController adesaoEntidadesController,
    String nomedocliente,
    String cidade,
    String idDaCidade,
    String uf,
    String valoratual,
    String i0a18,
    String i19a23,
    String i24a28,
    String i29a33,
    String i34a38,
    String i39a43,
    String i44a48,
    String i49a53,
    String i54a58,
    String i59,
    String totalDeVidas,
    String padraoDeConfortoFiltro,
    String reembolsoFiltro,
    String atendimentoFiltro,
    String abrangenciaFiltro,
    String coparticipacaoFiltro,
    String meiFiltro,
    String regrasDeAceitacaoFiltro,
    String somarTaxasFiltro,
    String padraoDeConforto,
    String reembolso,
    String atendimento,
    String abrangencia,
    String coparticipacao,
    String mei,
    String somarTaxas,
    String regrasDeAcetacao,
    int selectedSaudeIndex, // = 0;
    int selectedPlanoIndex, // = 2;
    String saudeOuOdonto,
    String pfOuPme,
    List<String> credenciados,
    List<String> produtos,
    List<String> operadoras,
    String mesDeReajuste,
    String indiceDeReajuste,
    String valorReajustado,
    String observacoesDeReajuste,
    String emailDoCliente,
    String idDoCliente,
    String celularDoCliente,
    List<String> listaDeResumoDeValores,
    String qualAdministradora,
    String qualProfissao,
    String qualEntidade) async {
  bool isAbertoEntidades = false;
  bool isAbertoProfissoes = false;
  bool isAbertoAdministradoras = false;
  SuggestionsBoxController fecharEntidades = SuggestionsBoxController();
  SuggestionsBoxController fecharProfissoes = SuggestionsBoxController();
  SuggestionsBoxController fecharAdministradoras = SuggestionsBoxController();
  adesaoAdministradoraController.text = qualAdministradora;
  adesaoProfissoesController.text = qualProfissao;
  adesaoEntidadesController.text = qualEntidade;
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(8.0), // Seu valor de borderRadius aqui
            ),
            insetPadding: const EdgeInsets.all(8),
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Filtros por Adesão'),
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.close))
                ]),
            content: SingleChildScrollView(
                child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                ),
                TypeAheadFormField(
                  hideOnError: true,
                  hideSuggestionsOnKeyboardHide: true,
                  onSuggestionsBoxToggle: (p0) {
                    p0
                        ? setState(() {
                            isAbertoAdministradoras = true;
                          })
                        : null;
                  },
                  textFieldConfiguration: TextFieldConfiguration(
                    controller: adesaoAdministradoraController,
                    decoration: InputDecoration(
                        labelText: 'Selecione uma Administradora',
                        prefixIcon: const Icon(Icons.business),
                        labelStyle: const TextStyle(fontSize: 14),
                        suffixIcon: adesaoAdministradoraController.text != '' ||
                                isAbertoAdministradoras
                            ? IconButton(
                                onPressed: () async {
                                  setState(() {
                                    adesaoAdministradoraController.text = '';
                                    isAbertoAdministradoras = false;
                                  });
                                  fecharAdministradoras.close();
                                },
                                icon: const Icon(Icons.close))
                            : const Icon(Icons.search)),
                  ),
                  suggestionsBoxController: fecharAdministradoras,
                  suggestionsCallback: (pattern) async {
                    return await buscarAdministradoras(
                        uf, pattern, saudeOuOdonto, pfOuPme, idDaCidade);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: suggestion == ''
                          ? const SizedBox.shrink()
                          : Text(suggestion.toString()),
                    );
                  },
                  onSuggestionSelected: (suggestion) async {
                    setState(() {
                      adesaoAdministradoraController.text = suggestion;
                    });
                  },
                  noItemsFoundBuilder: (context) {
                    return const ListTile(
                      title: Text('Nenhum resultado encontrado'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Por favor, selecione uma Administradora';
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: defaultPadding,
                ),
                TypeAheadFormField(
                  hideOnError: true,
                  hideSuggestionsOnKeyboardHide: true,
                  onSuggestionsBoxToggle: (p0) {
                    p0
                        ? setState(() {
                            isAbertoProfissoes = true;
                          })
                        : null;
                  },
                  textFieldConfiguration: TextFieldConfiguration(
                    controller: adesaoProfissoesController,
                    decoration: InputDecoration(
                        labelText: 'Selecione uma Profissão',
                        prefixIcon: const Icon(Icons.work),
                        labelStyle: const TextStyle(fontSize: 14),
                        suffixIcon: adesaoProfissoesController.text != '' ||
                                isAbertoProfissoes
                            ? IconButton(
                                onPressed: () async {
                                  setState(() {
                                    adesaoProfissoesController.text = '';
                                    isAbertoProfissoes = false;
                                  });
                                  fecharProfissoes.close();
                                },
                                icon: const Icon(Icons.close))
                            : const Icon(Icons.search)),
                  ),
                  suggestionsBoxController: fecharProfissoes,
                  suggestionsCallback: (pattern) async {
                    return await buscarProfissoes(uf, pattern, idDaCidade);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text(suggestion.toString()),
                    );
                  },
                  onSuggestionSelected: (suggestion) async {
                    setState(() {
                      adesaoProfissoesController.text = suggestion;
                    });
                  },
                  noItemsFoundBuilder: (context) {
                    return const ListTile(
                      title: Text('Nenhum resultado encontrado'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Por favor, selecione uma Profissão';
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: defaultPadding,
                ),
                TypeAheadFormField(
                  hideOnError: true,
                  hideSuggestionsOnKeyboardHide: true,
                  onSuggestionsBoxToggle: (p0) {
                    p0
                        ? setState(() {
                            isAbertoEntidades = true;
                          })
                        : null;
                  },
                  textFieldConfiguration: TextFieldConfiguration(
                    controller: adesaoEntidadesController,
                    decoration: InputDecoration(
                        labelText: 'Selecione uma Entidade',
                        prefixIcon: const Icon(Icons.supervised_user_circle),
                        labelStyle: const TextStyle(fontSize: 14),
                        suffixIcon: adesaoEntidadesController.text != '' ||
                                isAbertoEntidades
                            ? IconButton(
                                onPressed: () async {
                                  setState(() {
                                    adesaoEntidadesController.text = '';
                                    isAbertoEntidades = false;
                                  });
                                  fecharEntidades.close();
                                },
                                icon: const Icon(Icons.close))
                            : const Icon(Icons.search)),
                  ),
                  suggestionsCallback: (pattern) async {
                    return await buscarEntidades(uf, pattern, idDaCidade);
                  },
                  suggestionsBoxController: fecharEntidades,
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text(suggestion.toString()),
                    );
                  },
                  onSuggestionSelected: (suggestion) async {
                    setState(() {
                      adesaoEntidadesController.text = suggestion;
                    });
                  },
                  noItemsFoundBuilder: (context) {
                    return const ListTile(
                      title: Text('Nenhum resultado encontrado'),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Por favor, selecione uma Entidade';
                    }
                    return null;
                  },
                ),
              ],
            )),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      adesaoEntidadesController.text = '';
                      adesaoProfissoesController.text = '';
                      adesaoAdministradoraController.text = '';
                    });
                  },
                  child: const Text(
                    'Limpar',
                    style: TextStyle(color: Colors.grey),
                  )),
              ElevatedButton(
                  onPressed: () async {
                    if (adesaoAdministradoraController.text.isEmpty &&
                        adesaoEntidadesController.text.isEmpty &&
                        adesaoProfissoesController.text.isEmpty) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog.adaptive(
                              title: const Text(
                                  'Selecione ao menos um dos filtros para continuar'),
                              actions: [
                                ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: const Text('Fechar'))
                              ],
                            );
                          });
                    } else {
                      setState(() {
                        saudeOuOdonto = "a";
                        pfOuPme = "pf";
                      });
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EscolhaAOperadora(
                                    nomedocliente: nomedocliente,
                                    cidade: cidade,
                                    idDaCidade: idDaCidade,
                                    uf: uf,
                                    valoratual: valoratual,
                                    i0a18: i0a18,
                                    i19a23: i19a23,
                                    i24a28: i24a28,
                                    i29a33: i29a33,
                                    i34a38: i34a38,
                                    i39a43: i39a43,
                                    i44a48: i44a48,
                                    i49a53: i49a53,
                                    i54a58: i54a58,
                                    i59: i59,
                                    totalDeVidas: totalDeVidas,
                                    selectedSaudeIndex: selectedSaudeIndex,
                                    selectedPlanoIndex: selectedPlanoIndex,
                                    saudeOuOdonto: saudeOuOdonto,
                                    pfOuPme: pfOuPme,
                                    meiFiltro: meiFiltro,
                                    reembolsoFiltro: reembolsoFiltro,
                                    somarTaxasFiltro: somarTaxasFiltro,
                                    abrangenciaFiltro: abrangenciaFiltro,
                                    atendimentoFiltro: atendimentoFiltro,
                                    coparticipacaoFiltro: coparticipacaoFiltro,
                                    padraoDeConfortoFiltro:
                                        padraoDeConfortoFiltro,
                                    regrasDeAceitacaoFiltro:
                                        regrasDeAceitacaoFiltro,
                                    coparticipacao: coparticipacao,
                                    padraoDeConforto: padraoDeConforto,
                                    reembolso: reembolso,
                                    abrangencia: abrangencia,
                                    atendimento: atendimento,
                                    mei: mei,
                                    somarTaxas: somarTaxas,
                                    regrasDeAcetacao: regrasDeAcetacao,
                                    isAdesaoAgora: true,
                                    credenciado: credenciados,
                                    produtosFiltro: produtos,
                                    operadoras: operadoras,
                                    entidade: adesaoEntidadesController.text
                                        .split('- (')
                                        .last
                                        .split(')')
                                        .first,
                                    profissao: adesaoProfissoesController.text
                                        .split('- (')
                                        .last
                                        .split(')')
                                        .first,
                                    administradora:
                                        adesaoAdministradoraController.text
                                            .split('- (')
                                            .last
                                            .split(')')
                                            .first,
                                    mesDeReajuste: mesDeReajuste,
                                    indiceDeReajuste: indiceDeReajuste,
                                    valorReajustado: valorReajustado,
                                    observacoesDeReajuste:
                                        observacoesDeReajuste,
                                    emailDoCliente: emailDoCliente,
                                    idDoCliente: idDoCliente,
                                    celularDoCliente: celularDoCliente,
                                    qualAdministradora:
                                        adesaoAdministradoraController.text,
                                    qualProfissao:
                                        adesaoProfissoesController.text,
                                    qualEntidade:
                                        adesaoEntidadesController.text,
                                  )));
                    }
                  },
                  child: const Text('Aplicar')),
            ],
          );
        });
      });
}

alertaProfissoesDaEntidade(BuildContext context, String id, String uf) async {
  var response = await http
      .get(Uri.parse('https://api-simp.agencialink.com.br/entidade/$uf/$id'));
  if (response.statusCode == 200) {
    var mapResponse = json.decode(response.body);
    String nomeDaEntidade = mapResponse["0"]["descricao"];
    // ignore: use_build_context_synchronously
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding:
                const EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            insetPadding: const EdgeInsets.all(8),
            title: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(Icons.close))
                  ],
                ),
                Text(
                  nomeDaEntidade,
                  style: const TextStyle(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            content: SizedBox(
              height: MediaQuery.of(context).size.height - 20,
              width: MediaQuery.of(context).size.width - 20,
              child: SingleChildScrollView(
                  child: Wrap(
                children: [
                  const Divider(),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(
                          mapResponse["profissoes"].length,
                          (index) =>
                              Text(mapResponse["profissoes"][index]["nome"]))),
                ],
              )),
            ),
          );
        });
  } else {
    return null;
  }
}
