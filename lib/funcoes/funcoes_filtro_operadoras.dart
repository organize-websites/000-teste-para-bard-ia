import 'dart:convert';
import 'package:http/http.dart' as http;

//FUNÇÕES FILTRO

Future<List<String>> listaDeRedes(String uf, String idDaCidade) async {
  Uri url = Uri.parse(
      'https://api-simp.agencialink.com.br/credenciados/$uf/$idDaCidade');
  var response = await http.post(url);
  var mapResponse = json.decode(response.body);
  return List.generate(
      mapResponse.length,
      (index) =>
          '${mapResponse[index]["nome"]} - ${mapResponse[index]["cidade"]} (${mapResponse[index]["tipo"]}${mapResponse[index]["id"]})');
}

Future<List<String>> listaDeOperadorasMultiselect(
    String uf, String saudeOuOdonto, String pfOuPme) async {
  Uri url = Uri.parse(
      'https://api-simp.agencialink.com.br/operadoras/$uf/$saudeOuOdonto/$pfOuPme');
  var response = await http.post(url);
  var mapResponse = json.decode(response.body);
  return List.generate(mapResponse.length,
      (index) => '${mapResponse[index]["nome"]} (${mapResponse[index]["id"]})');
}

Future<List<String>> listaDeProdutosDaOperadorasMultiselect(
    String uf, String idDaCidade, String saudeOuOdonto, String pfOuPme) async {
  Uri url = Uri.parse(
      'https://api-simp.agencialink.com.br/produtos/$uf/0/$saudeOuOdonto/$pfOuPme/$idDaCidade');
  var response = await http.post(url);
  var mapResponse = json.decode(response.body);
  return List.generate(mapResponse.length,
      (index) => '${mapResponse[index]["nome"]} (${mapResponse[index]["id"]})');
}
