import 'package:flutter/material.dart';
import 'package:multi_select_flutter/bottom_sheet/multi_select_bottom_sheet_field.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:simplite/funcoes/funcoes_filtro_operadoras.dart';
import 'package:simplite/operadora/operadora.dart';

showModalDeFiltros(
    BuildContext context,
    String padraoDeConfortoFiltro,
    String reembolsoFiltro,
    String atendimentoFiltro,
    String abrangenciaFiltro,
    String coparticipacaoFiltro,
    String meiFiltro,
    String regrasDeAceitacaoFiltro,
    String somarTaxasFiltro,
    List<String?> selectedOptions,
    String uf,
    String idDaCidade,
    String saudeOuOdonto,
    String pfOuPme,
    String nomedocliente,
    String cidade,
    String valoratual,
    String totalDeVidas,
    String i0a18,
    String i19a23,
    String i24a28,
    String i29a33,
    String i34a38,
    String i39a43,
    String i44a48,
    String i49a53,
    String i54a58,
    String i59,
    int selectedSaudeIndex,
    int selectedPlanoIndex,
    String padraoDeConforto,
    String reembolso,
    String atendimento,
    String abrangencia,
    String coparticipacao,
    String mei,
    String somarTaxas,
    String regrasDeAceitacao,
    List<String> credenciado,
    List<String> produtosFiltro,
    List<String> operadoras,
    String entidade,
    String profissao,
    String administradora,
    String mesDeReajuste,
    String indiceDeReajuste,
    String valorReajustado,
    String observacoesDeReajuste,
    String emailDoCliente,
    String idDoCliente,
    String celularDoCliente,
    List<String> listaDeResumoDeValores,
    String qualAdministradora,
    String qualProfissao,
    String qualEntidade) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(8.0), // Seu valor de borderRadius aqui
            ),
            insetPadding: const EdgeInsets.all(8),
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Filtros'),
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.close))
                ]),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: MediaQuery.of(context).size.width),
                  DropdownButton<String>(
                    hint: const Text(
                      'Padrão de Conforto',
                    ),
                    style: TextStyle(
                        color: padraoDeConfortoFiltro == 'Padrão de Conforto'
                            ? Colors.grey
                            : Colors.blue),
                    isExpanded: true,
                    value: padraoDeConfortoFiltro,
                    onChanged: (newValue) async {
                      setState(() {
                        padraoDeConfortoFiltro = newValue.toString();
                        padraoDeConfortoFiltro == 'Apartamento'
                            ? padraoDeConforto = 'a'
                            : padraoDeConfortoFiltro == 'Enfermaria'
                                ? padraoDeConforto = 'e'
                                : padraoDeConfortoFiltro == 'Ambulatorial'
                                    ? padraoDeConforto = 'b'
                                    : padraoDeConforto = '';
                      });
                    },
                    items: <String>[
                      'Padrão de Conforto',
                      'Apartamento',
                      'Enfermaria',
                      'Ambulatorial'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            padraoDeConfortoFiltro == 'Padrão de Conforto'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Padrão de Conforto',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'Reembolso',
                    ),
                    style: TextStyle(
                        color: reembolsoFiltro == 'Reembolso'
                            ? Colors.grey
                            : Colors.blue),
                    value: reembolsoFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        reembolsoFiltro = newValue.toString();
                        reembolsoFiltro == 'Sim'
                            ? reembolso = 's'
                            : reembolsoFiltro == 'Não'
                                ? reembolso = 'n'
                                : reembolso = '';
                      });
                    },
                    items: <String>['Reembolso', 'Todos', 'Sim', 'Não']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            reembolsoFiltro == 'Reembolso'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Reembolso',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'Atendimento',
                    ),
                    style: TextStyle(
                        color: atendimentoFiltro == 'Atendimento'
                            ? Colors.grey
                            : Colors.blue),
                    value: atendimentoFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        atendimentoFiltro = newValue.toString();
                        atendimentoFiltro == 'Somente Hospitalar'
                            ? atendimento = 'h'
                            : atendimentoFiltro == 'Hospitalar + Ambulatorial'
                                ? atendimento = 'a'
                                : atendimentoFiltro == 'Somente Ambulatorial'
                                    ? atendimento = 'b'
                                    : atendimento = '';
                      });
                    },
                    items: <String>[
                      'Atendimento',
                      'Todos',
                      'Somente Hospitalar',
                      'Hospitalar + Ambulatorial',
                      'Somente Ambulatorial'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            atendimentoFiltro == 'Atendimento'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Atendimento',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'Abrangência',
                    ),
                    style: TextStyle(
                        color: abrangenciaFiltro == 'Abrangência'
                            ? Colors.grey
                            : Colors.blue),
                    value: abrangenciaFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        abrangenciaFiltro = newValue.toString();
                        abrangenciaFiltro == 'Nacional'
                            ? abrangencia = 'n'
                            : abrangenciaFiltro == 'Estadual'
                                ? abrangencia = 'e'
                                : abrangenciaFiltro == 'Regional'
                                    ? abrangencia = 'r'
                                    : abrangenciaFiltro == 'Local'
                                        ? abrangencia = 'l'
                                        : abrangencia = '';
                      });
                    },
                    items: <String>[
                      'Abrangência',
                      'Todas',
                      'Nacional',
                      'Estadual',
                      'Regional',
                      'Local'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            abrangenciaFiltro == 'Abrangência'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Abrangência',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'Coparticipação',
                    ),
                    style: TextStyle(
                        color: coparticipacaoFiltro == 'Coparticipação'
                            ? Colors.grey
                            : Colors.blue),
                    value: coparticipacaoFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        coparticipacaoFiltro = newValue.toString();
                        coparticipacaoFiltro == 'Sim'
                            ? coparticipacao = 's'
                            : coparticipacaoFiltro == 'Não'
                                ? coparticipacao = 'n'
                                : coparticipacao = '';
                      });
                    },
                    items: <String>['Coparticipação', 'Todos', 'Sim', 'Não']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            coparticipacaoFiltro == 'Coparticipação'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Coparticipação',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'MEI',
                    ),
                    style: TextStyle(
                        color: meiFiltro == 'MEI' ? Colors.grey : Colors.blue),
                    value: meiFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        meiFiltro = newValue.toString();
                        meiFiltro == 'Aceita'
                            ? mei = 's'
                            : meiFiltro == 'Não Aceita'
                                ? mei = 'n'
                                : mei = '';
                      });
                    },
                    items: <String>[
                      'MEI',
                      'Indiferente',
                      'Aceita',
                      'Não Aceita'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            meiFiltro == 'MEI'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'MEI',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  /*const SizedBox(
                    height: 10,
                  ),
                  FutureBuilder(
                      future: listaDeRedes(uf, idDaCidade),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        List<String> listaDeItensDaqui =
                            snapshot.hasData ? snapshot.data : [];

                        return snapshot.hasData
                            ? MultiSelectBottomSheetField<String?>(
                                itemsTextStyle:
                                    TextStyle(color: Colors.grey[600]),
                                confirmText: const Text('Ok'),
                                cancelText: const Text('Cancelar'),
                                searchable: true,
                                items: listaDeItensDaqui
                                    .map((option) =>
                                        MultiSelectItem(option, option))
                                    .toList(),
                                title: const Text('Rede (Múltiplas opções)'),
                                selectedColor: Colors.blue,
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  border: Border.all(
                                    color: Colors.blue,
                                    width: 2,
                                  ),
                                ),
                                buttonText:
                                    const Text('Rede (Múltiplas opções)'),
                                onConfirm: (List<String?> selectedItems) async {
                                  List<String> credenciadoList = selectedItems
                                      .where((item) => item != null)
                                      .cast<String>()
                                      .toList();

                                  List<String> resultList = [];

                                  for (String item in credenciadoList) {
                                    String splitResult =
                                        item.split('(').last.split(')').first;
                                    resultList.add(splitResult);
                                  }
                                  setState(() {
                                    credenciado = resultList;
                                  });
                                },
                                initialValue: credenciado,
                              )
                            : const LinearProgressIndicator();
                      }),*/
                  const SizedBox(
                    height: 10,
                  ),
                  FutureBuilder(
                      future: listaDeOperadorasMultiselect(
                          uf, saudeOuOdonto, pfOuPme),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        List<String> listaDeItensDaqui =
                            snapshot.hasData ? snapshot.data : [];
                        return snapshot.hasData
                            ? MultiSelectBottomSheetField<String?>(
                                itemsTextStyle:
                                    TextStyle(color: Colors.grey[600]),
                                confirmText: const Text('Ok'),
                                searchable: true,
                                cancelText: const Text('Cancelar'),
                                items: listaDeItensDaqui
                                    .map((option) =>
                                        MultiSelectItem(option, option))
                                    .toList(),
                                title: const Text('Operadoras'),
                                selectedColor: Colors.blue,
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  border: Border.all(
                                    color: Colors.blue,
                                    width: 2,
                                  ),
                                ),
                                buttonText: const Text('Operadoras'),
                                onConfirm: (selectedItems) {
                                  List<String> operadorasList = selectedItems
                                      .where((item) => item != null)
                                      .cast<String>()
                                      .toList();

                                  List<String> resultList = [];

                                  for (String item in operadorasList) {
                                    String splitResult =
                                        item.split('(').last.split(')').first;
                                    resultList.add(splitResult);
                                  }
                                  setState(() {
                                    operadoras = resultList;
                                  });
                                },
                                //initialValue: operadoras,
                              )
                            : const LinearProgressIndicator();
                      }),
                  /*const SizedBox(
                    height: 10,
                  ),
                  FutureBuilder(
                      future: listaDeProdutosDaOperadorasMultiselect(
                          uf, idDaCidade, saudeOuOdonto, pfOuPme),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        List<String> listaDeItensDaqui =
                            snapshot.hasData ? snapshot.data : [];
                        return snapshot.hasData
                            ? MultiSelectBottomSheetField<String?>(
                                itemsTextStyle:
                                    TextStyle(color: Colors.grey[600]),
                                confirmText: const Text('Ok'),
                                cancelText: const Text('Cancelar'),
                                searchable: true,
                                items: listaDeItensDaqui
                                    .map((option) =>
                                        MultiSelectItem(option, option))
                                    .toList(),
                                title: const Text('Produtos da Operadora'),
                                selectedColor: Colors.blue,
                                decoration: BoxDecoration(
                                  color: Colors.blue.withOpacity(0.1),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  border: Border.all(
                                    color: Colors.blue,
                                    width: 2,
                                  ),
                                ),
                                buttonText: const Text('Produtos da Operadora'),
                                onConfirm: (selectedItems) {
                                  List<String> produtosList = selectedItems
                                      .where((item) => item != null)
                                      .cast<String>()
                                      .toList();

                                  List<String> resultList = [];

                                  for (String item in produtosList) {
                                    String splitResult =
                                        item.split('(').last.split(')').first;
                                    resultList.add(splitResult);
                                  }
                                  setState(() {
                                    produtosFiltro = resultList;
                                  });
                                },
                                //initialValue: produtosFiltro,
                              )
                            : const LinearProgressIndicator();
                      }),*/
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'Regras de Aceitação',
                    ),
                    style: TextStyle(
                        color: regrasDeAceitacaoFiltro == 'Regras de Aceitação'
                            ? Colors.grey
                            : Colors.blue),
                    value: regrasDeAceitacaoFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        regrasDeAceitacaoFiltro = newValue.toString();
                        regrasDeAceitacaoFiltro == 'Inativo'
                            ? somarTaxas = '2'
                            : somarTaxasFiltro == 'Ativo'
                                ? somarTaxas = '1'
                                : somarTaxas = '';
                      });
                    },
                    items: <String>['Regras de Aceitação', 'Inativo', 'Ativo']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            regrasDeAceitacaoFiltro == 'Regras de Aceitação'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Regras de Aceitação',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  DropdownButton<String>(
                    hint: const Text(
                      'Somar Taxas',
                    ),
                    style: TextStyle(
                        color: somarTaxasFiltro == 'Somar Taxas'
                            ? Colors.grey
                            : Colors.blue),
                    value: somarTaxasFiltro,
                    isExpanded: true,
                    onChanged: (newValue) async {
                      setState(() {
                        somarTaxasFiltro = newValue.toString();
                        somarTaxasFiltro == 'Inativo'
                            ? somarTaxas = '2'
                            : somarTaxasFiltro == 'Ativo'
                                ? somarTaxas = '1'
                                : somarTaxas = '';
                      });
                    },
                    items: <String>['Somar Taxas', 'Inativo', 'Ativo']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Wrap(
                          children: [
                            somarTaxasFiltro == 'Somar Taxas'
                                ? const Center()
                                : const Row(
                                    children: [
                                      Text(
                                        'Somar Taxas',
                                        style: TextStyle(fontSize: 9),
                                      ),
                                    ],
                                  ),
                            Text(value),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                          onPressed: () async {
                            setState(() {
                              padraoDeConfortoFiltro = 'Padrão de Conforto';
                              reembolsoFiltro = 'Reembolso';
                              atendimentoFiltro = 'Atendimento';
                              abrangenciaFiltro = 'Abrangência';
                              coparticipacaoFiltro = 'Coparticipação';
                              meiFiltro = 'MEI';
                              regrasDeAceitacaoFiltro = 'Regras de Aceitação';
                              somarTaxasFiltro = 'Somar Taxas';
                              padraoDeConforto = '';
                              reembolso = '';
                              atendimento = '';
                              abrangencia = '';
                              coparticipacao = '';
                              mei = '';
                              regrasDeAceitacao = '';
                              somarTaxas = '';
                              credenciado = [];
                              produtosFiltro = [];
                              operadoras = [];
                            });
                          },
                          child: const Text(
                            'Limpar',
                            style: TextStyle(color: Colors.grey),
                          )),
                      const SizedBox(width: 10),
                      ElevatedButton(
                          onPressed: () async {
                            setState(() {});
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EscolhaAOperadora(
                                          isAdesaoAgora: saudeOuOdonto == 'a',
                                          nomedocliente: nomedocliente,
                                          cidade: cidade,
                                          idDaCidade: idDaCidade,
                                          uf: uf,
                                          valoratual: valoratual,
                                          i0a18: i0a18,
                                          i19a23: i19a23,
                                          i24a28: i24a28,
                                          i29a33: i29a33,
                                          i34a38: i34a38,
                                          i39a43: i39a43,
                                          i44a48: i44a48,
                                          i49a53: i49a53,
                                          i54a58: i54a58,
                                          i59: i59,
                                          totalDeVidas: totalDeVidas,
                                          selectedSaudeIndex:
                                              selectedSaudeIndex,
                                          selectedPlanoIndex:
                                              selectedPlanoIndex,
                                          saudeOuOdonto: saudeOuOdonto,
                                          pfOuPme: pfOuPme,
                                          meiFiltro: meiFiltro,
                                          reembolsoFiltro: reembolsoFiltro,
                                          somarTaxasFiltro: somarTaxasFiltro,
                                          abrangenciaFiltro: abrangenciaFiltro,
                                          atendimentoFiltro: atendimentoFiltro,
                                          coparticipacaoFiltro:
                                              coparticipacaoFiltro,
                                          padraoDeConfortoFiltro:
                                              padraoDeConfortoFiltro,
                                          regrasDeAceitacaoFiltro:
                                              regrasDeAceitacaoFiltro,
                                          coparticipacao: coparticipacao,
                                          padraoDeConforto: padraoDeConforto,
                                          reembolso: reembolso,
                                          abrangencia: abrangencia,
                                          atendimento: atendimento,
                                          mei: mei,
                                          somarTaxas: somarTaxas,
                                          regrasDeAcetacao: regrasDeAceitacao,
                                          credenciado: credenciado,
                                          produtosFiltro: produtosFiltro,
                                          operadoras: operadoras,
                                          entidade: entidade,
                                          profissao: profissao,
                                          administradora: administradora,
                                          mesDeReajuste: mesDeReajuste,
                                          indiceDeReajuste: indiceDeReajuste,
                                          valorReajustado: valorReajustado,
                                          observacoesDeReajuste:
                                              observacoesDeReajuste,
                                          emailDoCliente: emailDoCliente,
                                          idDoCliente: idDoCliente,
                                          celularDoCliente: celularDoCliente,
                                          qualAdministradora:
                                              qualAdministradora,
                                          qualProfissao: qualProfissao,
                                          qualEntidade: qualEntidade,
                                        )));
                          },
                          child: const Text('Aplicar')),
                    ],
                  )
                ],
              ),
            ),
          );
        });
      });
}

Future<Widget> buildProductsCountWidget(
    List<int> produtosParaEnviarPraCotacao) async {
  return Positioned(
    child: ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(100)),
      child: Container(
        width: 16,
        color: Colors.red,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: Text(
              produtosParaEnviarPraCotacao.length.toString(),
              style: const TextStyle(
                fontSize: 9,
                fontWeight: FontWeight.w200,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
