import 'dart:convert';

import 'package:el_tooltip2/el_tooltip2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:marquee_widget/marquee_widget.dart';
import 'package:provider/provider.dart';
import 'package:simplite/constants.dart';
import 'package:simplite/operadora/operadora.dart';

import '../funcoes/funcoes_adesao.dart';
import '../modelos/modelos.dart';

Future<List<Widget>> listandoOperadoras(
    BuildContext context,
    bool isLoadingAgora,
    String uf,
    String idDaCidade,
    String saudeOuOdonto,
    String pfOuPme,
    String coparticipacao,
    List<String> credenciados,
    String reembolso,
    String padraoDeConforto,
    String atendimento,
    String abrangencia,
    String mei,
    String somarTaxas,
    String compulsorio,
    String entidade,
    String profissao,
    String administradora,
    String adesao,
    String categoria,
    String filtrarFaixas,
    List<int> produtosParaEnviarPraCotacao,
    String searchText,
    bool isAdesaoAgora,
    String valoratual,
    String i0a18,
    String i19a23,
    String i24a28,
    String i29a33,
    String i34a38,
    String i39a43,
    String i44a48,
    String i49a53,
    String i54a58,
    String i59,
    String totalDeVidas,
    List<String> produtos,
    List<String> operadoras) async {
  List<String> listaDeResumoDeValores = [];
  var header = {"Content-Type": "application/json"};
  var body = isAdesaoAgora
      ? json.encode({
          "faixas": {
            "1": int.tryParse(i0a18),
            "2": int.tryParse(i19a23),
            "3": int.tryParse(i24a28),
            "4": int.tryParse(i29a33),
            "5": int.tryParse(i34a38),
            "6": int.tryParse(i39a43),
            "7": int.tryParse(i44a48),
            "8": int.tryParse(i49a53),
            "9": int.tryParse(i54a58),
            "10": int.tryParse(i59)
          },
          "faixas_global": {
            "1": int.tryParse(i0a18),
            "2": int.tryParse(i19a23),
            "3": int.tryParse(i24a28),
            "4": int.tryParse(i29a33),
            "5": int.tryParse(i34a38),
            "6": int.tryParse(i39a43),
            "7": int.tryParse(i44a48),
            "8": int.tryParse(i49a53),
            "9": int.tryParse(i54a58),
            "10": int.tryParse(i59)
          },
          "credenciados": credenciados,
          "produtos": produtos,
          "operadoras": operadoras,
          "administradora": administradora,
          "entidade": entidade,
          "profissao": profissao
        })
      : json.encode({
          "faixas": {
            "1": int.tryParse(i0a18),
            "2": int.tryParse(i19a23),
            "3": int.tryParse(i24a28),
            "4": int.tryParse(i29a33),
            "5": int.tryParse(i34a38),
            "6": int.tryParse(i39a43),
            "7": int.tryParse(i44a48),
            "8": int.tryParse(i49a53),
            "9": int.tryParse(i54a58),
            "10": int.tryParse(i59)
          },
          "faixas_global": {
            "1": int.tryParse(i0a18),
            "2": int.tryParse(i19a23),
            "3": int.tryParse(i24a28),
            "4": int.tryParse(i29a33),
            "5": int.tryParse(i34a38),
            "6": int.tryParse(i39a43),
            "7": int.tryParse(i44a48),
            "8": int.tryParse(i49a53),
            "9": int.tryParse(i54a58),
            "10": int.tryParse(i59)
          },
          "operadoras": operadoras
        });
  Uri url = Uri.parse(isAdesaoAgora
      ? saudeOuOdonto == 'o'
          ? 'https://api-simp.agencialink.com.br/planos/$uf/0/$saudeOuOdonto/$pfOuPme/$idDaCidade?modelo=produto&tipo_plano=$saudeOuOdonto&acomodacao=$padraoDeConforto&reembolso=$reembolso&atendimento=$atendimento&abrangencia=$abrangencia&coparticipacao=$coparticipacao&mei=$mei&tipo_filtro_vidas=f&filtrar_faixas=1&somar_taxas=$somarTaxas&credenciados=$credenciados&produtos=$produtos&operadoras=$operadoras&administradora=$administradora&entidade=$entidade&profissao=$profissao&compulsorio=$compulsorio&categoria=$categoria&adesao=1'
          : 'https://api-simp.agencialink.com.br/planos/$uf/0/$saudeOuOdonto/$pfOuPme/$idDaCidade?modelo=produto&tipo_plano=$saudeOuOdonto&acomodacao=$padraoDeConforto&reembolso=$reembolso&atendimento=$atendimento&abrangencia=$abrangencia&coparticipacao=$coparticipacao&mei=$mei&tipo_filtro_vidas=f&filtrar_faixas=1&somar_taxas=$somarTaxas&credenciados=$credenciados&produtos=$produtos&operadoras=$operadoras&administradora=$administradora&entidade=$entidade&profissao=$profissao&compulsorio=$compulsorio&categoria=$categoria'
      : 'https://api-simp.agencialink.com.br/planos/$uf/0/$saudeOuOdonto/$pfOuPme/$idDaCidade?tipo_plano=$saudeOuOdonto&acomodacao=$padraoDeConforto&reembolso=$reembolso&atendimento=$atendimento&abrangencia=$abrangencia&coparticipacao=$coparticipacao&mei=$mei&tipo_filtro_vidas=f&filtrar_faixas=1&somar_taxas=$somarTaxas');

  var response = await http.post(url, headers: header, body: body);
  if (response.statusCode == 200) {
    var mapResponse = json.decode(response.body);

    if (mapResponse.length == 0) {
      return [
        ListTile(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          selected: true,
          selectedColor: Colors.white,
          selectedTileColor: Colors.red,
          title: const Text('Ops...'),
          subtitle:
              const Text('Nenhum plano encontrado com os filtros informados'),
          trailing: const Icon(Icons.close),
        )
      ];
    }

    return List.generate(mapResponse.length, (index) {
      List<Administradora> administradoras = [];
      String logo = mapResponse[index]["oper_avo"]["logo"];
      String plano = mapResponse[index]["oper"]["nome"] + " - ";
      String plano2 = isAdesaoAgora
          ? mapResponse[index]["planos_grupo"]
          : mapResponse[index]["nome"];
      String planoParaPesquisa = '$plano - $plano2';
      String vidas =
          "${mapResponse[index]["min_vidas"]} à ${mapResponse[index]["max_vidas"]}";
      bool hasCoparticipacao = mapResponse[index]["coparticipacao"] ?? false;
      String coparticipacaoPorcentagem =
          mapResponse[index]["coparticipacao_porcentagem"] ?? '';
      String hasDataFim = mapResponse[index]["data_fim"] ?? 'false';
      bool hasOdonto = mapResponse[index]["odonto"] ?? false;
      bool hasObstetricia = mapResponse[index]["obstetricia"] ?? false;
      List<ExpandedTileController> itemControllers = List.generate(
        index + 1,
        (index) => ExpandedTileController(),
      );
      List<bool> isSelectedList =
          List.generate(mapResponse.length, (index) => false);
      if (isAdesaoAgora) {
        Map<String, dynamic> administradorasData =
            mapResponse[index]["administradoras"];
        administradorasData.forEach((key, value) {
          Administradora admin = Administradora.fromJson(value);
          administradoras.add(admin);
        });
      }

      return planoParaPesquisa
              .replaceAll(RegExp(r'[áàãâä]'), 'a')
              .replaceAll(RegExp(r'[éèêë]'), 'e')
              .replaceAll(RegExp(r'[íìîï]'), 'i')
              .replaceAll(RegExp(r'[óòõôö]'), 'o')
              .replaceAll(RegExp(r'[úùûü]'), 'u')
              .replaceAll(RegExp(r'ç'), 'c')
              .toLowerCase()
              .contains(searchText
                  .replaceAll(RegExp(r'[áàãâä]'), 'a')
                  .replaceAll(RegExp(r'[éèêë]'), 'e')
                  .replaceAll(RegExp(r'[íìîï]'), 'i')
                  .replaceAll(RegExp(r'[óòõôö]'), 'o')
                  .replaceAll(RegExp(r'[úùûü]'), 'u')
                  .replaceAll(RegExp(r'ç'), 'c')
                  .toLowerCase())
          ? ExpandedTile(
              theme: ExpandedTileThemeData(
                  headerPadding: const EdgeInsets.all(3),
                  trailingPadding: const EdgeInsets.all(2),
                  leadingPadding: const EdgeInsets.all(0),
                  titlePadding: const EdgeInsets.all(2),
                  contentPadding: const EdgeInsets.all(5),
                  contentBackgroundColor:
                      Provider.of<ThemeState>(context, listen: false).isDarkMode
                          ? Colors.white24
                          : const Color(0xffeeeeee),
                  headerColor:
                      Provider.of<ThemeState>(context, listen: false).isDarkMode
                          ? Colors.white12
                          : const Color(0xfffafafa)),
              trailing: const Icon(Icons.chevron_right, color: Colors.grey),
              title: Column(
                children: [
                  Row(
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Image.network(
                              logo,
                            ),
                          ),
                          StatefulBuilder(builder: (context, setState) {
                            return StreamBuilder<bool>(
                                stream: Stream.periodic(
                                    const Duration(seconds: 1), (count) {
                                  return isSelectedList[index] ? true : false;
                                }),
                                builder: (context, snapshot) {
                                  return snapshot.hasData
                                      ? snapshot.data!
                                          ? Positioned(
                                              child: ClipRRect(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(100)),
                                                child: Container(
                                                  width: 16,
                                                  height: 16,
                                                  color: Colors.red,
                                                  child: const Center(
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.all(2.0),
                                                      child: SizedBox.shrink(),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : const SizedBox.shrink()
                                      : const SizedBox.shrink();
                                });
                          }),
                        ],
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Row(
                              children: [
                                const SizedBox(
                                  width: 10,
                                ),
                                StatefulBuilder(builder: (context, setState) {
                                  return StreamBuilder(
                                      stream: Stream.periodic(
                                          const Duration(seconds: 1), (count) {
                                        return isSelectedList[index]
                                            ? Colors.blue
                                            : Provider.of<ThemeState>(context,
                                                        listen: false)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700];
                                      }),
                                      builder: (context, snapshot) {
                                        return Expanded(
                                          child: RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                  text: plano,
                                                  style: TextStyle(
                                                    color: snapshot.hasData
                                                        ? snapshot.data
                                                        : Colors.grey[600],
                                                    fontSize: 12.0,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: plano2,
                                                  style: TextStyle(
                                                    color: snapshot.hasData
                                                        ? snapshot.data
                                                        : Colors.grey[600],
                                                    fontSize: 12.0,
                                                    fontWeight: FontWeight.w300,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      });
                                }),
                              ],
                            ),
                            int.parse(vidas.split(' ').first) >= 2
                                ? Padding(
                                    padding:
                                        const EdgeInsets.only(left: 8, top: 8),
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          const Icon(Icons.info_rounded,
                                              color: Colors.orange, size: 10),
                                          const SizedBox(width: 3),
                                          Text(
                                              'Mínimo ${vidas.split(' ').first} Vidas',
                                              style: const TextStyle(
                                                  color: Colors.orange,
                                                  fontSize: 9))
                                        ]),
                                  )
                                : const Center()
                          ],
                        ),
                      ),
                    ],
                  ),
                  const Divider(),
                  Marquee(
                    direction: Axis.horizontal,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                            child: Row(
                          children: [
                            const Center(
                              child: FaIcon(
                                FontAwesomeIcons.userGroup,
                                size: 9,
                                color: Colors.grey,
                              ),
                            ),
                            const SizedBox(
                              width: defaultPaddingMini,
                            ),
                            Center(
                                child: Text(
                              vidas,
                              style: const TextStyle(fontSize: 9),
                            )),
                            const SizedBox(
                              width: defaultPadding,
                            ),
                          ],
                        )),
                        hasOdonto
                            ? const Center(
                                child: Row(
                                children: [
                                  Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.tooth,
                                      size: 9,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(
                                    width: defaultPaddingMini,
                                  ),
                                  Center(
                                      child: Text(
                                    "Odonto",
                                    style: TextStyle(fontSize: 9),
                                  )),
                                  SizedBox(
                                    width: defaultPadding,
                                  ),
                                ],
                              ))
                            : const Center(child: Center()),
                        hasDataFim != 'false'
                            ? Center(
                                child: Row(
                                children: [
                                  const Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.hourglassEnd,
                                      size: 9,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: defaultPaddingMini,
                                  ),
                                  Center(
                                      child: Text(
                                    hasDataFim,
                                    style: const TextStyle(fontSize: 9),
                                  )),
                                  const SizedBox(
                                    width: defaultPadding,
                                  ),
                                ],
                              ))
                            : const Center(child: Center()),
                        hasCoparticipacao
                            ? Center(
                                child: Row(
                                children: [
                                  const Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.handHoldingDollar,
                                      size: 9,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: defaultPaddingMini,
                                  ),
                                  Center(
                                      child: Text(
                                    coparticipacaoPorcentagem == ''
                                        ? "Copart."
                                        : "Copart. $coparticipacaoPorcentagem%",
                                    style: const TextStyle(fontSize: 9),
                                  )),
                                  const SizedBox(
                                    width: defaultPadding,
                                  ),
                                ],
                              ))
                            : const Center(child: Center()),
                        hasObstetricia
                            ? const Center(
                                child: Row(
                                children: [
                                  Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.baby,
                                      size: 9,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(
                                    width: defaultPaddingMini,
                                  ),
                                  Center(
                                      child: Text(
                                    "Obstetricia",
                                    style: TextStyle(fontSize: 9),
                                  )),
                                  SizedBox(
                                    width: defaultPadding,
                                  ),
                                ],
                              ))
                            : const Center(child: Center()),
                      ],
                    ),
                  ),
                ],
              ),
              content: isAdesaoAgora
                  ? Column(
                      children: List.generate(administradoras.length, (indexx) {
                      List<Administradora> entidades = [];

                      Map<String, dynamic> entidadesData = mapResponse[index]
                          ["entidades"][administradoras[indexx].id];
                      entidadesData.forEach((key, value) {
                        Administradora admin = Administradora.fromJson(value);
                        entidades.add(admin);
                      });
/*
-----------------AQUI TEM A LISTA QUE VC QUER----------------

                      List<bool> checkboxValues = List.generate(
                          mapResponse[index]["planos"]
                                  [administradoras[indexx].id]
                              .length, (index3) {
                        List<bool> entidadesLista = List.generate(
                            mapResponse[index]["planos"]
                                    [administradoras[indexx].id]
                                .length,
                            (index) => false);
                        try {
                          return entidadesLista[index3];
                        } catch (e) {
                          return false;
                        }
                      });*/

                      List<bool> selectEntidades = List.generate(
                          entidades.length, (indexSelectEntidades) => false);

                      List<ExpandedTileController> itemControllersAdesao =
                          List.generate(
                        index + 1,
                        (index) => ExpandedTileController(),
                      );

                      List<String> selectedEntidadesString = [];

                      return ExpandedTile(
                          theme: ExpandedTileThemeData(
                              headerColor: Provider.of<ThemeState>(context,
                                          listen: false)
                                      .isDarkMode
                                  ? Colors.white10
                                  : Colors.white70,
                              contentBackgroundColor: Provider.of<ThemeState>(
                                          context,
                                          listen: false)
                                      .isDarkMode
                                  ? Colors.white12
                                  : Colors.white,
                              headerPadding: const EdgeInsets.all(3)),
                          title: Text(
                            administradoras[indexx].nome,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          content: Wrap(
                            children: [
                              StatefulBuilder(builder: (context, setState) {
                                return Wrap(
                                    children: List.generate(
                                  entidades.length,
                                  (index4) => Wrap(
                                    crossAxisAlignment:
                                        WrapCrossAlignment.center,
                                    children: [
                                      GestureDetector(
                                        onTap: () async {
                                          selectEntidades[index4]
                                              ? setState(() {
                                                  // Desmarque todas as outras entidades
                                                  for (var i = 0;
                                                      i < entidades.length;
                                                      i++) {
                                                    selectEntidades[i] = false;
                                                  }
                                                  // Marque a entidade atual
                                                  selectEntidades[index4] =
                                                      false;
                                                })
                                              : setState(() {
                                                  // Desmarque todas as outras entidades
                                                  for (var i = 0;
                                                      i < entidades.length;
                                                      i++) {
                                                    selectEntidades[i] = false;
                                                  }
                                                  // Marque a entidade atual
                                                  selectEntidades[index4] =
                                                      true;
                                                });
                                          selectedEntidadesString.clear();
                                          selectedEntidadesString
                                              .add(entidades[index4].id);
                                        },
                                        child: selectEntidades[index4]
                                            ? const Icon(Icons.check,
                                                color: Colors.blue, size: 10)
                                            : Container(
                                                width: 10,
                                                height: 10,
                                                decoration: BoxDecoration(
                                                  color: Colors
                                                      .white, // Cor de fundo do container
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          2), // Borda arredondada
                                                  border: Border.all(
                                                      color: Provider.of<
                                                                      ThemeState>(
                                                                  context,
                                                                  listen: false)
                                                              .isDarkMode
                                                          ? Colors.white
                                                          : Colors.grey[
                                                              700]!), // Borda cinza
                                                ),
                                                child: const Center(),
                                              ),
                                      ),
                                      const SizedBox(
                                        width: 4,
                                      ),
                                      Text(
                                        entidades[index4].nome,
                                        style: const TextStyle(fontSize: 11),
                                      ),
                                      const SizedBox(
                                        width: 2,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          alertaProfissoesDaEntidade(context,
                                              entidades[index4].id, uf);
                                        },
                                        child: const Icon(Icons.list,
                                            size: 12, color: Colors.grey),
                                      ),
                                      const SizedBox(
                                        width: 7,
                                      ),
                                    ],
                                  ),
                                ));
                              }),
                              const Divider(),
                              StatefulBuilder(builder: (context, setState) {
                                return Wrap(
                                  children: List.generate(
                                    mapResponse[index]["planos"]
                                            [administradoras[indexx].id]
                                        .length,
                                    (index2) {
                                      final entidadesDoPlano = mapResponse[
                                                      index]["planos"]
                                                  [administradoras[indexx].id]
                                              [index2]["entidades"]
                                          .cast<
                                              String>(); // Converte a lista de entidades para Strings

                                      final isSelected =
                                          selectedEntidadesString.isNotEmpty &&
                                              entidadesDoPlano.contains(
                                                  selectedEntidadesString[0]);

                                      String calculoDoTotal = saudeOuOdonto == 'o'
                                          ? (double.parse(mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["unico"].toString()) *
                                                  int.parse(totalDeVidas))
                                              .toStringAsFixed(2)
                                              .toString()
                                              .replaceAll(".", ",")
                                          : double.parse((mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["1"] * int.parse(i0a18) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]
                                                              ["valores"]["2"] *
                                                          int.parse(i19a23) +
                                                      mapResponse[index]["planos"]
                                                                  [administradoras[indexx].id][index2]
                                                              ["valores"]["3"] *
                                                          int.parse(i24a28) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["4"] * int.parse(i29a33) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["5"] * int.parse(i34a38) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["6"] * int.parse(i39a43) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["7"] * int.parse(i44a48) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["8"] * int.parse(i49a53) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["9"] * int.parse(i54a58) +
                                                      mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["10"] * int.parse(i59))
                                                  .toString())
                                              .toStringAsFixed(2)
                                              .toString()
                                              .replaceAll(".", ",");

                                      var checkboxController =
                                          ValueNotifier(isSelected);

                                      return StatefulBuilder(
                                          builder: (context, setState) {
                                        return ValueListenableBuilder(
                                          valueListenable: checkboxController,
                                          builder: (context, value, child) {
                                            return CheckboxListTile(
                                              contentPadding:
                                                  const EdgeInsets.all(0),
                                              title: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    mapResponse[index]["planos"]
                                                            [administradoras[
                                                                    indexx]
                                                                .id][index2]
                                                        ["nome"],
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        'R\$ $calculoDoTotal',
                                                        style: TextStyle(
                                                            color: Provider.of<
                                                                            ThemeState>(
                                                                        context,
                                                                        listen:
                                                                            true)
                                                                    .isDarkMode
                                                                ? Colors
                                                                    .blue[300]
                                                                : Colors.blue,
                                                            fontSize: 12),
                                                      ),
                                                      double.parse(valoratual
                                                                  .toString()
                                                                  .replaceAll(
                                                                      ',',
                                                                      '.')) >
                                                              double.parse(
                                                                  calculoDoTotal
                                                                      .toString()
                                                                      .replaceAll(
                                                                          ',',
                                                                          '.'))
                                                          ? ElTooltip(
                                                              color: Provider.of<
                                                                              ThemeState>(
                                                                          context,
                                                                          listen:
                                                                              true)
                                                                      .isDarkMode
                                                                  ? Colors
                                                                      .black87
                                                                  : Colors
                                                                      .white,
                                                              content: Wrap(
                                                                direction: Axis
                                                                    .vertical,
                                                                children: [
                                                                  const Text(
                                                                    'Custo do plano atual:',
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                  Text(
                                                                      'R\$ $valoratual'),
                                                                  const SizedBox(
                                                                    height: 10,
                                                                  ),
                                                                  const Text(
                                                                    'Valor da economia:',
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                  Text(
                                                                    'Mensal: R\$ ${(double.parse(valoratual.toString().replaceAll(',', '.')) - double.parse(calculoDoTotal.toString().replaceAll(',', '.'))).toStringAsFixed(2).replaceAll('.', ',')}',
                                                                  ),
                                                                  Text(
                                                                    'Anual: R\$ ${((double.parse(valoratual.toString().replaceAll(',', '.')) - double.parse(calculoDoTotal.toString().replaceAll(',', '.'))) * 12).toStringAsFixed(2).replaceAll('.', ',')}',
                                                                  ),
                                                                ],
                                                              ),
                                                              child: Wrap(
                                                                crossAxisAlignment:
                                                                    WrapCrossAlignment
                                                                        .center,
                                                                children: [
                                                                  FaIcon(
                                                                    FontAwesomeIcons
                                                                        .piggyBank,
                                                                    color: Colors
                                                                            .pink[
                                                                        300],
                                                                    size: 10,
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 5,
                                                                  ),
                                                                  Text(
                                                                    'Economia: R\$ ${(double.parse(valoratual.toString().replaceAll(',', '.')) - double.parse(calculoDoTotal.toString().replaceAll(',', '.'))).toStringAsFixed(2).replaceAll('.', ',')}',
                                                                    style: TextStyle(
                                                                        color: Colors.pink[
                                                                            300],
                                                                        fontSize:
                                                                            9),
                                                                  ),
                                                                ],
                                                              ),
                                                            )
                                                          : const SizedBox
                                                              .shrink(),
                                                      ElTooltip(
                                                        color: Provider.of<
                                                                        ThemeState>(
                                                                    context,
                                                                    listen:
                                                                        true)
                                                                .isDarkMode
                                                            ? Colors.black87
                                                            : Colors.white,
                                                        position:
                                                            ElTooltipPosition
                                                                .leftEnd,
                                                        content: Wrap(
                                                          direction:
                                                              Axis.vertical,
                                                          children: [
                                                            Text(
                                                                'Total + IOF: R\$ ${calculoDoTotal.toString()}'),
                                                            saudeOuOdonto == 'o'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : const Divider(),
                                                            i0a18 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '0 à 18: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["1"].toString().replaceAll(".", ",")} x $i0a18'),
                                                            i19a23 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '19 à 23: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["2"].toString().replaceAll(".", ",")} x $i19a23'),
                                                            i24a28 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '24 à 28: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["3"].toString().replaceAll(".", ",")} x $i24a28'),
                                                            i29a33 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '29 à 33: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["4"].toString().replaceAll(".", ",")} x $i29a33'),
                                                            i34a38 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '34 à 38: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["5"].toString().replaceAll(".", ",")} x $i34a38'),
                                                            i39a43 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '39 à 43: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["6"].toString().replaceAll(".", ",")} x $i39a43'),
                                                            i44a48 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '44 à 48: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["7"].toString().replaceAll(".", ",")} x $i44a48'),
                                                            i49a53 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '49 à 53: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["8"].toString().replaceAll(".", ",")} x $i49a53'),
                                                            i54a58 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '54 à 58: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["9"].toString().replaceAll(".", ",")} x $i54a58'),
                                                            i59 == '0'
                                                                ? const SizedBox
                                                                    .shrink()
                                                                : saudeOuOdonto ==
                                                                        'o'
                                                                    ? const SizedBox
                                                                        .shrink()
                                                                    : Text(
                                                                        '59+: R\$ ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["valores"]["10"].toString().replaceAll(".", ",")} x $i59'),
                                                          ],
                                                        ),
                                                        child: const FaIcon(
                                                          FontAwesomeIcons
                                                              .calculator,
                                                          size: 14,
                                                          color: Colors.grey,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Divider()
                                                ],
                                              ),
                                              value: value,
                                              onChanged: (newValue) async {
                                                checkboxController.value =
                                                    newValue!;
                                                setState(() {
                                                  if (newValue) {
                                                    produtosParaEnviarPraCotacao
                                                        .add(int.parse(mapResponse[
                                                                        index]
                                                                    ["planos"]
                                                                [
                                                                administradoras[
                                                                        indexx]
                                                                    .id][index2]
                                                            ["id"]));
                                                    isSelectedList[index] =
                                                        true;
                                                    listaDeResumoDeValores.add(
                                                      '*Opção ${(listaDeResumoDeValores.length + 1).toString()}* - ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["nome"]}\n*Total:* R\$ $calculoDoTotal \n\n',
                                                    );
                                                    checkboxController =
                                                        ValueNotifier(newValue);
                                                  } else {
                                                    produtosParaEnviarPraCotacao
                                                        .remove(int.parse(mapResponse[
                                                                        index]
                                                                    ["planos"]
                                                                [
                                                                administradoras[
                                                                        indexx]
                                                                    .id][index2]
                                                            ["id"]));
                                                    isSelectedList[index] =
                                                        false;
                                                    EscolhaAOperadora
                                                        .listaDeResumoDeValores
                                                        .remove(
                                                      '*Opção ${(EscolhaAOperadora.listaDeResumoDeValores.length + 1).toString()}* - ${mapResponse[index]["planos"][administradoras[indexx].id][index2]["nome"]}\n*Total:* R\$ $calculoDoTotal \n\n',
                                                    );
                                                    checkboxController =
                                                        ValueNotifier(newValue);
                                                  }
                                                });
                                              },
                                            );
                                          },
                                        );
                                      });
                                    },
                                  ),
                                );
                              })
                            ],
                          ),
                          controller: itemControllersAdesao[index]);
                    }))
                  : Column(
                      children: List.generate(
                          mapResponse[index]["planos"].length, (index2) {
                        List<bool> checkBoxValues = List.generate(
                            mapResponse[index]["planos"].length,
                            (index3) => isSelectedList[index] ? true : false);
                        return StatefulBuilder(builder:
                            (BuildContext context, StateSetter setState) {
                          String calculoDoTotal = saudeOuOdonto == 'o'
                              ? (double.parse(mapResponse[index]["planos"][index2]["valores"]["unico"].toString()) *
                                      int.parse(totalDeVidas))
                                  .toStringAsFixed(2)
                                  .toString()
                                  .replaceAll(".", ",")
                              : double.parse((mapResponse[index]["planos"]
                                                  [index2]["valores"]["00-18"] *
                                              int.parse(i0a18) +
                                          mapResponse[index]["planos"][index2]["valores"]["19-23"] *
                                              int.parse(i19a23) +
                                          mapResponse[index]["planos"][index2]
                                                  ["valores"]["24-28"] *
                                              int.parse(i24a28) +
                                          mapResponse[index]["planos"][index2]["valores"]["29-33"] * int.parse(i29a33) +
                                          mapResponse[index]["planos"][index2]["valores"]["34-38"] * int.parse(i34a38) +
                                          mapResponse[index]["planos"][index2]["valores"]["39-43"] * int.parse(i39a43) +
                                          mapResponse[index]["planos"][index2]["valores"]["44-48"] * int.parse(i44a48) +
                                          mapResponse[index]["planos"][index2]["valores"]["49-53"] * int.parse(i49a53) +
                                          mapResponse[index]["planos"][index2]["valores"]["54-58"] * int.parse(i54a58) +
                                          mapResponse[index]["planos"][index2]["valores"]["59-+"] * int.parse(i59))
                                      .toString())
                                  .toStringAsFixed(2)
                                  .toString()
                                  .replaceAll(".", ",");
                          return CheckboxListTile(
                            contentPadding: const EdgeInsets.all(0),
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  mapResponse[index]["planos"][index2]["nome"],
                                  style: const TextStyle(fontSize: 12),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'R\$ $calculoDoTotal',
                                      style: TextStyle(
                                          color: Provider.of<ThemeState>(
                                                      context,
                                                      listen: true)
                                                  .isDarkMode
                                              ? const Color.fromARGB(
                                                  255, 103, 145, 180)
                                              : Colors.blue,
                                          fontSize: 12),
                                    ),
                                    double.parse(valoratual
                                                .toString()
                                                .replaceAll(',', '.')) >
                                            double.parse(calculoDoTotal
                                                .toString()
                                                .replaceAll(',', '.'))
                                        ? ElTooltip(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.black87
                                                : Colors.white,
                                            content: Wrap(
                                              direction: Axis.vertical,
                                              children: [
                                                const Text(
                                                    'Custo do plano atual:',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text('R\$ $valoratual'),
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                const Text('Valor da economia:',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    'Mensal: R\$ ${(double.parse(valoratual.toString().replaceAll(',', '.')) - double.parse(calculoDoTotal.toString().replaceAll(',', '.'))).toStringAsFixed(2).replaceAll('.', ',')}'),
                                                Text(
                                                    'Anual: R\$ ${((double.parse(valoratual.toString().replaceAll(',', '.')) - double.parse(calculoDoTotal.toString().replaceAll(',', '.'))) * 12).toStringAsFixed(2).replaceAll('.', ',')}'),
                                              ],
                                            ),
                                            child: Wrap(
                                                crossAxisAlignment:
                                                    WrapCrossAlignment.center,
                                                children: [
                                                  FaIcon(
                                                      FontAwesomeIcons
                                                          .piggyBank,
                                                      color: Colors.pink[300],
                                                      size: 10),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                      'Economia: R\$ ${(double.parse(valoratual.toString().replaceAll(',', '.')) - double.parse(calculoDoTotal.toString().replaceAll(',', '.'))).toStringAsFixed(2).replaceAll('.', ',')}',
                                                      style: TextStyle(
                                                          color:
                                                              Colors.pink[300],
                                                          fontSize: 9))
                                                ]),
                                          )
                                        : const SizedBox.shrink(),
                                    ElTooltip(
                                      color: Provider.of<ThemeState>(context,
                                                  listen: true)
                                              .isDarkMode
                                          ? Colors.black87
                                          : Colors.white,
                                      position: ElTooltipPosition.leftEnd,
                                      content: Wrap(
                                          direction: Axis.vertical,
                                          children: [
                                            Text(
                                                'Total + IOF: R\$ ${calculoDoTotal.toString()}'),
                                            saudeOuOdonto == 'o'
                                                ? const SizedBox.shrink()
                                                : const Divider(),
                                            i0a18 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '0 à 18: R\$ ${mapResponse[index]["planos"][index2]["valores"]["00-18"].toString().replaceAll(".", ",")} x $i0a18'),
                                            i19a23 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '19 à 23: R\$ ${mapResponse[index]["planos"][index2]["valores"]["19-23"].toString().replaceAll(".", ",")} x $i19a23'),
                                            i24a28 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '24 à 28: R\$ ${mapResponse[index]["planos"][index2]["valores"]["24-28"].toString().replaceAll(".", ",")} x $i24a28'),
                                            i29a33 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '29 à 33: R\$ ${mapResponse[index]["planos"][index2]["valores"]["29-33"].toString().replaceAll(".", ",")} x $i29a33'),
                                            i34a38 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '34 à 38: R\$ ${mapResponse[index]["planos"][index2]["valores"]["34-38"].toString().replaceAll(".", ",")} x $i34a38'),
                                            i39a43 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '39 à 43: R\$ ${mapResponse[index]["planos"][index2]["valores"]["39-43"].toString().replaceAll(".", ",")} x $i39a43'),
                                            i44a48 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '44 à 48: R\$ ${mapResponse[index]["planos"][index2]["valores"]["44-48"].toString().replaceAll(".", ",")} x $i44a48'),
                                            i49a53 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '49 à 53: R\$ ${mapResponse[index]["planos"][index2]["valores"]["49-53"].toString().replaceAll(".", ",")} x $i49a53'),
                                            i54a58 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '54 à 58: R\$ ${mapResponse[index]["planos"][index2]["valores"]["54-58"].toString().replaceAll(".", ",")} x $i54a58'),
                                            i59 == '0'
                                                ? const SizedBox.shrink()
                                                : saudeOuOdonto == 'o'
                                                    ? const SizedBox.shrink()
                                                    : Text(
                                                        '59+: R\$ ${mapResponse[index]["planos"][index2]["valores"]["59-+"].toString().replaceAll(".", ",")} x $i59'),
                                          ]),
                                      child: const FaIcon(
                                        FontAwesomeIcons.calculator,
                                        size: 14,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                                const Divider()
                              ],
                            ),
                            value: checkBoxValues[index2],
                            onChanged: (value) async {
                              setState(() {
                                checkBoxValues[index2] == true
                                    ? produtosParaEnviarPraCotacao.remove(
                                        int.parse(mapResponse[index]["planos"]
                                            [index2]["id"]))
                                    : produtosParaEnviarPraCotacao.add(
                                        int.parse(mapResponse[index]["planos"]
                                            [index2]["id"]));
                                checkBoxValues[index2] == true
                                    ? checkBoxValues[index2] = false
                                    : checkBoxValues[index2] =
                                        true; // Atualiza o valor do checkbox no estado
                                checkBoxValues[index2] == true
                                    ? isSelectedList[index] = true
                                    : isSelectedList[index] = false;
                                setState(() {
                                  value!
                                      ? EscolhaAOperadora.listaDeResumoDeValores
                                          .add(
                                              '*Opção ${(listaDeResumoDeValores.length + 1).toString()}* - ${mapResponse[index]["planos"][index2]["nome"]}\n*Total:* R\$ $calculoDoTotal \n\n')
                                      : EscolhaAOperadora.listaDeResumoDeValores
                                          .remove(
                                              '*Opção ${(EscolhaAOperadora.listaDeResumoDeValores.length + 1).toString()}* - ${mapResponse[index]["planos"][index2]["nome"]}\n*Total:* R\$ $calculoDoTotal \n\n');
                                });
                              });
                            },
                          );
                        });
                      }),
                    ),
              controller: itemControllers[index])
          : const SizedBox.shrink();
    });
  } else {
    return [
      ExpandedTile(
          enabled: false,
          title: const Text('Ocorreu um erro'),
          content: const Center(),
          controller: ExpandedTileController())
    ];
  }
}
