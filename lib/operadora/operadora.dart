// ignore_for_file: must_be_immutable, use_build_context_synchronously

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/funcoes/funcoes_adesao.dart';
import 'package:simplite/funcoes/funcoes_operadora.dart';
import 'package:simplite/home/home.dart';
import 'package:simplite/menus/menudrawer.dart';
import 'package:simplite/operadora/operadora2.dart';
import 'package:simplite/resultado/resultado.dart';
import 'package:http/http.dart' as http;

class EscolhaAOperadora extends StatefulWidget {
  final String nomedocliente;
  final String cidade;
  final String idDaCidade;
  final String uf;
  final String valoratual;
  final String i0a18;
  final String i19a23;
  final String i24a28;
  final String i29a33;
  final String i34a38;
  final String i39a43;
  final String i44a48;
  final String i49a53;
  final String i54a58;
  final String i59;
  final String totalDeVidas;
  final String padraoDeConfortoFiltro;
  final String reembolsoFiltro;
  final String atendimentoFiltro;
  final String abrangenciaFiltro;
  final String coparticipacaoFiltro;
  final String meiFiltro;
  final String regrasDeAceitacaoFiltro;
  final String somarTaxasFiltro;
  final String padraoDeConforto;
  final String reembolso;
  final String atendimento;
  final String abrangencia;
  final String coparticipacao;
  final String mei;
  final String somarTaxas;
  final String regrasDeAcetacao;
  int selectedSaudeIndex;
  int selectedPlanoIndex;
  String saudeOuOdonto;
  String pfOuPme;
  bool isAdesaoAgora;
  List<String> credenciado;
  List<String> produtosFiltro;
  List<String> operadoras;
  String entidade;
  String profissao;
  String administradora;
  String mesDeReajuste;
  String indiceDeReajuste;
  String valorReajustado;
  String observacoesDeReajuste;
  String emailDoCliente;
  String idDoCliente;
  String celularDoCliente;
  String qualAdministradora;
  String qualProfissao;
  String qualEntidade;
  EscolhaAOperadora(
      {super.key,
      required this.nomedocliente,
      required this.cidade,
      required this.idDaCidade,
      required this.uf,
      required this.valoratual,
      required this.i0a18,
      required this.i19a23,
      required this.i24a28,
      required this.i29a33,
      required this.i34a38,
      required this.i39a43,
      required this.i44a48,
      required this.i49a53,
      required this.i54a58,
      required this.i59,
      required this.totalDeVidas,
      required this.selectedSaudeIndex,
      required this.selectedPlanoIndex,
      required this.saudeOuOdonto,
      required this.pfOuPme,
      required this.abrangenciaFiltro,
      required this.atendimentoFiltro,
      required this.coparticipacaoFiltro,
      required this.meiFiltro,
      required this.padraoDeConfortoFiltro,
      required this.reembolsoFiltro,
      required this.regrasDeAceitacaoFiltro,
      required this.somarTaxasFiltro,
      required this.padraoDeConforto,
      required this.reembolso,
      required this.abrangencia,
      required this.atendimento,
      required this.coparticipacao,
      required this.mei,
      required this.somarTaxas,
      required this.regrasDeAcetacao,
      required this.isAdesaoAgora,
      required this.credenciado,
      required this.operadoras,
      required this.produtosFiltro,
      required this.entidade,
      required this.profissao,
      required this.administradora,
      required this.mesDeReajuste,
      required this.indiceDeReajuste,
      required this.valorReajustado,
      required this.observacoesDeReajuste,
      required this.emailDoCliente,
      required this.idDoCliente,
      required this.celularDoCliente,
      required this.qualAdministradora,
      required this.qualProfissao,
      required this.qualEntidade});

  static bool isEdicaoDeCotacao = false;
  static int somaTotalDePlanos = 0;
  static List<String> listaDeResumoDeValores = [];

  @override
  State<EscolhaAOperadora> createState() => _EscolhaAOperadoraState();
}

class _EscolhaAOperadoraState extends State<EscolhaAOperadora> {
  String compulsorio = '';
  String adesao = '';
  String categoria = '';
  String faixas = '';
  String produtos = '';
  String filtrarFaixas = '';
  String searchText = '';
  bool isLoadingAgora = false;
  final TextEditingController _adesaoAdministradoraController =
      TextEditingController();
  final TextEditingController _adesaoProfissoesController =
      TextEditingController();
  final TextEditingController _adesaoEntidadesController =
      TextEditingController();
  final TextEditingController _pesquisarText = TextEditingController();
  bool _isIndoPraOutraTela = false;
  List<int> produtosParaEnviarPraCotacao = [];
  List<String?> selectedOptions = [];

  late int qtdDeFiltrosAtivos;

  final _advancedDrawerController = AdvancedDrawerController();
  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }

  Widget _buildButton(String text, int index, Function() onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        textStyle: TextStyle(
            fontSize: 11,
            color: index == widget.selectedPlanoIndex
                ? Colors.white
                : Colors.grey),
        backgroundColor: index == widget.selectedSaudeIndex ||
                index == widget.selectedPlanoIndex
            ? Colors.blue
            : Colors.grey[400],
        padding: const EdgeInsets.symmetric(horizontal: 2),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      child: Text(text, style: const TextStyle(color: Colors.white)),
    );
  }

  Stream<int> productsCountStream() {
    return Stream.periodic(const Duration(seconds: 2), (count) {
      return produtosParaEnviarPraCotacao.length;
    });
  }

  Stream<int> filtersCountStream() {
    return Stream.periodic(const Duration(seconds: 2), (count) {
      return qtdDeFiltrosAtivos;
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _isIndoPraOutraTela = false;
      qtdDeFiltrosAtivos = (widget.padraoDeConforto != '' ? 1 : 0) +
          (widget.reembolso != '' ? 1 : 0) +
          (widget.atendimento != '' ? 1 : 0) +
          (widget.abrangencia != '' ? 1 : 0) +
          (widget.coparticipacao != '' ? 1 : 0) +
          (widget.mei != '' ? 1 : 0) +
          (widget.regrasDeAcetacao != '' ? 1 : 0) +
          (widget.somarTaxas != '' ? 1 : 0);
    });
  }

  @override
  void dispose() {
    super.dispose();
    productsCountStream();
    filtersCountStream();
  }

  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
      backdrop: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.blueGrey, Colors.blueGrey.withOpacity(0.7)],
          ),
        ),
      ),
      controller: _advancedDrawerController,
      animationCurve: Curves.easeInOut,
      animationDuration: const Duration(milliseconds: 300),
      animateChildDecoration: true,
      rtlOpening: false,
      disabledGestures: false,
      childDecoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      drawer: const MenuDrawer(),
      child: SafeArea(
        child: Scaffold(
          body: Center(
            child: SizedBox(
              width: 550,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: _buildButton(
                              'Saúde',
                              0,
                              () async {
                                setState(() {
                                  widget.selectedSaudeIndex = 0;
                                  widget.saudeOuOdonto = 's';
                                });
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EscolhaAOperadora(
                                              nomedocliente:
                                                  widget.nomedocliente,
                                              cidade: widget.cidade,
                                              idDaCidade: widget.idDaCidade,
                                              uf: widget.uf,
                                              valoratual: widget.valoratual,
                                              i0a18: widget.i0a18,
                                              i19a23: widget.i19a23,
                                              i24a28: widget.i24a28,
                                              i29a33: widget.i29a33,
                                              i34a38: widget.i34a38,
                                              i39a43: widget.i39a43,
                                              i44a48: widget.i44a48,
                                              i49a53: widget.i49a53,
                                              i54a58: widget.i54a58,
                                              i59: widget.i59,
                                              totalDeVidas: widget.totalDeVidas,
                                              selectedSaudeIndex: 0,
                                              selectedPlanoIndex:
                                                  widget.selectedPlanoIndex,
                                              saudeOuOdonto: 's',
                                              pfOuPme: widget.pfOuPme,
                                              meiFiltro: widget.meiFiltro,
                                              reembolsoFiltro:
                                                  widget.reembolsoFiltro,
                                              somarTaxasFiltro:
                                                  widget.somarTaxasFiltro,
                                              abrangenciaFiltro:
                                                  widget.abrangenciaFiltro,
                                              atendimentoFiltro:
                                                  widget.atendimentoFiltro,
                                              coparticipacaoFiltro:
                                                  widget.coparticipacaoFiltro,
                                              padraoDeConfortoFiltro:
                                                  widget.padraoDeConfortoFiltro,
                                              regrasDeAceitacaoFiltro: widget
                                                  .regrasDeAceitacaoFiltro,
                                              coparticipacao:
                                                  widget.coparticipacao,
                                              padraoDeConforto:
                                                  widget.padraoDeConforto,
                                              reembolso: widget.reembolso,
                                              abrangencia: widget.abrangencia,
                                              atendimento: widget.atendimento,
                                              mei: widget.mei,
                                              somarTaxas: widget.somarTaxas,
                                              regrasDeAcetacao:
                                                  widget.regrasDeAcetacao,
                                              isAdesaoAgora:
                                                  widget.isAdesaoAgora,
                                              credenciado: widget.credenciado,
                                              produtosFiltro:
                                                  widget.produtosFiltro,
                                              operadoras: widget.operadoras,
                                              entidade: widget.entidade,
                                              profissao: widget.profissao,
                                              administradora:
                                                  widget.administradora,
                                              mesDeReajuste:
                                                  widget.mesDeReajuste,
                                              indiceDeReajuste:
                                                  widget.indiceDeReajuste,
                                              valorReajustado:
                                                  widget.valorReajustado,
                                              observacoesDeReajuste:
                                                  widget.observacoesDeReajuste,
                                              emailDoCliente:
                                                  widget.emailDoCliente,
                                              idDoCliente: widget.idDoCliente,
                                              celularDoCliente:
                                                  widget.celularDoCliente,
                                              qualAdministradora:
                                                  widget.qualAdministradora,
                                              qualProfissao:
                                                  widget.qualProfissao,
                                              qualEntidade: widget.qualEntidade,
                                            )));
                              },
                            ),
                          ),
                          const SizedBox(width: 3.0),
                          Expanded(
                            child: _buildButton(
                              'Odonto',
                              1,
                              () async {
                                setState(() {
                                  widget.selectedSaudeIndex = 1;
                                  widget.saudeOuOdonto = 'o';
                                  adesao = '1';
                                });
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EscolhaAOperadora(
                                              nomedocliente:
                                                  widget.nomedocliente,
                                              cidade: widget.cidade,
                                              idDaCidade: widget.idDaCidade,
                                              uf: widget.uf,
                                              valoratual: widget.valoratual,
                                              i0a18: widget.i0a18,
                                              i19a23: widget.i19a23,
                                              i24a28: widget.i24a28,
                                              i29a33: widget.i29a33,
                                              i34a38: widget.i34a38,
                                              i39a43: widget.i39a43,
                                              i44a48: widget.i44a48,
                                              i49a53: widget.i49a53,
                                              i54a58: widget.i54a58,
                                              i59: widget.i59,
                                              totalDeVidas: widget.totalDeVidas,
                                              selectedSaudeIndex: 1,
                                              selectedPlanoIndex:
                                                  widget.selectedPlanoIndex,
                                              saudeOuOdonto: 'o',
                                              pfOuPme: widget.pfOuPme,
                                              meiFiltro: widget.meiFiltro,
                                              reembolsoFiltro:
                                                  widget.reembolsoFiltro,
                                              somarTaxasFiltro:
                                                  widget.somarTaxasFiltro,
                                              abrangenciaFiltro:
                                                  widget.abrangenciaFiltro,
                                              atendimentoFiltro:
                                                  widget.atendimentoFiltro,
                                              coparticipacaoFiltro:
                                                  widget.coparticipacaoFiltro,
                                              padraoDeConfortoFiltro:
                                                  widget.padraoDeConfortoFiltro,
                                              regrasDeAceitacaoFiltro: widget
                                                  .regrasDeAceitacaoFiltro,
                                              coparticipacao:
                                                  widget.coparticipacao,
                                              padraoDeConforto:
                                                  widget.padraoDeConforto,
                                              reembolso: widget.reembolso,
                                              abrangencia: widget.abrangencia,
                                              atendimento: widget.atendimento,
                                              mei: widget.mei,
                                              somarTaxas: widget.somarTaxas,
                                              regrasDeAcetacao:
                                                  widget.regrasDeAcetacao,
                                              isAdesaoAgora:
                                                  widget.isAdesaoAgora,
                                              credenciado: widget.credenciado,
                                              produtosFiltro:
                                                  widget.produtosFiltro,
                                              operadoras: widget.operadoras,
                                              entidade: widget.entidade,
                                              profissao: widget.profissao,
                                              administradora:
                                                  widget.administradora,
                                              mesDeReajuste:
                                                  widget.mesDeReajuste,
                                              indiceDeReajuste:
                                                  widget.indiceDeReajuste,
                                              valorReajustado:
                                                  widget.valorReajustado,
                                              observacoesDeReajuste:
                                                  widget.observacoesDeReajuste,
                                              emailDoCliente:
                                                  widget.emailDoCliente,
                                              idDoCliente: widget.idDoCliente,
                                              celularDoCliente:
                                                  widget.celularDoCliente,
                                              qualAdministradora:
                                                  widget.qualAdministradora,
                                              qualProfissao:
                                                  widget.qualProfissao,
                                              qualEntidade: widget.qualEntidade,
                                            )));
                              },
                            ),
                          ),
                          const SizedBox(width: 25.0),
                          Expanded(
                            child: _buildButton(
                              'PF',
                              2,
                              () async {
                                setState(() {
                                  widget.selectedPlanoIndex = 2;
                                  widget.isAdesaoAgora = false;
                                  widget.pfOuPme = 'pf';
                                  widget.saudeOuOdonto == 'a'
                                      ? widget.saudeOuOdonto = 's'
                                      : null;
                                });
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EscolhaAOperadora(
                                              nomedocliente:
                                                  widget.nomedocliente,
                                              cidade: widget.cidade,
                                              idDaCidade: widget.idDaCidade,
                                              uf: widget.uf,
                                              valoratual: widget.valoratual,
                                              i0a18: widget.i0a18,
                                              i19a23: widget.i19a23,
                                              i24a28: widget.i24a28,
                                              i29a33: widget.i29a33,
                                              i34a38: widget.i34a38,
                                              i39a43: widget.i39a43,
                                              i44a48: widget.i44a48,
                                              i49a53: widget.i49a53,
                                              i54a58: widget.i54a58,
                                              i59: widget.i59,
                                              totalDeVidas: widget.totalDeVidas,
                                              selectedSaudeIndex:
                                                  widget.selectedSaudeIndex,
                                              selectedPlanoIndex: 2,
                                              saudeOuOdonto:
                                                  widget.saudeOuOdonto,
                                              pfOuPme: 'pf',
                                              meiFiltro: widget.meiFiltro,
                                              reembolsoFiltro:
                                                  widget.reembolsoFiltro,
                                              somarTaxasFiltro:
                                                  widget.somarTaxasFiltro,
                                              abrangenciaFiltro:
                                                  widget.abrangenciaFiltro,
                                              atendimentoFiltro:
                                                  widget.atendimentoFiltro,
                                              coparticipacaoFiltro:
                                                  widget.coparticipacaoFiltro,
                                              padraoDeConfortoFiltro:
                                                  widget.padraoDeConfortoFiltro,
                                              regrasDeAceitacaoFiltro: widget
                                                  .regrasDeAceitacaoFiltro,
                                              coparticipacao:
                                                  widget.coparticipacao,
                                              padraoDeConforto:
                                                  widget.padraoDeConforto,
                                              reembolso: widget.reembolso,
                                              abrangencia: widget.abrangencia,
                                              atendimento: widget.atendimento,
                                              mei: widget.mei,
                                              somarTaxas: widget.somarTaxas,
                                              regrasDeAcetacao:
                                                  widget.regrasDeAcetacao,
                                              isAdesaoAgora: false,
                                              credenciado: widget.credenciado,
                                              produtosFiltro:
                                                  widget.produtosFiltro,
                                              operadoras: widget.operadoras,
                                              entidade: widget.entidade,
                                              profissao: widget.profissao,
                                              administradora:
                                                  widget.administradora,
                                              mesDeReajuste:
                                                  widget.mesDeReajuste,
                                              indiceDeReajuste:
                                                  widget.indiceDeReajuste,
                                              valorReajustado:
                                                  widget.valorReajustado,
                                              observacoesDeReajuste:
                                                  widget.observacoesDeReajuste,
                                              emailDoCliente:
                                                  widget.emailDoCliente,
                                              idDoCliente: widget.idDoCliente,
                                              celularDoCliente:
                                                  widget.celularDoCliente,
                                              qualAdministradora:
                                                  widget.qualAdministradora,
                                              qualProfissao:
                                                  widget.qualProfissao,
                                              qualEntidade: widget.qualEntidade,
                                            )));
                              },
                            ),
                          ),
                          const SizedBox(width: 3.0),
                          Expanded(
                            child: _buildButton(
                              'PME',
                              3,
                              () async {
                                setState(() {
                                  widget.selectedPlanoIndex = 3;
                                  widget.isAdesaoAgora = false;
                                  widget.pfOuPme = 'pme';
                                  widget.saudeOuOdonto == 'a'
                                      ? widget.saudeOuOdonto = 's'
                                      : null;
                                });
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EscolhaAOperadora(
                                              nomedocliente:
                                                  widget.nomedocliente,
                                              cidade: widget.cidade,
                                              idDaCidade: widget.idDaCidade,
                                              uf: widget.uf,
                                              valoratual: widget.valoratual,
                                              i0a18: widget.i0a18,
                                              i19a23: widget.i19a23,
                                              i24a28: widget.i24a28,
                                              i29a33: widget.i29a33,
                                              i34a38: widget.i34a38,
                                              i39a43: widget.i39a43,
                                              i44a48: widget.i44a48,
                                              i49a53: widget.i49a53,
                                              i54a58: widget.i54a58,
                                              i59: widget.i59,
                                              totalDeVidas: widget.totalDeVidas,
                                              selectedSaudeIndex:
                                                  widget.selectedSaudeIndex,
                                              selectedPlanoIndex: 3,
                                              saudeOuOdonto:
                                                  widget.saudeOuOdonto,
                                              pfOuPme: 'pme',
                                              meiFiltro: widget.meiFiltro,
                                              reembolsoFiltro:
                                                  widget.reembolsoFiltro,
                                              somarTaxasFiltro:
                                                  widget.somarTaxasFiltro,
                                              abrangenciaFiltro:
                                                  widget.abrangenciaFiltro,
                                              atendimentoFiltro:
                                                  widget.atendimentoFiltro,
                                              coparticipacaoFiltro:
                                                  widget.coparticipacaoFiltro,
                                              padraoDeConfortoFiltro:
                                                  widget.padraoDeConfortoFiltro,
                                              regrasDeAceitacaoFiltro: widget
                                                  .regrasDeAceitacaoFiltro,
                                              coparticipacao:
                                                  widget.coparticipacao,
                                              padraoDeConforto:
                                                  widget.padraoDeConforto,
                                              reembolso: widget.reembolso,
                                              abrangencia: widget.abrangencia,
                                              atendimento: widget.atendimento,
                                              mei: widget.mei,
                                              somarTaxas: widget.somarTaxas,
                                              regrasDeAcetacao:
                                                  widget.regrasDeAcetacao,
                                              isAdesaoAgora: false,
                                              credenciado: widget.credenciado,
                                              produtosFiltro:
                                                  widget.produtosFiltro,
                                              operadoras: widget.operadoras,
                                              entidade: widget.entidade,
                                              profissao: widget.profissao,
                                              administradora:
                                                  widget.administradora,
                                              mesDeReajuste:
                                                  widget.mesDeReajuste,
                                              indiceDeReajuste:
                                                  widget.indiceDeReajuste,
                                              valorReajustado:
                                                  widget.valorReajustado,
                                              observacoesDeReajuste:
                                                  widget.observacoesDeReajuste,
                                              emailDoCliente:
                                                  widget.emailDoCliente,
                                              idDoCliente: widget.idDoCliente,
                                              celularDoCliente:
                                                  widget.celularDoCliente,
                                              qualAdministradora:
                                                  widget.qualAdministradora,
                                              qualProfissao:
                                                  widget.qualProfissao,
                                              qualEntidade: widget.qualEntidade,
                                            )));
                              },
                            ),
                          ),
                          const SizedBox(width: 3.0),
                          Expanded(
                            child: _buildButton(
                              'ADE',
                              4,
                              () async {
                                setState(() {
                                  widget.selectedPlanoIndex = 4;
                                });
                                modalFiltrosAdesao(
                                    context,
                                    _adesaoAdministradoraController,
                                    _adesaoProfissoesController,
                                    _adesaoEntidadesController,
                                    widget.nomedocliente,
                                    widget.cidade,
                                    widget.idDaCidade,
                                    widget.uf,
                                    widget.valoratual,
                                    widget.i0a18,
                                    widget.i19a23,
                                    widget.i24a28,
                                    widget.i29a33,
                                    widget.i34a38,
                                    widget.i39a43,
                                    widget.i44a48,
                                    widget.i49a53,
                                    widget.i54a58,
                                    widget.i59,
                                    widget.totalDeVidas,
                                    widget.padraoDeConfortoFiltro,
                                    widget.reembolsoFiltro,
                                    widget.atendimentoFiltro,
                                    widget.abrangenciaFiltro,
                                    widget.coparticipacaoFiltro,
                                    widget.meiFiltro,
                                    widget.regrasDeAceitacaoFiltro,
                                    widget.somarTaxasFiltro,
                                    widget.padraoDeConforto,
                                    widget.reembolso,
                                    widget.atendimento,
                                    widget.abrangencia,
                                    widget.coparticipacao,
                                    widget.mei,
                                    widget.somarTaxas,
                                    widget.regrasDeAcetacao,
                                    widget.selectedSaudeIndex,
                                    widget.selectedPlanoIndex,
                                    widget.saudeOuOdonto,
                                    widget.pfOuPme,
                                    widget.credenciado,
                                    widget.produtosFiltro,
                                    widget.operadoras,
                                    widget.mesDeReajuste,
                                    widget.indiceDeReajuste,
                                    widget.valorReajustado,
                                    widget.observacoesDeReajuste,
                                    widget.emailDoCliente,
                                    widget.idDoCliente,
                                    widget.celularDoCliente,
                                    EscolhaAOperadora.listaDeResumoDeValores,
                                    widget.qualAdministradora,
                                    widget.qualProfissao,
                                    widget.qualEntidade);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Vidas: ${widget.totalDeVidas}',
                          style:
                              const TextStyle(fontSize: 10, color: Colors.grey),
                        ),
                        const Text(
                          '  -  ',
                          style: TextStyle(fontSize: 10, color: Colors.grey),
                        ),
                        Text(
                          '${widget.cidade} - ${widget.uf}',
                          style:
                              const TextStyle(fontSize: 10, color: Colors.grey),
                        ),
                        widget.valoratual == '0'
                            ? const SizedBox.shrink()
                            : const Text(
                                ' - ',
                                style:
                                    TextStyle(fontSize: 10, color: Colors.grey),
                              ),
                        widget.valoratual == '0'
                            ? const SizedBox.shrink()
                            : Text(
                                'R\$ ${widget.valoratual}',
                                style: const TextStyle(
                                    fontSize: 10, color: Colors.grey),
                              ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 5,
                          child: TextFormField(
                            controller: _pesquisarText,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  horizontal: 2, vertical: 0),
                              hintStyle: TextStyle(color: Colors.grey[300]),
                              hintText: 'Pesquisar...',
                              prefixIcon: searchText == ''
                                  ? Icon(Icons.search, color: Colors.grey[300])
                                  : IconButton(
                                      onPressed: () async {
                                        setState(() {
                                          searchText = '';
                                          _pesquisarText.text = '';
                                        });
                                      },
                                      icon: Icon(Icons.close,
                                          color: Colors.grey[300])),
                            ),
                            onChanged: (value) async {
                              setState(() {
                                searchText = value;
                              });
                            },
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Stack(
                            children: [
                              StreamBuilder<int>(
                                  stream: filtersCountStream(),
                                  initialData: qtdDeFiltrosAtivos,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return snapshot.data == 0
                                          ? const SizedBox.shrink()
                                          : Positioned(
                                              child: ClipRRect(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(100)),
                                                child: Container(
                                                  width: 16,
                                                  color: Colors.red,
                                                  child: Center(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              2.0),
                                                      child: Text(
                                                        snapshot.data
                                                            .toString(),
                                                        style: const TextStyle(
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            );
                                    } else {
                                      return const SizedBox.shrink();
                                    }
                                  }),
                              Ink(
                                decoration: const ShapeDecoration(
                                  color: Colors.blue,
                                  shape: CircleBorder(),
                                ),
                                child: IconButton(
                                    color: Colors.white,
                                    onPressed: () {
                                      showModalDeFiltros(
                                          context,
                                          widget.padraoDeConfortoFiltro,
                                          widget.reembolsoFiltro,
                                          widget.atendimentoFiltro,
                                          widget.abrangenciaFiltro,
                                          widget.coparticipacaoFiltro,
                                          widget.meiFiltro,
                                          widget.regrasDeAceitacaoFiltro,
                                          widget.somarTaxasFiltro,
                                          selectedOptions,
                                          widget.uf,
                                          widget.idDaCidade,
                                          widget.saudeOuOdonto,
                                          widget.pfOuPme,
                                          widget.nomedocliente,
                                          widget.cidade,
                                          widget.valoratual,
                                          widget.totalDeVidas,
                                          widget.i0a18,
                                          widget.i19a23,
                                          widget.i24a28,
                                          widget.i29a33,
                                          widget.i34a38,
                                          widget.i39a43,
                                          widget.i44a48,
                                          widget.i49a53,
                                          widget.i54a58,
                                          widget.i59,
                                          widget.selectedSaudeIndex,
                                          widget.selectedPlanoIndex,
                                          widget.padraoDeConforto,
                                          widget.reembolso,
                                          widget.atendimento,
                                          widget.abrangencia,
                                          widget.coparticipacao,
                                          widget.mei,
                                          widget.somarTaxas,
                                          widget.regrasDeAcetacao,
                                          widget.credenciado,
                                          widget.produtosFiltro,
                                          widget.operadoras,
                                          widget.entidade,
                                          widget.profissao,
                                          widget.administradora,
                                          widget.mesDeReajuste,
                                          widget.indiceDeReajuste,
                                          widget.valorReajustado,
                                          widget.observacoesDeReajuste,
                                          widget.emailDoCliente,
                                          widget.idDoCliente,
                                          widget.celularDoCliente,
                                          EscolhaAOperadora
                                              .listaDeResumoDeValores,
                                          widget.qualAdministradora,
                                          widget.qualProfissao,
                                          widget.qualEntidade);
                                    },
                                    icon: const Padding(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 10),
                                      child: Icon(Icons.filter_alt_outlined),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    Expanded(
                      child: FutureBuilder(
                          future: listandoOperadoras(
                              context,
                              isLoadingAgora,
                              widget.uf,
                              widget.idDaCidade,
                              widget.saudeOuOdonto,
                              widget.pfOuPme,
                              widget.coparticipacao,
                              widget.credenciado,
                              widget.reembolso,
                              widget.padraoDeConforto,
                              widget.atendimento,
                              widget.abrangencia,
                              widget.mei,
                              widget.somarTaxas,
                              compulsorio,
                              widget.entidade,
                              widget.profissao,
                              widget.administradora,
                              adesao,
                              categoria,
                              filtrarFaixas,
                              produtosParaEnviarPraCotacao,
                              searchText,
                              widget.isAdesaoAgora,
                              widget.valoratual,
                              widget.i0a18,
                              widget.i19a23,
                              widget.i24a28,
                              widget.i29a33,
                              widget.i34a38,
                              widget.i39a43,
                              widget.i44a48,
                              widget.i49a53,
                              widget.i54a58,
                              widget.i59,
                              widget.totalDeVidas,
                              widget.produtosFiltro,
                              widget.operadoras),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) =>
                                  snapshot.hasData
                                      ? ListView(
                                          children: snapshot.data,
                                        )
                                      : Center(
                                          child: Image.asset(
                                          'assets/loader.gif',
                                          width: 90,
                                        ))),
                    ),
                    const SizedBox(height: 20.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Wrap(
                          children: [
                            IconButton(
                              onPressed: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyHomePage(
                                              nomeDoCliente:
                                                  widget.nomedocliente,
                                              cidadeEstado:
                                                  '${widget.cidade} - ${widget.uf} (${widget.idDaCidade.toString()})',
                                              custoDoPlanoAtualValorInicial:
                                                  widget.valoratual,
                                              value0a18:
                                                  int.parse(widget.i0a18),
                                              value19a23:
                                                  int.parse(widget.i19a23),
                                              value24a28:
                                                  int.parse(widget.i24a28),
                                              value29a33:
                                                  int.parse(widget.i29a33),
                                              value34a38:
                                                  int.parse(widget.i34a38),
                                              value39a43:
                                                  int.parse(widget.i39a43),
                                              value44a48:
                                                  int.parse(widget.i44a48),
                                              value49a53:
                                                  int.parse(widget.i49a53),
                                              value54a58:
                                                  int.parse(widget.i54a58),
                                              value59: int.parse(widget.i59),
                                              idDoCliente: widget.idDoCliente,
                                              emailDoCliente:
                                                  widget.emailDoCliente,
                                              celularDoCliente:
                                                  widget.celularDoCliente,
                                            )));
                              },
                              icon: const Icon(Icons.chevron_left),
                            ),
                          ],
                        ),
                        Wrap(
                          children: [
                            IconButton(
                              onPressed: _handleMenuButtonPressed,
                              icon: const Icon(Icons.apps),
                            ),
                          ],
                        ),
                        Wrap(
                          children: [
                            StatefulBuilder(builder: (context, setState) {
                              return ElevatedButton(
                                style: const ButtonStyle(
                                    padding: MaterialStatePropertyAll(
                                        EdgeInsets.symmetric(
                                            vertical: 2, horizontal: 3)),
                                    textStyle: MaterialStatePropertyAll(
                                        TextStyle(fontSize: 13)),
                                    iconSize: MaterialStatePropertyAll(14)),
                                onPressed: _isIndoPraOutraTela
                                    ? null
                                    : () async {
                                        setState(() {
                                          _isIndoPraOutraTela = true;
                                        });
                                        if (produtosParaEnviarPraCotacao
                                            .isEmpty) {
                                          setState(() {
                                            _isIndoPraOutraTela = false;
                                          });
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                  ),
                                                  title: Center(
                                                    child: Column(
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            IconButton(
                                                                onPressed: () {
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                icon: const Icon(
                                                                    Icons
                                                                        .close))
                                                          ],
                                                        ),
                                                        const FaIcon(
                                                          FontAwesomeIcons
                                                              .triangleExclamation,
                                                          size: 30,
                                                          color:
                                                              Colors.deepOrange,
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        const Text(
                                                          'Atenção!!!',
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .deepOrange),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        const Text(
                                                          'Selecione pelo menos um plano para gerar a cotação',
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                        const SizedBox(
                                                          height: 20,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        } else {
                                          var prefs = await SharedPreferences
                                              .getInstance();
                                          String token =
                                              prefs.getString('usr_tkn') ?? '';
                                          int idDaCotacao =
                                              prefs.getInt('idDaCotacao') ?? 0;
                                          String url = EscolhaAOperadora
                                                  .isEdicaoDeCotacao
                                              ? 'https://api-simp.agencialink.com.br/cotacao/salvar/profissional/${idDaCotacao.toString()}'
                                              : 'https://api-simp.agencialink.com.br/cotacao/salvar/profissional';
                                          var header = {
                                            'Authorization': 'Bearer $token',
                                            'Content-Type': 'application/json'
                                          };
                                          Map<String, String> mapaDeItens = {};
                                          for (int item
                                              in produtosParaEnviarPraCotacao) {
                                            mapaDeItens[item.toString()] =
                                                item.toString();
                                          }
                                          var body = json.encode({
                                            "titulo":
                                                "Cotação para ${widget.nomedocliente}",
                                            "nome": widget.nomedocliente,
                                            "email": widget.emailDoCliente,
                                            "fone": "",
                                            "indicacao": widget.idDoCliente,
                                            "url":
                                                "https://profissional-simp.agencialink.com.br/",
                                            "detalhes": {
                                              "uf": widget.uf,
                                              "tipo_plano":
                                                  widget.saudeOuOdonto,
                                              "contratacao": widget.pfOuPme,
                                              "ferramenta": "profissional",
                                              "origem": "app-simp-lite"
                                            },
                                            "parametros": {
                                              "uf": widget.uf,
                                              "cidade": widget.idDaCidade,
                                              "tipo_plano":
                                                  widget.saudeOuOdonto,
                                              "operadoras": "",
                                              "contratacao": widget.pfOuPme,
                                              "profissao": "",
                                              "auto_enviar": "0"
                                            },
                                            "opcoes": {
                                              "processar": "true",
                                              "tipo_plano":
                                                  widget.saudeOuOdonto,
                                              "contratacao": widget.pfOuPme,
                                              "tipo_filtro_vidas": "f",
                                              "grupos_i": ["64f6256361087"],
                                              "faixas_64f625636108a": {
                                                "1": widget.i0a18,
                                                "2": widget.i19a23,
                                                "3": widget.i24a28,
                                                "4": widget.i29a33,
                                                "5": widget.i34a38,
                                                "6": widget.i39a43,
                                                "7": widget.i44a48,
                                                "8": widget.i49a53,
                                                "9": widget.i54a58,
                                                "10": widget.i59
                                              },
                                              "grupos_f": ["64f625636108a"],
                                              "grupos_d": ["64f625636108b"],
                                              "planos":
                                                  produtosParaEnviarPraCotacao,
                                              "modelo": "produto",
                                              "planos_id":
                                                  produtosParaEnviarPraCotacao,
                                              "detalhes": "todos",
                                              "faixas_global": {
                                                "1": widget.i0a18,
                                                "2": widget.i19a23,
                                                "3": widget.i24a28,
                                                "4": widget.i29a33,
                                                "5": widget.i34a38,
                                                "6": widget.i39a43,
                                                "7": widget.i44a48,
                                                "8": widget.i49a53,
                                                "9": widget.i54a58,
                                                "10": widget.i59
                                              },
                                              "faixas": {
                                                "1": widget.i0a18,
                                                "2": widget.i19a23,
                                                "3": widget.i24a28,
                                                "4": widget.i29a33,
                                                "5": widget.i34a38,
                                                "6": widget.i39a43,
                                                "7": widget.i44a48,
                                                "8": widget.i49a53,
                                                "9": widget.i54a58,
                                                "10": widget.i59
                                              },
                                              "resumo_valores": "0",
                                              "copia_corretor": "0",
                                              "alterar_msg": "0",
                                              "copia": "",
                                              "visualizacao": "m",
                                              "imagem_preview": "l",
                                              "aba_ativa": "tab-porplano-tab",
                                              "projecao_economia": {
                                                "valor_atual":
                                                    "R\$ ${widget.valoratual}",
                                                "mes_reajuste":
                                                    widget.mesDeReajuste,
                                                "indice":
                                                    widget.indiceDeReajuste,
                                                "reajustado":
                                                    widget.valorReajustado,
                                                "obs":
                                                    widget.observacoesDeReajuste
                                              },
                                            },
                                            "opcoes_pdf": {}
                                          });
                                          var response = await http.post(
                                              Uri.parse(url),
                                              body: body,
                                              headers: header);
                                          var mapResponse =
                                              json.decode(response.body);
                                          if (response.statusCode == 200) {
                                            if (EscolhaAOperadora
                                                .isEdicaoDeCotacao) {
                                              String urlDaCotacao =
                                                  prefs.getString(
                                                          'urlDaCotacao') ??
                                                      '';
                                              String urlDaCotacaoCompleta =
                                                  prefs.getString(
                                                          'urlDaCotacaoCompleta') ??
                                                      '';
                                              String idDaCotacaoHash =
                                                  prefs.getString(
                                                          'idDaCotacaoHash') ??
                                                      '';
                                              String idDaCotacaoDaqui = prefs
                                                  .getInt('idDaCotacao')
                                                  .toString();
                                              String tokenDoUsr =
                                                  prefs.getString('usr_tkn') ??
                                                      '';
                                              var responseFinal =
                                                  await http.get(Uri.parse(
                                                      'https://api-simp.agencialink.com.br/login/token/$tokenDoUsr/simp_profissional'));
                                              if (responseFinal.statusCode ==
                                                  200) {
                                                var mapResponseFinal = json
                                                    .decode(responseFinal.body);
                                                setState(() {
                                                  _isIndoPraOutraTela = false;
                                                });
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Dialog(
                                                          insetPadding:
                                                              const EdgeInsets
                                                                  .all(0),
                                                          child: Resultado(
                                                            urlDoResultado:
                                                                urlDaCotacao,
                                                            urlDoResultadoCompleta:
                                                                urlDaCotacaoCompleta,
                                                            idDaCotacao:
                                                                idDaCotacaoHash,
                                                            idDaCotacaoNumber:
                                                                idDaCotacaoDaqui,
                                                            emailDoCliente: widget
                                                                .emailDoCliente,
                                                            mensagemPadrao: //"testando aqui",
                                                                mapResponseFinal[
                                                                    "mensagem_padrao"],
                                                            tituloDaCotacao:
                                                                'Cotação para ${widget.nomedocliente}',
                                                            idDoLead: widget
                                                                .idDoCliente,
                                                            celularDoCliente: widget
                                                                .celularDoCliente,
                                                            listaDeResumoDeValores:
                                                                EscolhaAOperadora
                                                                    .listaDeResumoDeValores,
                                                          ));
                                                    });
                                                if (widget.idDoCliente == '') {
                                                  var prefs =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  String token =
                                                      prefs.getString(
                                                              'tokenjwt2') ??
                                                          '';
                                                  String id =
                                                      prefs.getString('id') ??
                                                          '';
                                                  String idDaCotacaoDaqui =
                                                      prefs
                                                          .getInt('idDaCotacao')
                                                          .toString();
                                                  var header = {
                                                    'x-api-key': token,
                                                    'Content-Type':
                                                        'application/json'
                                                  };
                                                  var body = json.encode({
                                                    "user": id,
                                                    "nome":
                                                        widget.nomedocliente,
                                                    "email":
                                                        widget.emailDoCliente,
                                                    "fone": "",
                                                    "assunto":
                                                        "Cotação id $idDaCotacaoDaqui",
                                                    "cotacao": idDaCotacaoDaqui
                                                  });
                                                  await http.post(
                                                      Uri.parse(
                                                          'https://b2corapi.agencialink.com.br/lead/add/simp/'),
                                                      headers: header,
                                                      body: body);
                                                }
                                                /*Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Resultado(
                                                                urlDoResultado:
                                                                    urlDaCotacao,
                                                                urlDoResultadoCompleta:
                                                                    urlDaCotacaoCompleta,
                                                                idDaCotacao:
                                                                    idDaCotacaoHash,
                                                                emailDoCliente: widget
                                                                    .emailDoCliente,
                                                                mensagemPadrao:
                                                                    mapResponseFinal[
                                                                        "mensagem_padrao"],
                                                              )));*/
                                              } else {
                                                setState(() {
                                                  _isIndoPraOutraTela = false;
                                                });
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Dialog(
                                                          insetPadding:
                                                              const EdgeInsets
                                                                  .all(0),
                                                          child: Resultado(
                                                              urlDoResultado:
                                                                  urlDaCotacao,
                                                              urlDoResultadoCompleta:
                                                                  urlDaCotacaoCompleta,
                                                              idDaCotacao:
                                                                  idDaCotacaoHash,
                                                              idDaCotacaoNumber:
                                                                  idDaCotacaoDaqui,
                                                              emailDoCliente: widget
                                                                  .emailDoCliente,
                                                              mensagemPadrao:
                                                                  '',
                                                              tituloDaCotacao:
                                                                  'Cotação para ${widget.nomedocliente}',
                                                              idDoLead: widget
                                                                  .idDoCliente,
                                                              celularDoCliente:
                                                                  widget
                                                                      .celularDoCliente,
                                                              listaDeResumoDeValores:
                                                                  EscolhaAOperadora
                                                                      .listaDeResumoDeValores));
                                                    });
                                                if (widget.idDoCliente == '') {
                                                  var prefs =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  String token =
                                                      prefs.getString(
                                                              'tokenjwt2') ??
                                                          '';
                                                  String id =
                                                      prefs.getString('id') ??
                                                          '';
                                                  String idDaCotacaoDaqui =
                                                      prefs
                                                          .getInt('idDaCotacao')
                                                          .toString();
                                                  var header = {
                                                    'x-api-key': token,
                                                    'Content-Type':
                                                        'application/json'
                                                  };
                                                  var body = json.encode({
                                                    "user": id,
                                                    "nome":
                                                        widget.nomedocliente,
                                                    "email":
                                                        widget.emailDoCliente,
                                                    "fone": "",
                                                    "assunto":
                                                        "Cotação id $idDaCotacaoDaqui",
                                                    "cotacao": idDaCotacaoDaqui
                                                  });
                                                  await http.post(
                                                      Uri.parse(
                                                          'https://b2corapi.agencialink.com.br/lead/add/simp/'),
                                                      headers: header,
                                                      body: body);
                                                }
                                                /*Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Resultado(
                                                                urlDoResultado:
                                                                    urlDaCotacao,
                                                                urlDoResultadoCompleta:
                                                                    urlDaCotacaoCompleta,
                                                                idDaCotacao:
                                                                    idDaCotacaoHash,
                                                                emailDoCliente: widget
                                                                    .emailDoCliente,
                                                                mensagemPadrao:
                                                                    '',
                                                              )));*/
                                              }
                                            } else {
                                              await prefs.setInt('idDaCotacao',
                                                  mapResponse["id_cotacao"]);
                                              await prefs.setString(
                                                  'urlDaCotacao',
                                                  mapResponse["url"]);
                                              await prefs.setString(
                                                  'urlDaCotacaoCompleta',
                                                  mapResponse["url_cotacao"]);
                                              await prefs.setString(
                                                  'idDaCotacaoHash',
                                                  mapResponse["hash"]);
                                              String urlDaCotacao =
                                                  mapResponse["url"];
                                              String urlDaCotacaoCompleta =
                                                  mapResponse["url_cotacao"];
                                              String idDaCotacaoHash =
                                                  mapResponse["hash"]
                                                      .toString();
                                              String idDaCotacaoDaqui =
                                                  mapResponse["id_cotacao"]
                                                      .toString();
                                              String tokenDoUsr =
                                                  prefs.getString('usr_tkn') ??
                                                      '';
                                              var responseFinal =
                                                  await http.get(Uri.parse(
                                                      'https://api-simp.agencialink.com.br/login/token/$tokenDoUsr/simp_profissional'));
                                              if (responseFinal.statusCode ==
                                                  200) {
                                                var mapResponseFinal = json
                                                    .decode(responseFinal.body);
                                                setState(() {
                                                  _isIndoPraOutraTela = false;
                                                });
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Dialog(
                                                          insetPadding:
                                                              const EdgeInsets
                                                                  .all(0),
                                                          child: Resultado(
                                                              urlDoResultado:
                                                                  urlDaCotacao,
                                                              urlDoResultadoCompleta:
                                                                  urlDaCotacaoCompleta,
                                                              idDaCotacao:
                                                                  idDaCotacaoHash,
                                                              idDaCotacaoNumber:
                                                                  idDaCotacaoDaqui,
                                                              emailDoCliente: widget
                                                                  .emailDoCliente,
                                                              mensagemPadrao: //'testando aqui',
                                                                  mapResponseFinal[
                                                                      "mensagem_padrao"],
                                                              tituloDaCotacao:
                                                                  'Cotação para ${widget.nomedocliente}',
                                                              idDoLead: widget
                                                                  .idDoCliente,
                                                              celularDoCliente:
                                                                  widget
                                                                      .celularDoCliente,
                                                              listaDeResumoDeValores:
                                                                  EscolhaAOperadora
                                                                      .listaDeResumoDeValores));
                                                    });
                                                if (widget.idDoCliente == '') {
                                                  var prefs =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  String token =
                                                      prefs.getString(
                                                              'tokenjwt2') ??
                                                          '';
                                                  String id =
                                                      prefs.getString('id') ??
                                                          '';
                                                  String idDaCotacaoDaqui =
                                                      prefs
                                                          .getInt('idDaCotacao')
                                                          .toString();
                                                  var header = {
                                                    'x-api-key': token,
                                                    'Content-Type':
                                                        'application/json'
                                                  };
                                                  var body = json.encode({
                                                    "user": id,
                                                    "nome":
                                                        widget.nomedocliente,
                                                    "email":
                                                        widget.emailDoCliente,
                                                    "fone": "",
                                                    "assunto":
                                                        "Cotação id $idDaCotacaoDaqui",
                                                    "cotacao": idDaCotacaoDaqui
                                                  });
                                                  await http.post(
                                                      Uri.parse(
                                                          'https://b2corapi.agencialink.com.br/lead/add/simp/'),
                                                      headers: header,
                                                      body: body);
                                                }
                                                /*Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Resultado(
                                                                urlDoResultado:
                                                                    urlDaCotacao,
                                                                urlDoResultadoCompleta:
                                                                    urlDaCotacaoCompleta,
                                                                idDaCotacao:
                                                                    idDaCotacaoHash,
                                                                emailDoCliente: widget
                                                                    .emailDoCliente,
                                                                mensagemPadrao:
                                                                    mapResponseFinal[
                                                                        "mensagem_padrao"],
                                                              )));*/
                                              } else {
                                                setState(() {
                                                  _isIndoPraOutraTela = false;
                                                });
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Dialog(
                                                          insetPadding:
                                                              const EdgeInsets
                                                                  .all(0),
                                                          child: Resultado(
                                                              urlDoResultado:
                                                                  urlDaCotacao,
                                                              urlDoResultadoCompleta:
                                                                  urlDaCotacaoCompleta,
                                                              idDaCotacao:
                                                                  idDaCotacaoHash,
                                                              idDaCotacaoNumber:
                                                                  idDaCotacaoDaqui,
                                                              emailDoCliente: widget
                                                                  .emailDoCliente,
                                                              mensagemPadrao:
                                                                  '',
                                                              tituloDaCotacao:
                                                                  'Cotação para ${widget.nomedocliente}',
                                                              idDoLead: widget
                                                                  .idDoCliente,
                                                              celularDoCliente:
                                                                  widget
                                                                      .celularDoCliente,
                                                              listaDeResumoDeValores:
                                                                  EscolhaAOperadora
                                                                      .listaDeResumoDeValores));
                                                    });
                                                if (widget.idDoCliente == '') {
                                                  var prefs =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  String token =
                                                      prefs.getString(
                                                              'tokenjwt2') ??
                                                          '';
                                                  String id =
                                                      prefs.getString('id') ??
                                                          '';
                                                  String idDaCotacaoDaqui =
                                                      prefs
                                                          .getInt('idDaCotacao')
                                                          .toString();
                                                  var header = {
                                                    'x-api-key': token,
                                                    'Content-Type':
                                                        'application/json'
                                                  };
                                                  var body = json.encode({
                                                    "user": id,
                                                    "nome":
                                                        widget.nomedocliente,
                                                    "email":
                                                        widget.emailDoCliente,
                                                    "fone": "",
                                                    "assunto":
                                                        "Cotação id $idDaCotacaoDaqui",
                                                    "cotacao": idDaCotacaoDaqui
                                                  });
                                                  await http.post(
                                                      Uri.parse(
                                                          'https://b2corapi.agencialink.com.br/lead/add/simp/'),
                                                      headers: header,
                                                      body: body);
                                                }
                                                /*Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Resultado(
                                                                urlDoResultado:
                                                                    urlDaCotacao,
                                                                urlDoResultadoCompleta:
                                                                    urlDaCotacaoCompleta,
                                                                idDaCotacao:
                                                                    idDaCotacaoHash,
                                                                emailDoCliente: widget
                                                                    .emailDoCliente,
                                                                mensagemPadrao:
                                                                    '',
                                                              )));*/
                                              }
                                            }
                                          } else {
                                            setState(() {
                                              _isIndoPraOutraTela = false;
                                            });
                                          }
                                        }
                                      },
                                child: _isIndoPraOutraTela
                                    ? const Text('...')
                                    : Wrap(
                                        alignment: WrapAlignment.center,
                                        crossAxisAlignment:
                                            WrapCrossAlignment.center,
                                        children: [
                                          Stack(
                                            children: [
                                              StreamBuilder<int>(
                                                stream: productsCountStream(),
                                                initialData:
                                                    produtosParaEnviarPraCotacao
                                                        .length,
                                                builder: (context, snapshot) {
                                                  if (snapshot.hasData) {
                                                    return snapshot.data == 0
                                                        ? const SizedBox
                                                            .shrink()
                                                        : Positioned(
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(
                                                                      Radius.circular(
                                                                          100)),
                                                              child: Container(
                                                                width: 16,
                                                                color:
                                                                    Colors.red,
                                                                child: Center(
                                                                  child:
                                                                      Padding(
                                                                    padding:
                                                                        const EdgeInsets
                                                                            .all(
                                                                            2.0),
                                                                    child: Text(
                                                                      snapshot
                                                                          .data
                                                                          .toString(),
                                                                      style:
                                                                          const TextStyle(
                                                                        fontSize:
                                                                            10,
                                                                        fontWeight:
                                                                            FontWeight.w400,
                                                                        color: Colors
                                                                            .white,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          );
                                                  } else {
                                                    return const SizedBox
                                                        .shrink();
                                                  }
                                                },
                                              ),
                                            ],
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          const Text('Processar'),
                                          const Icon(Icons.chevron_right),
                                        ],
                                      ),
                              );
                            }),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
