// prefer_typing_uninitialized_variables

// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';
import 'package:http/http.dart' as http;
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/usuarionew.dart';
//import '../api/home_block.dart';

class LoginApi {
  //var bloc = BlocHome();

  //void initState(BuildContext context){
  //  bloc.initOneSignal(context);
  //}

  static Future<Usuario?> login(
      String login, String senha, String onesignalUserId, int permis) async {
    //var status = await OneSignal.shared.getDeviceState();

    var url = Uri.parse('https://b2corapi.agencialink.com.br/login/');
    var header = {"Content-Type": "application/json"};

    //String? onesignalUserId = status?.userId;

    Map params = {
      "login": login,
      "senha": senha,
      "onesignal_id": onesignalUserId,
      "detalhes": permis,
    };

    var usuario;

    var prefs = await SharedPreferences.getInstance();

    //if(onesignalUserId!.isNotEmpty){
    //  prefs.setString("oneid", onesignalUserId);
    // }

    var body = json.encode(params);

    var response = await http.post(url, headers: header, body: body);

    Map<String, dynamic> mapResponse = json.decode(response.body);

    if (mapResponse["code"] == json.decode('12')) {
      //usuario = Usuario.fromJson(mapResponse);
      prefs.setString("msg", mapResponse["msg"]);
      usuario = null;
    } else if (mapResponse["status"] == json.decode('false')) {
      //usuario = Usuario.fromJson(mapResponse);
      prefs.setString("msg", mapResponse["msg"]);
      usuario = null;
    } else if (mapResponse["logotipo_usuario"] == false) {
      usuario = Usuario.fromJson(mapResponse);
      //bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
      bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
      bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
      bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor;
      bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
      bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
      bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
      bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
      bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
      bool simpTabelas = Usuario.fromJson(mapResponse).permissoes.simpTabelas;
      bool simpProfissional =
          Usuario.fromJson(mapResponse).permissoes.simpProfissional;
      bool simpModerno = Usuario.fromJson(mapResponse).permissoes.simpModerno;
      bool simpTradicional =
          Usuario.fromJson(mapResponse).permissoes.simpTradicional;
      bool simpAntigo = Usuario.fromJson(mapResponse).permissoes.simpAntigo;
      bool b2corAntigo = Usuario.fromJson(mapResponse).permissoes.b2corAntigo;
      String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
      prefs.setString('tokenjwt2', mapResponse["token"]);
      prefs.setString("usr_tkn", mapResponse["user_token"]);
      prefs.setString("usr_pass", mapResponse["user_pass"]);
      prefs.setString("nome", mapResponse["name"]);
      prefs.setString("login", mapResponse["login"]);
      prefs.setString("id", mapResponse["id"]);
      //prefs.setBool("treinacor", treinacor);
      prefs.setBool("controlcor", controlcor);
      prefs.setBool("sivcor", sivcor);
      prefs.setBool("appcor", appcor);
      prefs.setBool("cardcor", cardcor);
      prefs.setBool("b2cor", b2cor);
      prefs.setBool("pontodigital", pontodigital);
      prefs.setBool("mktcor", mktcor);
      prefs.setBool("simp", simp);
      prefs.setBool("simpTabelas", simpTabelas);
      prefs.setBool("simpProfissional", simpProfissional);
      prefs.setBool("simpModerno", simpModerno);
      prefs.setBool("simpTradicional", simpTradicional);
      prefs.setBool("simpAntigo", simpAntigo);
      try {
        prefs.setBool("b2corAntigo", b2corAntigo);
      } catch (e) {
        prefs.setBool("b2corAntigo", false);
      }
      prefs.setString("appControle", appControle);
      if (mapResponse["cartao"] != false || mapResponse["cartao"] != null) {
        try {
          prefs.setString("cartao", mapResponse["cartao"]);
        } catch (e) {
          prefs.setBool("cartao21", mapResponse["cartao"]);
        }
      }
      await prefs.setInt("counterqualtela", 0);
      int counterqualtela = prefs.getInt("counterqualtela") ?? 0;
      if (simpTabelas) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpModerno) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpTradicional) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpProfissional) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (counterqualtela == 0) {
        await prefs.setString("qualateladehome", "telaoriginal");
      }
      if (counterqualtela == 1) {
        await prefs.setString("qualateladehome", "direto");
        if (simpModerno) {
          await prefs.setString("simuladorUnico", "simp_moderno");
        }
        if (simpTradicional) {
          await prefs.setString("simuladorUnico", "simp_tradicional");
        }
        if (simpTabelas) {
          await prefs.setString("simuladorUnico", "simp_tabelas");
        }
        if (simpProfissional) {
          await prefs.setString("simuladorUnico", "simp_profissional");
        }
      }
      if (counterqualtela > 1) {
        await prefs.setString("qualateladehome", "telaoriginal");
      }
    } else if (mapResponse["cartao"] == false) {
      usuario = Usuario.fromJson(mapResponse);
      bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
      bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
      bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
      bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor;
      bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
      bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
      bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
      bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
      bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
      bool simpTabelas = Usuario.fromJson(mapResponse).permissoes.simpTabelas;
      bool simpProfissional =
          Usuario.fromJson(mapResponse).permissoes.simpProfissional;
      bool simpModerno = Usuario.fromJson(mapResponse).permissoes.simpModerno;
      bool simpTradicional =
          Usuario.fromJson(mapResponse).permissoes.simpTradicional;
      bool simpAntigo = Usuario.fromJson(mapResponse).permissoes.simpAntigo;
      bool b2corAntigo = Usuario.fromJson(mapResponse).permissoes.b2corAntigo;
      String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
      prefs.setString('tokenjwt2', mapResponse["token"]);
      prefs.setString("usr_tkn", mapResponse["user_token"]);
      prefs.setString("usr_pass", mapResponse["user_pass"]);
      prefs.setString("nome", mapResponse["name"]);
      prefs.setString("login", mapResponse["login"]);
      prefs.setString("logotipousuario", mapResponse["logotipo_usuario"]);
      prefs.setString("id", mapResponse["id"]);
      prefs.setBool("treinacor", treinacor);
      prefs.setBool("controlcor", controlcor);
      prefs.setBool("sivcor", sivcor);
      prefs.setBool("appcor", appcor);
      prefs.setBool("cardcor", cardcor);
      prefs.setBool("b2cor", b2cor);
      prefs.setBool("pontodigital", pontodigital);
      prefs.setBool("mktcor", mktcor);
      prefs.setBool("simp", simp);
      prefs.setBool("simpTabelas", simpTabelas);
      prefs.setBool("simpProfissional", simpProfissional);
      prefs.setBool("simpModerno", simpModerno);
      prefs.setBool("simpTradicional", simpTradicional);
      prefs.setBool("simpAntigo", simpAntigo);
      try {
        prefs.setBool("b2corAntigo", b2corAntigo);
      } catch (e) {
        prefs.setBool("b2corAntigo", false);
      }
      prefs.setString("appControle", appControle);
      if (mapResponse["cartao"] != false || mapResponse["cartao"] != null) {
        try {
          prefs.setString("cartao", mapResponse["cartao"]);
        } catch (e) {
          prefs.setBool("cartao21", mapResponse["cartao"]);
        }
      }
      await prefs.setInt("counterqualtela", 0);
      int counterqualtela = prefs.getInt("counterqualtela") ?? 0;
      if (simpTabelas) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpModerno) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpTradicional) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpProfissional) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (counterqualtela == 0) {
        await prefs.setString("qualateladehome", "telaoriginal");
      }
      if (counterqualtela == 1) {
        await prefs.setString("qualateladehome", "direto");
        if (simpModerno) {
          await prefs.setString("simuladorUnico", "simp_moderno");
        }
        if (simpTradicional) {
          await prefs.setString("simuladorUnico", "simp_tradicional");
        }
        if (simpTabelas) {
          await prefs.setString("simuladorUnico", "simp_tabelas");
        }
        if (simpProfissional) {
          await prefs.setString("simuladorUnico", "simp_profissional");
        }
      }
      if (counterqualtela > 1) {
        await prefs.setString("qualateladehome", "telaoriginal");
      }
    } else if (mapResponse["status"] == true) {
      usuario = Usuario.fromJson(mapResponse);
      bool treinacor = Usuario.fromJson(mapResponse).permissoes.treinacor;
      bool controlcor = Usuario.fromJson(mapResponse).permissoes.controlcor;
      bool sivcor = Usuario.fromJson(mapResponse).permissoes.sivcor;
      bool appcor = Usuario.fromJson(mapResponse).permissoes.appcor;
      bool cardcor = Usuario.fromJson(mapResponse).permissoes.cardcor;
      bool b2cor = Usuario.fromJson(mapResponse).permissoes.b2cor;
      bool pontodigital = Usuario.fromJson(mapResponse).permissoes.pontodigital;
      bool mktcor = Usuario.fromJson(mapResponse).permissoes.mktcor;
      bool simp = Usuario.fromJson(mapResponse).permissoes.simp;
      bool simpTabelas = Usuario.fromJson(mapResponse).permissoes.simpTabelas;
      bool simpProfissional =
          Usuario.fromJson(mapResponse).permissoes.simpProfissional;
      bool simpModerno = Usuario.fromJson(mapResponse).permissoes.simpModerno;
      bool simpTradicional =
          Usuario.fromJson(mapResponse).permissoes.simpTradicional;
      bool simpAntigo = Usuario.fromJson(mapResponse).permissoes.simpAntigo;
      String appControle = Usuario.fromJson(mapResponse).permissoes.appControle;
      bool b2corAntigo = Usuario.fromJson(mapResponse).permissoes.b2corAntigo;
      prefs.setString('tokenjwt2', mapResponse["token"]);
      prefs.setString("usr_tkn", mapResponse["user_token"]);
      prefs.setString("usr_pass", mapResponse["user_pass"]);
      prefs.setString("nome", mapResponse["name"]);
      prefs.setString("login", mapResponse["login"]);
      prefs.setString("logotipousuario", mapResponse["logotipo_usuario"]);
      prefs.setString("cartao", mapResponse["cartao"]);
      prefs.setString("id", mapResponse["id"]);
      prefs.setBool("treinacor", treinacor);
      prefs.setBool("controlcor", controlcor);
      prefs.setBool("sivcor", sivcor);
      prefs.setBool("appcor", appcor);
      prefs.setBool("cardcor", cardcor);
      prefs.setBool("b2cor", b2cor);
      prefs.setBool("pontodigital", pontodigital);
      prefs.setBool("mktcor", mktcor);
      prefs.setBool("simp", simp);
      prefs.setBool("simpTabelas", simpTabelas);
      prefs.setBool("simpProfissional", simpProfissional);
      prefs.setBool("simpModerno", simpModerno);
      prefs.setBool("simpTradicional", simpTradicional);
      prefs.setBool("simpAntigo", simpAntigo);
      prefs.setString("appControle", appControle);
      try {
        prefs.setBool("b2corAntigo", b2corAntigo);
      } catch (e) {
        prefs.setBool("b2corAntigo", false);
      }
      await prefs.setInt("counterqualtela", 0);
      int counterqualtela = prefs.getInt("counterqualtela") ?? 0;
      if (simpTabelas) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpModerno) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpTradicional) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (simpProfissional) {
        await prefs.setInt("counterqualtela", counterqualtela++);
      }
      if (counterqualtela == 0) {
        await prefs.setString("qualateladehome", "telaoriginal");
      }
      if (counterqualtela == 1) {
        await prefs.setString("qualateladehome", "direto");
        if (simpModerno) {
          await prefs.setString("simuladorUnico", "simp_moderno");
        }
        if (simpTradicional) {
          await prefs.setString("simuladorUnico", "simp_tradicional");
        }
        if (simpTabelas) {
          await prefs.setString("simuladorUnico", "simp_tabelas");
        }
        if (simpProfissional) {
          await prefs.setString("simuladorUnico", "simp_profissional");
        }
      }
      if (counterqualtela > 1) {
        await prefs.setString("qualateladehome", "telaoriginal");
      }
    }
    return usuario;
  }
}
