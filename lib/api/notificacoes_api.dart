// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:simplite/constants.dart';
import 'package:simplite/notificacoes/notificacoes.dart';
import 'package:simplite/notificacoes/visualizar_notificacao.dart';

marcarTodasComoLidas() async {
  var prefs = await SharedPreferences.getInstance();
  String url = prefs.getString('usr_tkn')!;
  var response = await http
      .get(Uri.parse("https://b2corapi.agencialink.com.br/login/avisos/$url"));
  List<dynamic> mapResponse = json.decode(response.body);
  List.generate(
      mapResponse.length,
      (index) async => await http.get(Uri.parse(
          'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"]}/lida')));
}

marcarTodasComoNaoLidas() async {
  var prefs = await SharedPreferences.getInstance();
  String url = prefs.getString('usr_tkn')!;
  var response = await http
      .get(Uri.parse("https://b2corapi.agencialink.com.br/login/avisos/$url"));
  List<dynamic> mapResponse = json.decode(response.body);
  List.generate(
      mapResponse.length,
      (index) async => await http.get(Uri.parse(
          'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"]}/naolida')));
}

Future<List<Widget>> listaDeNotificacoesTodas(BuildContext context) async {
  bool? isLoading;
  var prefs = await SharedPreferences.getInstance();
  String url = prefs.getString('usr_tkn')!;
  var response = await http
      .get(Uri.parse("https://b2corapi.agencialink.com.br/login/avisos/$url"));

  if (response.statusCode == 200) {
    List<dynamic> mapResponse = json.decode(response.body);
    return List.generate(mapResponse.length, (index) {
      List<ExpandedTileController> itemControllers = List.generate(
        mapResponse.length,
        (index) => ExpandedTileController(),
      );
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: ExpandedTile(
              contentseparator: 0,
              onTap: () async {
                setState(() {
                  isLoading = true;
                });
                var prefs = await SharedPreferences.getInstance();
                String url = prefs.getString('usr_tkn')!;

                mapResponse[index]["status"] == "1"
                    ? await http.get(Uri.parse(
                        'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                    : await http.get(Uri.parse(
                        'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));
                setState(() {
                  isLoading = false;
                });
              },
              trailingRotation: 0,
              theme: ExpandedTileThemeData(
                titlePadding: const EdgeInsets.all(0),
                headerPadding: const EdgeInsets.all(0),
                leadingPadding: const EdgeInsets.all(0),
                trailingPadding: const EdgeInsets.all(0),
                contentPadding:
                    const EdgeInsets.only(bottom: 8, left: 8, right: 8),
                contentBackgroundColor:
                    Provider.of<ThemeState>(context, listen: false).isDarkMode
                        ? Colors.white24
                        : const Color.fromARGB(238, 247, 247, 255),
                headerColor:
                    Provider.of<ThemeState>(context, listen: false).isDarkMode
                        ? Colors.white12
                        : const Color.fromARGB(238, 247, 247, 255),
              ),
              trailing: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var prefs = await SharedPreferences.getInstance();
                        String url = prefs.getString('usr_tkn')!;
                        mapResponse[index]["status"] == "1"
                            ? await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                            : await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VisualizarNotificacao(
                                    urlDaNotificacao: mapResponse[index]["link"]
                                        .toString())));
                      },
                      icon: const Icon(
                        Icons.open_in_new,
                        size: 13,
                      )),
                  IconButton(
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var prefs = await SharedPreferences.getInstance();
                        String url = prefs.getString('usr_tkn')!;

                        var response = mapResponse[index]["status"] == "1"
                            ? await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                            : await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));

                        if (response.statusCode == 200) {
                          setState(() {
                            isLoading = false;
                            NotificacoesModal.statusDaMensagem =
                                const NotificacoesTodas();
                          });
                          await Future.delayed(const Duration(seconds: 1));
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return const NotificacoesModal();
                            },
                          );
                        } else {
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      icon: isLoading == true
                          ? const Text('...')
                          : mapResponse[index]["status"] == "1"
                              ? const FaIcon(FontAwesomeIcons.envelopeOpen,
                                  size: 13)
                              : const FaIcon(
                                  FontAwesomeIcons.envelope,
                                  size: 13,
                                  color: Colors.blue,
                                )),
                ],
              ),
              leading: ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: mapResponse[index]["status"] == "0"
                            ? Colors.blue
                            : Colors.grey, // Cor da borda azul
                        width: 3.0, // Largura da borda
                      ),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: mapResponse[index]["categoria_id"] == "0"
                        ? const FaIcon(
                            FontAwesomeIcons.clockRotateLeft,
                            size: 16,
                          )
                        : mapResponse[index]["categoria_id"] == "2"
                            ? const FaIcon(
                                FontAwesomeIcons.calculator,
                                size: 16,
                              )
                            : mapResponse[index]["categoria_id"] == "8"
                                ? const FaIcon(
                                    FontAwesomeIcons.envelopeOpen,
                                    size: 16,
                                  )
                                : const FaIcon(
                                    FontAwesomeIcons.bell,
                                    size: 16,
                                  ),
                  ),
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      "${mapResponse[index]["categoria"].toString().replaceAll('&ccedil;', 'ç').replaceAll('&atilde;', 'ã').replaceAll('&oacute;', 'ó')} - ${mapResponse[index]["titulo"]}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: const TextStyle(fontSize: 12)),
                  Text(mapResponse[index]["data"],
                      style: const TextStyle(fontSize: 10))
                ],
              ),
              content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Divider(
                        height: 1,
                        thickness: 1,
                        color: Colors.grey[300],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(mapResponse[index]["mensagem"],
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                  ]),
              controller: itemControllers[index]),
        );
      });
    });
  } else {
    ExpandedTileController controller = ExpandedTileController();
    return [
      ExpandedTile(
          title: const Text('Ocorreu um erro'),
          content: const Text('Não foi possível obter os dados da API'),
          controller: controller)
    ];
  }
}

Future<List<Widget>> listaDeNotificacoesLidas(BuildContext context) async {
  bool? isLoading;
  var prefs = await SharedPreferences.getInstance();
  String url = prefs.getString('usr_tkn')!;
  var response = await http.get(Uri.parse(
      "https://b2corapi.agencialink.com.br/login/avisos/$url?status=1"));

  if (response.statusCode == 200) {
    List<dynamic> mapResponse = json.decode(response.body);
    return List.generate(mapResponse.length, (index) {
      List<ExpandedTileController> itemControllers = List.generate(
        mapResponse.length,
        (index) => ExpandedTileController(),
      );
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: ExpandedTile(
              contentseparator: 0,
              onTap: () async {
                setState(() {
                  isLoading = true;
                });
                var prefs = await SharedPreferences.getInstance();
                String url = prefs.getString('usr_tkn')!;
                mapResponse[index]["status"] == "1"
                    ? await http.get(Uri.parse(
                        'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                    : await http.get(Uri.parse(
                        'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));
                setState(() {
                  isLoading = false;
                });
              },
              trailingRotation: 0,
              theme: ExpandedTileThemeData(
                titlePadding: const EdgeInsets.all(0),
                headerPadding: const EdgeInsets.all(0),
                leadingPadding: const EdgeInsets.all(0),
                trailingPadding: const EdgeInsets.all(0),
                contentPadding:
                    const EdgeInsets.only(bottom: 8, left: 8, right: 8),
                contentBackgroundColor:
                    Provider.of<ThemeState>(context, listen: false).isDarkMode
                        ? Colors.white24
                        : const Color.fromARGB(238, 247, 247, 255),
                headerColor:
                    Provider.of<ThemeState>(context, listen: false).isDarkMode
                        ? Colors.white12
                        : const Color.fromARGB(238, 247, 247, 255),
              ),
              trailing: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var prefs = await SharedPreferences.getInstance();
                        String url = prefs.getString('usr_tkn')!;
                        mapResponse[index]["status"] == "1"
                            ? await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                            : await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VisualizarNotificacao(
                                    urlDaNotificacao: mapResponse[index]["link"]
                                        .toString())));
                      },
                      icon: const Icon(
                        Icons.open_in_new,
                        size: 13,
                      )),
                  IconButton(
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var prefs = await SharedPreferences.getInstance();
                        String url = prefs.getString('usr_tkn')!;

                        var response = mapResponse[index]["status"] == "1"
                            ? await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                            : await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));

                        if (response.statusCode == 200) {
                          setState(() {
                            isLoading = false;
                            NotificacoesModal.statusDaMensagem =
                                const NotificacoesTodas();
                          });
                          await Future.delayed(const Duration(seconds: 1));
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return const NotificacoesModal();
                            },
                          );
                        } else {
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      icon: isLoading == true
                          ? const Text('...')
                          : mapResponse[index]["status"] == "1"
                              ? const FaIcon(FontAwesomeIcons.envelopeOpen,
                                  size: 13)
                              : const FaIcon(
                                  FontAwesomeIcons.envelope,
                                  size: 13,
                                  color: Colors.blue,
                                )),
                ],
              ),
              leading: ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: mapResponse[index]["status"] == "0"
                            ? Colors.blue
                            : Colors.grey, // Cor da borda azul
                        width: 3.0, // Largura da borda
                      ),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: mapResponse[index]["categoria_id"] == "0"
                        ? const FaIcon(
                            FontAwesomeIcons.clockRotateLeft,
                            size: 16,
                          )
                        : mapResponse[index]["categoria_id"] == "2"
                            ? const FaIcon(
                                FontAwesomeIcons.calculator,
                                size: 16,
                              )
                            : mapResponse[index]["categoria_id"] == "8"
                                ? const FaIcon(
                                    FontAwesomeIcons.envelopeOpen,
                                    size: 16,
                                  )
                                : const FaIcon(
                                    FontAwesomeIcons.bell,
                                    size: 16,
                                  ),
                  ),
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      "${mapResponse[index]["categoria"].toString().replaceAll('&ccedil;', 'ç').replaceAll('&atilde;', 'ã').replaceAll('&oacute;', 'ó')} - ${mapResponse[index]["titulo"]}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: const TextStyle(fontSize: 12)),
                  Text(mapResponse[index]["data"],
                      style: const TextStyle(fontSize: 10))
                ],
              ),
              content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Divider(
                        height: 1,
                        thickness: 1,
                        color: Colors.grey[300],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(mapResponse[index]["mensagem"],
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                  ]),
              controller: itemControllers[index]),
        );
      });
    });
  } else {
    ExpandedTileController controller = ExpandedTileController();
    return [
      ExpandedTile(
          title: const Text('Ocorreu um erro'),
          content: const Text('Não foi possível obter os dados da API'),
          controller: controller)
    ];
  }
}

Future<List<Widget>> listaDeNotificacoesNaoLidas(BuildContext context) async {
  bool? isLoading;
  var prefs = await SharedPreferences.getInstance();
  String url = prefs.getString('usr_tkn')!;
  var response = await http.get(Uri.parse(
      "https://b2corapi.agencialink.com.br/login/avisos/$url?status=0"));

  if (response.statusCode == 200) {
    List<dynamic> mapResponse = json.decode(response.body);
    return List.generate(mapResponse.length, (index) {
      List<ExpandedTileController> itemControllers = List.generate(
        mapResponse.length,
        (index) => ExpandedTileController(),
      );
      return StatefulBuilder(builder: (context, setState) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: ExpandedTile(
              contentseparator: 0,
              onTap: () async {
                setState(() {
                  isLoading = true;
                });
                var prefs = await SharedPreferences.getInstance();
                String url = prefs.getString('usr_tkn')!;

                mapResponse[index]["status"] == "1"
                    ? await http.get(Uri.parse(
                        'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                    : await http.get(Uri.parse(
                        'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));
                setState(() {
                  isLoading = false;
                });
              },
              trailingRotation: 0,
              theme: ExpandedTileThemeData(
                titlePadding: const EdgeInsets.all(0),
                headerPadding: const EdgeInsets.all(0),
                leadingPadding: const EdgeInsets.all(0),
                trailingPadding: const EdgeInsets.all(0),
                contentPadding:
                    const EdgeInsets.only(bottom: 8, left: 8, right: 8),
                contentBackgroundColor:
                    Provider.of<ThemeState>(context, listen: false).isDarkMode
                        ? Colors.white24
                        : const Color.fromARGB(238, 247, 247, 255),
                headerColor:
                    Provider.of<ThemeState>(context, listen: false).isDarkMode
                        ? Colors.white12
                        : const Color.fromARGB(238, 247, 247, 255),
              ),
              trailing: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var prefs = await SharedPreferences.getInstance();
                        String url = prefs.getString('usr_tkn')!;

                        mapResponse[index]["status"] == "1"
                            ? await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                            : await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VisualizarNotificacao(
                                    urlDaNotificacao: mapResponse[index]["link"]
                                        .toString())));
                      },
                      icon: const Icon(
                        Icons.open_in_new,
                        size: 13,
                      )),
                  IconButton(
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var prefs = await SharedPreferences.getInstance();
                        String url = prefs.getString('usr_tkn')!;

                        var response = mapResponse[index]["status"] == "1"
                            ? await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/naolida'))
                            : await http.get(Uri.parse(
                                'https://b2corapi.agencialink.com.br/login/aviso/$url/${mapResponse[index]["id"].toString()}/lida'));

                        if (response.statusCode == 200) {
                          setState(() {
                            isLoading = false;
                            NotificacoesModal.statusDaMensagem =
                                const NotificacoesTodas();
                          });
                          await Future.delayed(const Duration(seconds: 1));
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return const NotificacoesModal();
                            },
                          );
                        } else {
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      icon: isLoading == true
                          ? const Text('...')
                          : mapResponse[index]["status"] == "1"
                              ? const FaIcon(FontAwesomeIcons.envelopeOpen,
                                  size: 13)
                              : const FaIcon(
                                  FontAwesomeIcons.envelope,
                                  size: 13,
                                  color: Colors.blue,
                                )),
                ],
              ),
              leading: ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: mapResponse[index]["status"] == "0"
                            ? Colors.blue
                            : Colors.grey, // Cor da borda azul
                        width: 3.0, // Largura da borda
                      ),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: mapResponse[index]["categoria_id"] == "0"
                        ? const FaIcon(
                            FontAwesomeIcons.clockRotateLeft,
                            size: 16,
                          )
                        : mapResponse[index]["categoria_id"] == "2"
                            ? const FaIcon(
                                FontAwesomeIcons.calculator,
                                size: 16,
                              )
                            : mapResponse[index]["categoria_id"] == "8"
                                ? const FaIcon(
                                    FontAwesomeIcons.envelopeOpen,
                                    size: 16,
                                  )
                                : const FaIcon(
                                    FontAwesomeIcons.bell,
                                    size: 16,
                                  ),
                  ),
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      "${mapResponse[index]["categoria"].toString().replaceAll('&ccedil;', 'ç').replaceAll('&atilde;', 'ã').replaceAll('&oacute;', 'ó')} - ${mapResponse[index]["titulo"]}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: const TextStyle(fontSize: 12)),
                  Text(mapResponse[index]["data"],
                      style: const TextStyle(fontSize: 10))
                ],
              ),
              content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Divider(
                        height: 1,
                        thickness: 1,
                        color: Colors.grey[300],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(mapResponse[index]["mensagem"],
                        style: const TextStyle(
                          fontSize: 12,
                        )),
                  ]),
              controller: itemControllers[index]),
        );
      });
    });
  } else {
    ExpandedTileController controller = ExpandedTileController();
    return [
      ExpandedTile(
          title: const Text('Ocorreu um erro'),
          content: const Text('Não foi possível obter os dados da API'),
          controller: controller)
    ];
  }
}

class NotificacoesTodas extends StatelessWidget {
  const NotificacoesTodas({super.key});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeNotificacoesTodas(
          context), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

class NotificacoesNaoLidasClass extends StatelessWidget {
  const NotificacoesNaoLidasClass({super.key});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeNotificacoesNaoLidas(
          context), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

class NotificacoesLidasClass extends StatelessWidget {
  const NotificacoesLidasClass({super.key});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeNotificacoesLidas(
          context), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

Future<String> qtdNotificacoesNaoLidas() async {
  var prefs = await SharedPreferences.getInstance();
  String url = prefs.getString('usr_tkn')!;
  var response = await http.get(Uri.parse(
      "https://b2corapi.agencialink.com.br/login/avisos/$url?status=0"));
  List<dynamic> mapResponse = json.decode(response.body);
  return mapResponse.length.toString();
}
