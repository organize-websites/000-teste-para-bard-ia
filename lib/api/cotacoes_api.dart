// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:simplite/constants.dart';
import 'package:simplite/cotacoes/cotacoes.dart';
import 'package:simplite/cotacoes/visualizar_cotacao.dart';

Future<List<Widget>> listaDeCotacoesTodas(
    BuildContext context, TextEditingController searchController) async {
  var prefs = await SharedPreferences.getInstance();
  List<dynamic> filteredCotacoes = [];
  String token = prefs.getString('usr_tkn')!;
  var header = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var body = json.encode({"procurar": searchController.text});
  var response = await http.post(
      Uri.parse(
          "https://api-simp.agencialink.com.br/dashboard/cotacoes/filtro?${searchController.text}"),
      headers: header,
      body: body);

  if (response.statusCode == 200) {
    var mapResponse = json.decode(response.body);
    // Filtrar com base no texto de pesquisa
    if (searchController.text.isNotEmpty) {
      filteredCotacoes = mapResponse["dados"].where((cotacao) {
        String clienteNome = cotacao["cliente_nome"].toLowerCase();
        String clienteEmail = cotacao["cliente_email"].toLowerCase();
        String secao = cotacao["secao"].toLowerCase();
        String tipo = cotacao["tipo"].toLowerCase();
        String dataD = cotacao["data_d"].toLowerCase();
        String ferramenta = cotacao["ferramenta"].toLowerCase();
        String searchQuery = searchController.text.toLowerCase();
        return clienteNome.contains(searchQuery) ||
            ferramenta.contains(searchQuery) ||
            clienteEmail.contains(searchQuery) ||
            secao.contains(searchQuery) ||
            tipo.contains(searchQuery) ||
            dataD.contains(searchQuery);
      }).toList();
    } else {
      filteredCotacoes = List.from(mapResponse["dados"]);
    }
    return List.generate(filteredCotacoes.length, (index) {
      List<ExpandedTileController> itemControllers = List.generate(
        filteredCotacoes.length,
        (index) => ExpandedTileController(),
      );
      return StatefulBuilder(builder: (context, setState) {
        return ExpandedTile(
            enabled: false,
            trailingRotation: 0,
            theme: ExpandedTileThemeData(
              titlePadding: const EdgeInsets.all(0),
              headerPadding: const EdgeInsets.all(0),
              leadingPadding: const EdgeInsets.all(0),
              trailingPadding: const EdgeInsets.all(0),
              contentPadding: const EdgeInsets.all(8),
              contentBackgroundColor:
                  Provider.of<ThemeState>(context, listen: false).isDarkMode
                      ? Colors.white24
                      : const Color.fromARGB(238, 247, 247, 255),
              headerColor:
                  Provider.of<ThemeState>(context, listen: false).isDarkMode
                      ? Colors.white12
                      : const Color.fromARGB(238, 247, 247, 255),
            ),
            trailing: GestureDetector(
              onTap: () async {
                var response = await http.get(Uri.parse(
                    'https://api-simp.agencialink.com.br/cotacao/buscar/${filteredCotacoes[index]["hash"]}'));
                if (response.statusCode == 200) {
                  var mapResponseLink = json.decode(response.body);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VisualizarCotacao(
                                urlDaNotificacao:
                                    mapResponseLink["url"].toString(),
                                hashUrl: filteredCotacoes[index]["hash"],
                              )));
                } else {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog.adaptive(
                          title: const Text('Ocorreu um erro'),
                          actions: [
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('Fechar'))
                          ],
                        );
                      });
                }
              },
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      right: BorderSide(
                        color: filteredCotacoes[index]["ferramenta"] ==
                                'profissional'
                            ? Colors.blue
                            : filteredCotacoes[index]["ferramenta"] == 'moderno'
                                ? Colors.orange[800]!
                                : filteredCotacoes[index]["ferramenta"] ==
                                        'tradicional'
                                    ? Colors.red[800]!
                                    : filteredCotacoes[index]["ferramenta"] ==
                                            'tabelas'
                                        ? Colors.green
                                        : Colors.grey, // Cor da borda azul
                        width: 3.0, // Largura da borda
                      ),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      IconButton(
                          onPressed: () async {
                            var response = await http.get(Uri.parse(
                                'https://api-simp.agencialink.com.br/cotacao/buscar/${filteredCotacoes[index]["hash"]}'));
                            if (response.statusCode == 200) {
                              var mapResponseLink = json.decode(response.body);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => VisualizarCotacao(
                                            urlDaNotificacao:
                                                mapResponseLink["url"]
                                                    .toString(),
                                            hashUrl: filteredCotacoes[index]
                                                ["hash"],
                                          )));
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog.adaptive(
                                      title: const Text('Ocorreu um erro'),
                                      actions: [
                                        ElevatedButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: const Text('Fechar'))
                                      ],
                                    );
                                  });
                            }
                          },
                          icon: const Icon(
                            Icons.open_in_new,
                            size: 13,
                          )),
                    ],
                  ),
                ),
              ),
            ),
            leading: CotacoesEnviadas.isLoadinDetalhesDaCotacao
                ? ClipRRect(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8),
                        bottomLeft: Radius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          left: BorderSide(
                            color: filteredCotacoes[index]["ferramenta"] ==
                                    'profissional'
                                ? Colors.blue
                                : filteredCotacoes[index]["ferramenta"] ==
                                        'moderno'
                                    ? Colors.orange[800]!
                                    : filteredCotacoes[index]["ferramenta"] ==
                                            'tradicional'
                                        ? Colors.red[800]!
                                        : filteredCotacoes[index]
                                                    ["ferramenta"] ==
                                                'tabelas'
                                            ? Colors.green
                                            : Colors.grey,
                            width: 3.0, // Largura da borda
                          ),
                        ),
                      ),
                      child: CotacoesEnviadas.isLoadinDetalhesDaCotacao
                          ? const Padding(
                              padding: EdgeInsets.all(8.0),
                              child:
                                  Text('...', style: TextStyle(fontSize: 10)),
                            )
                          : Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircleAvatar(
                                radius: 10,
                                backgroundColor:
                                    filteredCotacoes[index]["views"] != null
                                        ? Colors.green
                                        : Colors.grey[200],
                                child: Center(
                                    child: Text(
                                  filteredCotacoes[index]["views"] ?? '',
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 10),
                                )),
                              )),
                    ),
                  )
                : GestureDetector(
                    onTap: () async {
                      setState(() {
                        CotacoesEnviadas.isLoadinDetalhesDaCotacao = true;
                      });
                      var prefs = await SharedPreferences.getInstance();
                      String token = prefs.getString('usr_tkn') ?? '';
                      var header = {'Authorization': 'Bearer $token'};
                      var response2 = await http.post(
                          Uri.parse(
                              "https://api-simp.agencialink.com.br/dashboard/estatisticas/${mapResponse["dados"][index]["id"].toString()}/historico"),
                          headers: header);
                      if (response2.statusCode == 200) {
                        setState(() {
                          CotacoesEnviadas.isLoadinDetalhesDaCotacao = false;
                        });
                        var mapResponse2 = json.decode(response2.body);
                        showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  8.0), // Seu valor de borderRadius aqui
                            ),
                            enableDrag: false,
                            isScrollControlled: true,
                            context: context,
                            builder: (BuildContext context) {
                              return SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.7,
                                child: SingleChildScrollView(
                                  child: Wrap(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(
                                              'Detalhes das Visualizações:',
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            IconButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                icon: const Icon(Icons.close)),
                                          ],
                                        ),
                                      ),
                                      Column(
                                        children: List.generate(
                                            mapResponse2["dados"].length,
                                            (index) => Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8, left: 8, right: 8),
                                                child: Card(
                                                    elevation: 5,
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Wrap(
                                                        children: [
                                                          Marquee(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  mapResponse2["dados"]
                                                                              [
                                                                              index]
                                                                          [
                                                                          "acao"]
                                                                      .toString()
                                                                      .replaceAll(
                                                                          '&ccedil;',
                                                                          'ç')
                                                                      .replaceAll(
                                                                          '&atilde;',
                                                                          'ã'),
                                                                ),
                                                                const SizedBox(
                                                                    width: 15),
                                                                const Text(
                                                                    ' - '),
                                                                const SizedBox(
                                                                    width: 15),
                                                                Text(
                                                                  mapResponse2[
                                                                              "dados"]
                                                                          [
                                                                          index]
                                                                      [
                                                                      "data_f"],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          const Divider(),
                                                          Html(
                                                            data: mapResponse2[
                                                                            "dados"]
                                                                        [index][
                                                                    "detalhes"] ??
                                                                '',
                                                          ),
                                                        ],
                                                      ),
                                                    )))),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            });
                      } else {
                        setState(() {
                          CotacoesEnviadas.isLoadinDetalhesDaCotacao = false;
                        });
                      }
                    },
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(8),
                          bottomLeft: Radius.circular(8)),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              color: filteredCotacoes[index]["ferramenta"] ==
                                      'profissional'
                                  ? Colors.blue
                                  : filteredCotacoes[index]["ferramenta"] ==
                                          'moderno'
                                      ? Colors.orange[800]!
                                      : filteredCotacoes[index]["ferramenta"] ==
                                              'tradicional'
                                          ? Colors.red[800]!
                                          : filteredCotacoes[index]
                                                      ["ferramenta"] ==
                                                  'tabelas'
                                              ? Colors.green
                                              : Colors.grey,
                              width: 3.0, // Largura da borda
                            ),
                          ),
                        ),
                        child: CotacoesEnviadas.isLoadinDetalhesDaCotacao
                            ? const Padding(
                                padding: EdgeInsets.all(8.0),
                                child:
                                    Text('...', style: TextStyle(fontSize: 10)),
                              )
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CircleAvatar(
                                  radius: 10,
                                  backgroundColor:
                                      filteredCotacoes[index]["views"] != null
                                          ? Colors.green
                                          : Colors.grey[200],
                                  child: Center(
                                      child: Text(
                                    filteredCotacoes[index]["views"] ?? '',
                                    style: const TextStyle(
                                        color: Colors.white, fontSize: 10),
                                  )),
                                )),
                      ),
                    ),
                  ),
            title: GestureDetector(
              onTap: () async {
                setState(() {
                  CotacoesEnviadas.isLoadinDetalhesDaCotacao = true;
                });
                var prefs = await SharedPreferences.getInstance();
                String token = prefs.getString('usr_tkn') ?? '';
                var header = {'Authorization': 'Bearer $token'};
                var response2 = await http.post(
                    Uri.parse(
                        "https://api-simp.agencialink.com.br/dashboard/estatisticas/${mapResponse["dados"][index]["id"].toString()}/historico"),
                    headers: header);
                if (response2.statusCode == 200) {
                  setState(() {
                    CotacoesEnviadas.isLoadinDetalhesDaCotacao = false;
                  });
                  var mapResponse2 = json.decode(response2.body);
                  showModalBottomSheet(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                            8.0), // Seu valor de borderRadius aqui
                      ),
                      enableDrag: false,
                      isScrollControlled: true,
                      context: context,
                      builder: (BuildContext context) {
                        return SizedBox(
                          height: MediaQuery.of(context).size.height * 0.7,
                          child: SingleChildScrollView(
                            child: Wrap(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        'Detalhes das Visualizações:',
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      IconButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          icon: const Icon(Icons.close)),
                                    ],
                                  ),
                                ),
                                Column(
                                  children: List.generate(
                                      mapResponse2["dados"].length,
                                      (index) => Padding(
                                          padding: const EdgeInsets.only(
                                              top: 8, left: 8, right: 8),
                                          child: Card(
                                              elevation: 5,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Wrap(
                                                  children: [
                                                    Marquee(
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(
                                                            mapResponse2["dados"]
                                                                        [index]
                                                                    ["acao"]
                                                                .toString()
                                                                .replaceAll(
                                                                    '&ccedil;',
                                                                    'ç')
                                                                .replaceAll(
                                                                    '&atilde;',
                                                                    'ã'),
                                                          ),
                                                          const SizedBox(
                                                              width: 15),
                                                          const Text(' - '),
                                                          const SizedBox(
                                                              width: 15),
                                                          Text(
                                                            mapResponse2[
                                                                        "dados"]
                                                                    [index]
                                                                ["data_f"],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    const Divider(),
                                                    Html(
                                                      data: mapResponse2[
                                                                      "dados"]
                                                                  [index]
                                                              ["detalhes"] ??
                                                          '',
                                                    ),
                                                  ],
                                                ),
                                              )))),
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                } else {
                  setState(() {
                    CotacoesEnviadas.isLoadinDetalhesDaCotacao = false;
                  });
                }
              },
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Wrap(
                      alignment: WrapAlignment.spaceBetween,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text(filteredCotacoes[index]["id"].toString(),
                            style: const TextStyle(fontSize: 10)),
                        const Text('-'),
                        Text(
                            '${filteredCotacoes[index]["data_d"]}-${filteredCotacoes[index]["data_h"]}',
                            style: const TextStyle(fontSize: 10)),
                        const Text('-'),
                        Text(filteredCotacoes[index]["ferramenta"].toString(),
                            style: TextStyle(
                              fontSize: 10,
                              color: filteredCotacoes[index]["ferramenta"] ==
                                      'profissional'
                                  ? Colors.blue
                                  : filteredCotacoes[index]["ferramenta"] ==
                                          'moderno'
                                      ? Colors.orange[800]!
                                      : filteredCotacoes[index]["ferramenta"] ==
                                              'tradicional'
                                          ? Colors.red[800]!
                                          : mapResponse["dados"][index]
                                                      ["ferramenta"] ==
                                                  'tabelas'
                                              ? Colors.green
                                              : Colors.grey,
                            )),
                        const Row()
                      ],
                    ),
                    Text('${filteredCotacoes[index]["cliente_nome"]}',
                        style: const TextStyle(fontSize: 12)),
                    Wrap(
                      alignment: WrapAlignment.spaceBetween,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text('${filteredCotacoes[index]["cliente_email"]}',
                            style: const TextStyle(fontSize: 10)),
                        const Text('-'),
                        Text(
                            '${filteredCotacoes[index]["secao"].toString().replaceAll('&uacute;', 'ú').replaceAll('&atilde;', 'ã')} - ${mapResponse["dados"][index]["tipo"].toString().replaceAll('&uacute;', 'ú').replaceAll('&atilde;', 'ã')} - ${mapResponse["dados"][index]["uf"]}',
                            style: const TextStyle(fontSize: 10)),
                        const Row()
                      ],
                    ),
                  ],
                ),
              ),
            ),
            content: const Column(
                crossAxisAlignment: CrossAxisAlignment.start, children: []),
            controller: itemControllers[index]);
      });
    });
  } else {
    ExpandedTileController controller = ExpandedTileController();
    return [
      ExpandedTile(
          title: const Text('Ocorreu um erro'),
          content: const Text('Não foi possível obter os dados da API'),
          controller: controller)
    ];
  }
}

Future<List<Widget>> listaDeCotacoesLidas(
    BuildContext context, TextEditingController searchController) async {
  var prefs = await SharedPreferences.getInstance();
  List<dynamic> filteredCotacoes = [];
  String token = prefs.getString('usr_tkn')!;
  var header = {'Authorization': 'Bearer $token'};
  var response = await http.post(
      Uri.parse(
          "https://api-simp.agencialink.com.br/dashboard/cotacoes/visualizadas"),
      headers: header);

  if (response.statusCode == 200) {
    var mapResponse = json.decode(response.body);
    // Filtrar com base no texto de pesquisa
    if (searchController.text.isNotEmpty) {
      filteredCotacoes = mapResponse["dados"].where((cotacao) {
        String clienteNome = cotacao["cliente_nome"].toLowerCase();
        String ferramenta = cotacao["ferramenta"].toLowerCase();
        String searchQuery = searchController.text.toLowerCase();
        return clienteNome.contains(searchQuery) ||
            ferramenta.contains(searchQuery);
      }).toList();
    } else {
      filteredCotacoes = List.from(mapResponse["dados"]);
    }
    return List.generate(filteredCotacoes.length, (index) {
      List<ExpandedTileController> itemControllers = List.generate(
        filteredCotacoes.length,
        (index) => ExpandedTileController(),
      );
      return StatefulBuilder(builder: (context, setState) {
        return ExpandedTile(
            enabled: false,
            trailingRotation: 0,
            theme: ExpandedTileThemeData(
              titlePadding: const EdgeInsets.all(0),
              headerPadding: const EdgeInsets.all(0),
              leadingPadding: const EdgeInsets.all(0),
              trailingPadding: const EdgeInsets.all(0),
              contentPadding: const EdgeInsets.all(8),
              contentBackgroundColor:
                  Provider.of<ThemeState>(context, listen: false).isDarkMode
                      ? Colors.white24
                      : const Color.fromARGB(238, 247, 247, 255),
              headerColor:
                  Provider.of<ThemeState>(context, listen: false).isDarkMode
                      ? Colors.white12
                      : const Color.fromARGB(238, 247, 247, 255),
            ),
            trailing: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8)),
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    right: BorderSide(
                      color: filteredCotacoes[index]["ferramenta"] ==
                              'profissional'
                          ? Colors.blue
                          : filteredCotacoes[index]["ferramenta"] == 'moderno'
                              ? Colors.orange[800]!
                              : filteredCotacoes[index]["ferramenta"] ==
                                      'tradicional'
                                  ? Colors.red[800]!
                                  : filteredCotacoes[index]["ferramenta"] ==
                                          'tabelas'
                                      ? Colors.green
                                      : Colors.grey, // Cor da borda azul
                      width: 3.0, // Largura da borda
                    ),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () async {
                          var response = await http.get(Uri.parse(
                              'https://api-simp.agencialink.com.br/cotacao/buscar/${filteredCotacoes[index]["hash"]}'));
                          if (response.statusCode == 200) {
                            var mapResponseLink = json.decode(response.body);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => VisualizarCotacao(
                                          urlDaNotificacao:
                                              mapResponseLink["url"].toString(),
                                          hashUrl: filteredCotacoes[index]
                                              ["hash"],
                                        )));
                          } else {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog.adaptive(
                                    title: const Text('Ocorreu um erro'),
                                    actions: [
                                      ElevatedButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: const Text('Fechar'))
                                    ],
                                  );
                                });
                          }
                          /*Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VisualizarCotacao(
                                      urlDaNotificacao: filteredCotacoes[index]
                                              ["link"]
                                          .toString())));*/
                        },
                        icon: const Icon(
                          Icons.open_in_new,
                          size: 13,
                        )),
                  ],
                ),
              ),
            ),
            leading: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(
                      color: filteredCotacoes[index]["ferramenta"] ==
                              'profissional'
                          ? Colors.blue
                          : filteredCotacoes[index]["ferramenta"] == 'moderno'
                              ? Colors.orange[800]!
                              : filteredCotacoes[index]["ferramenta"] ==
                                      'tradicional'
                                  ? Colors.red[800]!
                                  : filteredCotacoes[index]["ferramenta"] ==
                                          'tabelas'
                                      ? Colors.green
                                      : Colors.grey,
                      width: 3.0, // Largura da borda
                    ),
                  ),
                ),
                child: CotacoesEnviadas.isLoadinDetalhesDaCotacao
                    ? const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text('...', style: TextStyle(fontSize: 10)),
                      )
                    : GestureDetector(
                        onTap: () async {
                          setState(() {
                            CotacoesEnviadas.isLoadinDetalhesDaCotacao = true;
                          });
                          var prefs = await SharedPreferences.getInstance();
                          String token = prefs.getString('usr_tkn') ?? '';
                          var header = {'Authorization': 'Bearer $token'};
                          var response2 = await http.post(
                              Uri.parse(
                                  "https://api-simp.agencialink.com.br/dashboard/estatisticas/${mapResponse["dados"][index]["id"].toString()}/historico"),
                              headers: header);
                          if (response2.statusCode == 200) {
                            setState(() {
                              CotacoesEnviadas.isLoadinDetalhesDaCotacao =
                                  false;
                            });
                            var mapResponse2 = json.decode(response2.body);
                            showModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      8.0), // Seu valor de borderRadius aqui
                                ),
                                enableDrag: false,
                                isScrollControlled: true,
                                context: context,
                                builder: (BuildContext context) {
                                  return SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.7,
                                    child: SingleChildScrollView(
                                      child: Wrap(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text(
                                                  'Detalhes das Visualizações:',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                IconButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    icon: const Icon(
                                                        Icons.close)),
                                              ],
                                            ),
                                          ),
                                          Column(
                                            children: List.generate(
                                                mapResponse2["dados"].length,
                                                (index) => Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 8,
                                                            left: 8,
                                                            right: 8),
                                                    child: Card(
                                                        elevation: 5,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: Wrap(
                                                            children: [
                                                              Marquee(
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  children: [
                                                                    Text(
                                                                      mapResponse2["dados"][index]
                                                                              [
                                                                              "acao"]
                                                                          .toString()
                                                                          .replaceAll(
                                                                              '&ccedil;',
                                                                              'ç')
                                                                          .replaceAll(
                                                                              '&atilde;',
                                                                              'ã'),
                                                                    ),
                                                                    const SizedBox(
                                                                        width:
                                                                            15),
                                                                    const Text(
                                                                        ' - '),
                                                                    const SizedBox(
                                                                        width:
                                                                            15),
                                                                    Text(
                                                                      mapResponse2["dados"]
                                                                              [
                                                                              index]
                                                                          [
                                                                          "data_f"],
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              const Divider(),
                                                              Html(
                                                                data: mapResponse2["dados"]
                                                                            [
                                                                            index]
                                                                        [
                                                                        "detalhes"] ??
                                                                    '',
                                                              ),
                                                            ],
                                                          ),
                                                        )))),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                });
                          } else {
                            setState(() {
                              CotacoesEnviadas.isLoadinDetalhesDaCotacao =
                                  false;
                            });
                          }
                        },
                        child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircleAvatar(
                              radius: 10,
                              backgroundColor:
                                  filteredCotacoes[index]["views"] != null
                                      ? Colors.green
                                      : Colors.grey[200],
                              child: Center(
                                  child: Text(
                                filteredCotacoes[index]["views"] ?? '',
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 10),
                              )),
                            )),
                      ),
              ),
            ),
            title: GestureDetector(
              onTap: () async {
                var response = await http.get(Uri.parse(
                    'https://api-simp.agencialink.com.br/cotacao/buscar/${filteredCotacoes[index]["hash"]}'));
                if (response.statusCode == 200) {
                  var mapResponseLink = json.decode(response.body);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VisualizarCotacao(
                                urlDaNotificacao:
                                    mapResponseLink["url"].toString(),
                                hashUrl: filteredCotacoes[index]["hash"],
                              )));
                } else {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog.adaptive(
                          title: const Text('Ocorreu um erro'),
                          actions: [
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('Fechar'))
                          ],
                        );
                      });
                }
              },
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Wrap(
                      alignment: WrapAlignment.spaceBetween,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text(filteredCotacoes[index]["id"].toString(),
                            style: const TextStyle(fontSize: 10)),
                        const Text('-'),
                        Text(
                            '${filteredCotacoes[index]["data_d"]}-${filteredCotacoes[index]["data_h"]}',
                            style: const TextStyle(fontSize: 10)),
                        const Text('-'),
                        Text(filteredCotacoes[index]["ferramenta"].toString(),
                            style: TextStyle(
                              fontSize: 10,
                              color: filteredCotacoes[index]["ferramenta"] ==
                                      'profissional'
                                  ? Colors.blue
                                  : filteredCotacoes[index]["ferramenta"] ==
                                          'moderno'
                                      ? Colors.orange[800]!
                                      : filteredCotacoes[index]["ferramenta"] ==
                                              'tradicional'
                                          ? Colors.red[800]!
                                          : mapResponse["dados"][index]
                                                      ["ferramenta"] ==
                                                  'tabelas'
                                              ? Colors.green
                                              : Colors.grey,
                            )),
                        const Row()
                      ],
                    ),
                    Text('${filteredCotacoes[index]["cliente_nome"]}',
                        style: const TextStyle(fontSize: 12)),
                    Wrap(
                      alignment: WrapAlignment.spaceBetween,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text('${filteredCotacoes[index]["cliente_email"]}',
                            style: const TextStyle(fontSize: 10)),
                        const Text('-'),
                        Text(
                            '${filteredCotacoes[index]["secao"].toString().replaceAll('&uacute;', 'ú').replaceAll('&atilde;', 'ã')} - ${mapResponse["dados"][index]["tipo"].toString().replaceAll('&uacute;', 'ú').replaceAll('&atilde;', 'ã')} - ${mapResponse["dados"][index]["uf"]}',
                            style: const TextStyle(fontSize: 10)),
                        const Row()
                      ],
                    ),
                  ],
                ),
              ),
            ),
            content: const Column(
                crossAxisAlignment: CrossAxisAlignment.start, children: []),
            controller: itemControllers[index]);
      });
    });
  } else {
    ExpandedTileController controller = ExpandedTileController();
    return [
      ExpandedTile(
          title: const Text('Ocorreu um erro'),
          content: const Text('Não foi possível obter os dados da API'),
          controller: controller)
    ];
  }
}

class CotacoesTodas extends StatelessWidget {
  final TextEditingController searchController;
  const CotacoesTodas({super.key, required this.searchController});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeCotacoesTodas(context,
          searchController), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

class CotacoesLidasClass extends StatelessWidget {
  final TextEditingController searchController;
  const CotacoesLidasClass({super.key, required this.searchController});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeCotacoesLidas(context,
          searchController), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

class CotacoesBusca1 extends StatelessWidget {
  final TextEditingController searchController;
  const CotacoesBusca1({super.key, required this.searchController});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeCotacoesTodas(context,
          searchController), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

class CotacoesBusca2 extends StatelessWidget {
  final TextEditingController searchController;
  const CotacoesBusca2({super.key, required this.searchController});
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Widget>>(
      future: listaDeCotacoesTodas(context,
          searchController), // Seu Future que retorna a lista de ExpandedTile
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: Image.asset(
            'assets/loader.gif',
            width: 90,
          ));
        } else if (snapshot.hasError) {
          return Center(child: Text('Erro: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(child: Text('Nenhum dado disponível.'));
        } else {
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return snapshot.data![index];
            },
          );
        }
      },
    );
  }
}

Future<String> getTotalDeCotacoes() async {
  var prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('usr_tkn')!;
  var header = {'Authorization': 'Bearer $token'};
  var response = await http.post(
      Uri.parse(
          "https://api-simp.agencialink.com.br/dashboard/cotacoes/filtro"),
      headers: header);
  if (response.statusCode == 200) {
    var mapResponse = json.decode(response.body);
    return mapResponse["dados"].length.toString();
  } else {
    return '';
  }
}
