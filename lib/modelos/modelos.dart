class Lead {
  final String name;
  final String email;
  final String data;
  final String id;
  final String celular;

  Lead(
      {required this.name,
      required this.email,
      required this.data,
      required this.id,
      required this.celular});
}

class City {
  String id;
  String name;
  String uf;

  City({required this.id, required this.name, required this.uf});

  factory City.fromJson(Map<String, dynamic> json) {
    return City(
      id: json['id'],
      name: json['cidade'],
      uf: json['uf'],
    );
  }
}

class Administradora {
  String id;
  String nome;
  String logo;

  Administradora({required this.id, required this.nome, required this.logo});

  factory Administradora.fromJson(Map<String, dynamic> json) {
    return Administradora(
      id: json['id'],
      nome: json['nome'],
      logo: json['logo'],
    );
  }
}
