// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simplite/constants.dart';
import 'package:simplite/operadora/operadora.dart';
import 'package:simplite/splash/splash.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webviewx_plus/webviewx_plus.dart';
import 'package:http/http.dart' as http;

String removeHtmlTags(String htmlString) {
  RegExp exp = RegExp(r"<[^>]*>");
  return htmlString.replaceAll(exp, '');
}

class Resultado extends StatefulWidget {
  final String urlDoResultado;
  final String urlDoResultadoCompleta;
  final String idDaCotacao;
  final String idDaCotacaoNumber;
  final String emailDoCliente;
  final String mensagemPadrao;
  final String tituloDaCotacao;
  final String idDoLead;
  final String celularDoCliente;
  final List<String> listaDeResumoDeValores;
  const Resultado(
      {super.key,
      required this.urlDoResultado,
      required this.urlDoResultadoCompleta,
      required this.idDaCotacao,
      required this.idDaCotacaoNumber,
      required this.emailDoCliente,
      required this.mensagemPadrao,
      required this.tituloDaCotacao,
      required this.idDoLead,
      required this.celularDoCliente,
      required this.listaDeResumoDeValores});
  @override
  State<Resultado> createState() => _ResultadoState();
}

class _ResultadoState extends State<Resultado> {
  String selectedImageOption = 'logotipo';
  String selectedHomepageOption = 'planos';
  String selectedViewOption = 'mobile';
  bool inserirResumo = false;
  bool naoAlterarMensagemPadrao = true;
  bool receberCopia = false;
  bool isGerandoPdf = false;
  bool webviewEstaCarregando = false;
  bool isEnviandoAgora = false;
  TextEditingController numeroWhatsApp = TextEditingController();
  TextEditingController assuntoWhatsApp = TextEditingController();
  TextEditingController enderecoDeEmailAEnviar = TextEditingController();
  TextEditingController assuntoAEnviarEmail = TextEditingController();
  TextEditingController controladorDeTeste = TextEditingController();
  TextEditingController mensagemController = TextEditingController();
  final _formKeyWhats = GlobalKey<FormState>();
  final _formKeyEmail = GlobalKey<FormState>();
  late WebViewXController webviewController;

  enviarWhatsModal(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            surfaceTintColor:
                Provider.of<ThemeState>(context, listen: true).isDarkMode
                    ? Colors.black
                    : Colors.white,
            backgroundColor:
                Provider.of<ThemeState>(context, listen: true).isDarkMode
                    ? Colors.black
                    : Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            insetPadding: const EdgeInsets.all(5),
            title:
                const Text('Envie pelo WhatsApp', textAlign: TextAlign.center),
            content: StatefulBuilder(builder: (context, setState) {
              return Form(
                key: _formKeyWhats,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Por favor, digite um número de telefone válido';
                          }
                          if (value.length < 10) {
                            return 'Por favor, digite um número de telefone válido - DDD + Número';
                          }
                          return null;
                        },
                        controller: numeroWhatsApp,
                        keyboardType: TextInputType.phone,
                        decoration: const InputDecoration(
                          prefixIcon: Icon(Icons.phone),
                          labelText: 'WhatsApp',
                        ),
                      ),
                      const SizedBox(
                        height: defaultPadding,
                      ),
                      TextFormField(
                        controller: assuntoWhatsApp,
                        decoration: const InputDecoration(
                          prefixIcon: Icon(Icons.text_fields),
                          labelText: 'Assunto',
                        ),
                      ),
                      const SizedBox(
                        height: defaultPadding,
                      ),
                      TextFormField(
                        maxLines: 3,
                        controller: mensagemController,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(8),
                          prefixIcon: Icon(Icons.mail),
                          labelText: 'Digite sua mensagem aqui',
                        ),
                      ),
                      const SizedBox(height: defaultPadding),
                      Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            naoAlterarMensagemPadrao
                                ? GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        naoAlterarMensagemPadrao = false;
                                      });
                                    },
                                    child: const Icon(
                                      Icons.check,
                                      color: Colors.blue,
                                      size: 10,
                                    ),
                                  )
                                : GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        naoAlterarMensagemPadrao = true;
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(2),
                                        border: Border.all(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700]!),
                                      ),
                                      child: const Center(),
                                    ),
                                  ),
                            const SizedBox(
                              width: 2,
                            ),
                            Marquee(
                              child: const Text('Não alterar a mensagem padrão',
                                  style: TextStyle(fontSize: 11)),
                            ),
                          ]),
                      const SizedBox(height: defaultPaddingMini),
                      Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            const Text(
                              'Visualização:',
                              style: TextStyle(
                                  fontSize: 11, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: defaultPadding),
                            Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        selectedViewOption = 'desktop';
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: selectedViewOption == 'desktop'
                                            ? Colors.blue
                                            : Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        border: Border.all(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700]!),
                                      ),
                                      child: const Center(),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  const FaIcon(FontAwesomeIcons.desktop,
                                      size: 11, color: Colors.grey),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  Marquee(
                                    child: const Text('Desktop',
                                        style: TextStyle(fontSize: 11)),
                                  ),
                                ]),
                            const SizedBox(width: defaultPaddingMini),
                            Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        selectedViewOption = 'mobile';
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: selectedViewOption == 'mobile'
                                            ? Colors.blue
                                            : Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        border: Border.all(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700]!),
                                      ),
                                      child: const Center(),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  const FaIcon(FontAwesomeIcons.mobile,
                                      size: 11, color: Colors.grey),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  Marquee(
                                    child: const Text('Mobile',
                                        style: TextStyle(fontSize: 11)),
                                  ),
                                ]),
                          ]),
                      const SizedBox(
                        height: defaultPadding,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text(
                                'Cancelar',
                                style: TextStyle(color: Colors.grey),
                              )),
                          const SizedBox(
                            width: defaultPadding,
                          ),
                          ElevatedButton(
                              onPressed: () async {
                                if (_formKeyWhats.currentState!.validate()) {
                                  setState(() {
                                    isEnviandoAgora = true;
                                  });
                                  var prefs =
                                      await SharedPreferences.getInstance();
                                  String token =
                                      prefs.getString('usr_tkn') ?? '';
                                  if (selectedViewOption == 'desktop') {
                                    await http.post(
                                        Uri.parse(
                                            'https://api-simp.agencialink.com.br/cotacao/salvar/profissional/${widget.idDaCotacao}'),
                                        headers: {
                                          'Authorization': 'Bearer $token',
                                          'Content-Type': 'application/json'
                                        },
                                        body: json.encode({
                                          "opcoes": {"visualizacao": "d"}
                                        }));
                                  }
                                  await http.post(
                                      Uri.parse(
                                          'https://b2corapi.agencialink.com.br/lead/updateLead/${widget.idDoLead}'),
                                      headers: {
                                        'x-api-key': token,
                                        'Content-Type': 'application/json'
                                      },
                                      body: json.encode(
                                          {"celular": numeroWhatsApp.text}));
                                  if (naoAlterarMensagemPadrao == false) {
                                    await http.post(
                                        Uri.parse(
                                            'https://api-simp.agencialink.com.br/login/mensagem_padrao'),
                                        headers: {
                                          "Authorization": "Bearer $token",
                                          "Content-Type": "application/json"
                                        },
                                        body: json.encode({
                                          "mensagem": mensagemController.text
                                        }));
                                  }
                                  setState(() {
                                    isEnviandoAgora = false;
                                  });
                                  if (!await launchUrl(
                                      Uri.parse(
                                          "https://api.whatsapp.com/send?phone=55${numeroWhatsApp.text}&text=Link da sua Cotação: ${widget.urlDoResultado} - ${assuntoWhatsApp.text} - ${mensagemController.text} \n\n\n*Opções:*\n\n\n ${widget.listaDeResumoDeValores.toString().replaceAll('[', '').replaceAll(']', '')}"),
                                      mode: LaunchMode.externalApplication)) {
                                    throw Exception('Could not launch');
                                  }
                                }
                              },
                              child: Text(isEnviandoAgora ? '...' : 'Enviar')),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }),
          );
        });
  }

  enviarEmailModal(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            surfaceTintColor:
                Provider.of<ThemeState>(context, listen: true).isDarkMode
                    ? Colors.black
                    : Colors.white,
            backgroundColor:
                Provider.of<ThemeState>(context, listen: true).isDarkMode
                    ? Colors.black
                    : Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            insetPadding: const EdgeInsets.all(5),
            title: const Text('Envie por e-mail', textAlign: TextAlign.center),
            content: StatefulBuilder(builder: (context, setState) {
              return Form(
                key: _formKeyEmail,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Por favor, digite um e-mail válido';
                          }
                          if (value.contains('@')) {
                            return null;
                          } else {
                            return 'Por favor, digite um e-mail válido';
                          }
                        },
                        controller: enderecoDeEmailAEnviar,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                          prefixIcon: Icon(Icons.email),
                          labelText: 'E-mail',
                        ),
                      ),
                      const SizedBox(
                        height: defaultPadding,
                      ),
                      TextFormField(
                        controller: assuntoAEnviarEmail,
                        decoration: const InputDecoration(
                          prefixIcon: Icon(Icons.text_fields),
                          labelText: 'Assunto',
                        ),
                      ),
                      const SizedBox(height: defaultPadding),
                      TextFormField(
                        maxLines: 3,
                        controller: mensagemController,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(8),
                          prefixIcon: Icon(Icons.mail),
                          labelText: 'Digite sua mensagem aqui',
                        ),
                      ),
                      const SizedBox(height: defaultPadding),
                      Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            naoAlterarMensagemPadrao
                                ? GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        naoAlterarMensagemPadrao = false;
                                      });
                                    },
                                    child: const Icon(
                                      Icons.check,
                                      color: Colors.blue,
                                      size: 10,
                                    ),
                                  )
                                : GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        naoAlterarMensagemPadrao = true;
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(2),
                                        border: Border.all(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700]!),
                                      ),
                                      child: const Center(),
                                    ),
                                  ),
                            const SizedBox(
                              width: 2,
                            ),
                            Marquee(
                              child: const Text('Não alterar a mensagem padrão',
                                  style: TextStyle(fontSize: 11)),
                            ),
                          ]),
                      const SizedBox(height: defaultPaddingMini),
                      Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            const Text(
                              'Visualização:',
                              style: TextStyle(
                                  fontSize: 11, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              width: defaultPaddingMini,
                            ),
                            Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        selectedViewOption = 'desktop';
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: selectedViewOption == 'desktop'
                                            ? Colors.blue
                                            : Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        border: Border.all(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700]!),
                                      ),
                                      child: const Center(),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  const FaIcon(FontAwesomeIcons.desktop,
                                      size: 11, color: Colors.grey),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  Marquee(
                                    child: const Text('Desktop',
                                        style: TextStyle(fontSize: 11)),
                                  ),
                                ]),
                            const SizedBox(width: defaultPadding),
                            Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      setState(() {
                                        selectedViewOption = 'mobile';
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                        color: selectedViewOption == 'mobile'
                                            ? Colors.blue
                                            : Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        border: Border.all(
                                            color: Provider.of<ThemeState>(
                                                        context,
                                                        listen: true)
                                                    .isDarkMode
                                                ? Colors.white
                                                : Colors.grey[700]!),
                                      ),
                                      child: const Center(),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  const FaIcon(FontAwesomeIcons.mobile,
                                      size: 11, color: Colors.grey),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  Marquee(
                                    child: const Text('Mobile',
                                        style: TextStyle(fontSize: 11)),
                                  ),
                                ]),
                          ]),
                      const SizedBox(
                        height: defaultPadding,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text(
                                'Cancelar',
                                style: TextStyle(color: Colors.grey),
                              )),
                          const SizedBox(
                            width: defaultPadding,
                          ),
                          ElevatedButton(
                              onPressed: () async {
                                if (_formKeyEmail.currentState!.validate()) {
                                  setState(() {
                                    isEnviandoAgora = true;
                                  });
                                  var prefs =
                                      await SharedPreferences.getInstance();
                                  String token =
                                      prefs.getString('usr_tkn') ?? '';
                                  if (selectedViewOption == 'desktop') {
                                    await http.post(
                                        Uri.parse(
                                            'https://api-simp.agencialink.com.br/cotacao/salvar/profissional/${widget.idDaCotacao}'),
                                        headers: {
                                          'Authorization': 'Bearer $token',
                                          'Content-Type': 'application/json'
                                        },
                                        body: json.encode({
                                          "opcoes": {"visualizacao": "d"}
                                        }));
                                  }
                                  var responseTrocaEmail = await http.post(
                                      Uri.parse(
                                          'https://b2corapi.agencialink.com.br/lead/updateLead/${widget.idDoLead}'),
                                      headers: {
                                        'x-api-key': token,
                                        'Content-Type': 'application/json'
                                      },
                                      body: json.encode({
                                        "email": enderecoDeEmailAEnviar.text
                                      }));
                                  if (naoAlterarMensagemPadrao == false) {
                                    await http.post(
                                        Uri.parse(
                                            'https://api-simp.agencialink.com.br/login/mensagem_padrao'),
                                        headers: {
                                          "Authorization": "Bearer $token",
                                          "Content-Type": "application/json"
                                        },
                                        body: json.encode({
                                          "mensagem": mensagemController.text
                                        }));
                                  }
                                  if (responseTrocaEmail.statusCode == 200) {
                                    var response = await http.post(
                                        Uri.parse(
                                            'https://api-simp.agencialink.com.br/email/agendar/${widget.idDaCotacaoNumber}'),
                                        headers: {
                                          "Authorization": "Bearer $token",
                                          "Content-Type": "application/json"
                                        },
                                        body: json.encode({
                                          "comentario":
                                              "<b>Opções:</b>\n\n\n ${widget.listaDeResumoDeValores.toString().replaceAll('[', '').replaceAll(']', '')}"
                                        }));
                                    if (response.statusCode == 200) {
                                      print(widget.idDaCotacao);
                                      setState(() {
                                        isEnviandoAgora = false;
                                      });
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog.adaptive(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                              ),
                                              title:
                                                  const Text('Email enviado!'),
                                              actions: [
                                                TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Fechar'))
                                              ],
                                            );
                                          });
                                    } else {
                                      setState(() {
                                        isEnviandoAgora = false;
                                      });
                                      print(
                                          'aqui temos ${response.body}  ${response.statusCode}');
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog.adaptive(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                              ),
                                              title: const Text(
                                                  'Erro no envio...'),
                                              actions: [
                                                TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Fechar'))
                                              ],
                                            );
                                          });
                                    }
                                  } else {
                                    setState(() {
                                      isEnviandoAgora = false;
                                    });
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog.adaptive(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                            ),
                                            title:
                                                const Text('Erro no envio...'),
                                            actions: [
                                              TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: const Text('Fechar'))
                                            ],
                                          );
                                        });
                                  }
                                }
                              },
                              child: Text(isEnviandoAgora ? '...' : 'Enviar')),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      isGerandoPdf = false;
      webviewEstaCarregando = false;
      enderecoDeEmailAEnviar.text = widget.emailDoCliente;
      assuntoAEnviarEmail.text = widget.tituloDaCotacao;
      assuntoWhatsApp.text = widget.tituloDaCotacao;
      numeroWhatsApp.text = widget.celularDoCliente;
      isEnviandoAgora = false;
      controladorDeTeste.text = widget.mensagemPadrao;
      mensagemController.text = removeHtmlTags(widget.mensagemPadrao);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        actionsAlignment: MainAxisAlignment.spaceBetween,
                        titlePadding:
                            const EdgeInsets.only(top: 10, left: 10, right: 10),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        title: Center(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      icon: const Icon(Icons.close))
                                ],
                              ),
                              const FaIcon(
                                FontAwesomeIcons.triangleExclamation,
                                size: 30,
                                color: Colors.deepOrange,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Text(
                                'Atenção!!!',
                                style: TextStyle(color: Colors.deepOrange),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                        content: const Text(
                          'Deseja editar essa cotação ou realizar uma nova?',
                          textAlign: TextAlign.center,
                        ),
                        actions: [
                          ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  EscolhaAOperadora.isEdicaoDeCotacao = true;
                                });
                                Navigator.pop(context);
                                Navigator.pop(context);
                              },
                              child: const Text(
                                'Editar',
                                style: TextStyle(color: Colors.grey),
                              )),
                          ElevatedButton(
                              onPressed: () async {
                                setState(() {
                                  EscolhaAOperadora.isEdicaoDeCotacao = false;
                                });
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const Splash()));
                              },
                              child: const Text(
                                'Nova',
                                style: TextStyle(color: Colors.blue),
                              )),
                        ],
                      );
                    });
              },
              icon: const Icon(Icons.chevron_left)),
          title: ListTile(
            onTap: () async {
              if (!await launchUrl(Uri.parse(widget.urlDoResultado),
                  mode: LaunchMode.externalApplication)) {
                throw Exception('Could not launch');
              }
            },
            contentPadding: const EdgeInsets.all(1),
            title: Marquee(
              child: const Text(
                'Link da Cotação',
                style: TextStyle(fontSize: 9),
              ),
            ),
            subtitle: Marquee(
              child: Text(
                widget.urlDoResultado,
                style: const TextStyle(fontSize: 12),
              ),
            ),
          ),
          actions: [
            IconButton(
                onPressed: () {
                  FlutterClipboard.copy(widget.urlDoResultado)
                      .whenComplete(() => showAdaptiveDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog.adaptive(
                              title: const Text('Link Copiado!'),
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: const Text('Ok'))
                              ],
                            );
                          }));
                },
                icon: const Icon(Icons.copy)),
            IconButton(
                onPressed: () {
                  enviarWhatsModal(context);
                },
                icon: const FaIcon(FontAwesomeIcons.whatsapp)),
            IconButton(
                onPressed: () {
                  enviarEmailModal(context);
                },
                icon: const FaIcon(FontAwesomeIcons.envelope)),
            IconButton(
                onPressed: () async {
                  setState(() {
                    isGerandoPdf = true;
                  });
                  var response = await http.post(Uri.parse(
                      'https://api-simp.agencialink.com.br/gerador/pdf/${widget.idDaCotacao.toString()}/?somente_profissional=1'));
                  if (response.statusCode == 200) {
                    var mapResponse = json.decode(response.body);
                    setState(() {
                      isGerandoPdf = false;
                    });
                    if (!await launchUrl(Uri.parse(mapResponse["urls"][0]),
                        mode: LaunchMode.externalApplication)) {
                      throw Exception('Could not launch');
                    }
                    /*showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            actionsAlignment: MainAxisAlignment.spaceBetween,
                            titlePadding: const EdgeInsets.only(
                                top: 10, left: 10, right: 10),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            title: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        icon: const Icon(Icons.close))
                                  ],
                                ),
                                FaIcon(
                                  FontAwesomeIcons.circleCheck,
                                  size: 30,
                                  color: Colors.green[700],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'PDF Gerado com Sucesso!',
                                  style: TextStyle(color: Colors.green[700]),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                            content: const Text(
                              'Escolha o que deseja fazer:',
                              textAlign: TextAlign.center,
                            ),
                            actions: [
                              ElevatedButton(
                                  onPressed: () async {
                                    if (!await launchUrl(
                                        Uri.parse(mapResponse["urls"][0]),
                                        mode: LaunchMode.externalApplication)) {
                                      throw Exception('Could not launch');
                                    }
                                  },
                                  child: const Text(
                                    'Visualizar',
                                    style: TextStyle(fontSize: 12),
                                  )),
                              ElevatedButton(
                                  onPressed: () async {
                                    await Share.share(mapResponse["urls"][0]);
                                    /*FlutterClipboard.copy(
                                        mapResponse["urls"][0]);*/
                                  },
                                  child: const Text(
                                    'Compartilhar',
                                    style: TextStyle(fontSize: 12),
                                  )),
                            ],
                          );
                        });*/
                  } else {
                    setState(() {
                      isGerandoPdf = false;
                    });
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: const Text(
                              'Não foi possível gerar o PDF',
                              textAlign: TextAlign.center,
                            ),
                            title: Wrap(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        icon: const Icon(Icons.close))
                                  ],
                                ),
                                const FaIcon(
                                  FontAwesomeIcons.check,
                                  size: 30,
                                  color: Colors.deepOrange,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  'Ocorreu um erro',
                                  style: TextStyle(color: Colors.deepOrange),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          );
                        });
                  }
                },
                icon: isGerandoPdf
                    ? const CircularProgressIndicator.adaptive()
                    : const FaIcon(FontAwesomeIcons.filePdf)),
          ],
        ),
        body: Stack(
          children: [
            WebViewX(
              onWebViewCreated: (controller) => webviewController = controller,
              onPageStarted: (src) async {
                if (src.contains('whatsapp')) {
                  if (!await launchUrl(Uri.parse(src),
                      mode: LaunchMode.externalApplication)) {
                    throw Exception('Could not launch');
                  }
                  await Future.delayed(const Duration(seconds: 2), () async {
                    await webviewController.goBack();
                  });
                }
                if (src.contains('wa.me')) {
                  if (!await launchUrl(Uri.parse(src),
                      mode: LaunchMode.externalApplication)) {
                    throw Exception('Could not launch');
                  }
                  await Future.delayed(const Duration(seconds: 2), () async {
                    await webviewController.goBack();
                  });
                }
                setState(() {
                  webviewEstaCarregando = true;
                });
              },
              onPageFinished: (src) async {
                setState(() {
                  webviewEstaCarregando = false;
                });
              },
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              initialContent: '${widget.urlDoResultadoCompleta}?source=iframe',
            ),
            webviewEstaCarregando
                ? Container(
                    color: Provider.of<ThemeState>(context, listen: false)
                            .isDarkMode
                        ? Colors.black87
                        : Colors.white,
                    child: Center(
                        child: Image.asset(
                      'assets/loader.gif',
                      width: 90,
                    )))
                : const Stack()
          ],
        ),
      ),
    );
  }
}
