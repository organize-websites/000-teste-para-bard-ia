import 'package:flutter/material.dart';

const Color primaryColor = Colors.blue;
const Color bgColor = Color(0xFFFBFBFD);

const double defaultPadding = 13.0;
const double defaultPaddingMini = 5.0;
const double defaultBorderRadius = 12.0;

bool modoEscuro = false;

class ThemeState with ChangeNotifier {
  bool _isDarkMode = false;

  bool get isDarkMode => _isDarkMode;

  void toggleTheme() {
    _isDarkMode = !_isDarkMode;
    notifyListeners();
  }
}
